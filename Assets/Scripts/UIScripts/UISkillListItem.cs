﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISkillListItem : MonoBehaviour,IPointerClickHandler {
	public int skillId;
	public UISkillList uiSkillList; 
	public Text skillName;
	public Text skillDescription;
	public Text skillCost;
	public Image skillIcon;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		if (Control.selectedUnit.GetComponent<Unit> ().mp._current < int.Parse(skillCost.text)) return;
		UIMain.selectedSkillId = skillId;
		Control.selectingSkillTarget = true;
		uiSkillList.cancelBtn.SetActive (true);
		//uiSkillList.GetComponent<UIMain>().selectedSkillListId = skillListId;
		uiSkillList.CloseSkillList ();
		//uiUnits.SelectChar (slot);
		//uiUnits.OpenDetailPanel ();
		//Debug.Log ("Clicked item");
	}
	#endregion
}
