﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UISTATE {INITIAL,UNITLIST,UNITDETAILS,EQUIPMENTLIST,SVERDLIST,SVERDDETAILS,SKILLTREE,SKILLLIST,SHOP};
public enum EQUIPSLOT {HEAD, BODY, LHAND, RHAND, ACC1, ACC2, ENGINE, FRAME, LARM, RARM, BOOSTER1, BOOSTER2};

public class UIMain : MonoBehaviour {
	public Stack<UISTATE> uiState;
	public int selectedUnitListId = -1;
	public int selectedSverdListId = -1;
	public static int selectedSkillId = -1;
	public EQUIPSLOT selectedEquipSlot;
	public static UIMain instance;

	// Use this for initialization
	void Start () {
		instance = this;
		uiState = new Stack<UISTATE> ();
		uiState.Push (UISTATE.INITIAL);
	}

	public static Sprite GetSpriteByName(Sprite[] spriteSheet, string name){
		for (int i = 0; i < spriteSheet.Length; i++) {
			if (spriteSheet [i].name == name)
				return spriteSheet [i];
		}
		return null;
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void CloseSkillList(){
		Time.timeScale = 1;
		Control.selectingSkillTarget = false;
		GetComponent<UISkillList> ().cancelBtn.SetActive (false);
		GetComponent<UISkillList> ().CloseSkillList ();
	}

	public void Close(){
		CloseCurrentUI (); //close current ui
		UISTATE prevState = uiState.Peek();
		uiState.Pop (); //pop state
		//open next ui
		switch (uiState.Peek ()) {
		case UISTATE.INITIAL:
			{
				Time.timeScale = 1;
				if(prevState != UISTATE.SHOP)
					Persistence.InstantiateActiveUnits ();
			}
			break;
		case UISTATE.UNITLIST:
			{
				GetComponent<UIUnitList> ().OpenUnitList ();
			}
			break;
		case UISTATE.UNITDETAILS:
			{
				GetComponent<UIUnitDetails> ().OpenUnitDetails (selectedUnitListId);
			}
			break;
		case UISTATE.SVERDLIST:
			{
				GetComponent<UISverdList> ().OpenSverdList (selectedUnitListId);
			}
			break;
		case UISTATE.SVERDDETAILS:
			{
				GetComponent<UISverdDetails> ().OpenSverdDetails (selectedSverdListId);
			}
			break;
		}			
	}

	public void CloseCurrentUI(){
		switch (uiState.Peek()) {
		case UISTATE.UNITLIST:
			{
				GetComponent<UIUnitList> ().CloseUnitList ();
			}
			break;
		case UISTATE.UNITDETAILS:
			{
				GetComponent<UIUnitDetails> ().CloseUnitDetails ();
			}
			break;
		case UISTATE.EQUIPMENTLIST:
			{
				GetComponent<UIItemList> ().CloseItemList ();
			}
			break;
		case UISTATE.SVERDLIST:
			{
				GetComponent<UISverdList> ().CloseSverdList ();
			}
			break;
		case UISTATE.SVERDDETAILS:
			{
				GetComponent<UISverdDetails> ().CloseSverdDetails ();
			}
			break;
		case UISTATE.SKILLTREE:
			{
				GetComponent<UISkillTree> ().CloseSkillTree();
			}
			break;
		case UISTATE.SHOP:
			{
				GetComponent<UIItemList> ().CloseItemList();
			}
			break;
		}
	}

	public void OpenMenu(){
		if (UnitMgr.IsAnyEngaged())
			return;

		Time.timeScale = 0;
		uiState.Push (UISTATE.UNITLIST);
		GetComponent<UIUnitList> ().OpenUnitList ();
	}

	public void OpenUnitDetails(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.UNITDETAILS);
		selectedUnitListId = unitListId;
		GetComponent<UIUnitDetails> ().OpenUnitDetails (selectedUnitListId);
	}

	public void OpenSkillTree(){
		CloseCurrentUI ();
		uiState.Push (UISTATE.SKILLTREE);
		GetComponent<UISkillTree> ().OpenSkillTree (selectedUnitListId);
	}

	public void OpenSverdDetails(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.SVERDDETAILS);
		selectedSverdListId = sverdListId;
		GetComponent<UISverdDetails> ().OpenSverdDetails (selectedSverdListId);
	}

	public void OpenSverdList(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.SVERDLIST);
		selectedUnitListId = unitListId;
		GetComponent<UISverdList> ().OpenSverdList (selectedUnitListId);
	}

	public void OpenShop(List<int> itemIds){
		Time.timeScale = 0;
		CloseCurrentUI ();
		uiState.Push (UISTATE.SHOP);
		GetComponent<UIItemList> ().shopItemIds = itemIds;
		GetComponent<UIItemList> ().OpenBuyList ();
	}

	public void OpenHeadgearList(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = unitListId;
		selectedEquipSlot = EQUIPSLOT.HEAD;
		GetComponent<UIItemList> ().OpenItemList (unitListId,1);
	}

	public void OpenArmorList(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = unitListId;
		selectedEquipSlot = EQUIPSLOT.BODY;
		GetComponent<UIItemList> ().OpenItemList (unitListId,2);
	}

	public void OpenLWeaponList(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = unitListId;
		selectedEquipSlot = EQUIPSLOT.LHAND;
		GetComponent<UIItemList> ().OpenItemList (unitListId,3);
	}

	public void OpenRWeaponList(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = unitListId;
		selectedEquipSlot = EQUIPSLOT.RHAND;
		GetComponent<UIItemList> ().OpenItemList (unitListId,4);
	}

	public void OpenAcc1List(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = unitListId;
		selectedEquipSlot = EQUIPSLOT.ACC1;
		GetComponent<UIItemList> ().OpenItemList (unitListId,5);
	}

	public void OpenAcc2List(int unitListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = unitListId;
		selectedEquipSlot = EQUIPSLOT.ACC2;
		GetComponent<UIItemList> ().OpenItemList (unitListId,6);
	}

	public void OpenEngineList(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedSverdListId = sverdListId;
		selectedEquipSlot = EQUIPSLOT.ENGINE;
		GetComponent<UIItemList> ().OpenItemList (sverdListId,7);
	}

	public void OpenFrameList(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedSverdListId = sverdListId;
		selectedEquipSlot = EQUIPSLOT.FRAME;
		GetComponent<UIItemList> ().OpenItemList (sverdListId,8);
	}

	public void OpenLArmList(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedSverdListId = sverdListId;
		selectedEquipSlot = EQUIPSLOT.LARM;
		GetComponent<UIItemList> ().OpenItemList (sverdListId,9);
	}

	public void OpenRArmList(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedSverdListId = sverdListId;
		selectedEquipSlot = EQUIPSLOT.RARM;
		GetComponent<UIItemList> ().OpenItemList (sverdListId,10);
	}

	public void OpenBooster1List(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedUnitListId = sverdListId;
		selectedEquipSlot = EQUIPSLOT.BOOSTER1;
		GetComponent<UIItemList> ().OpenItemList (sverdListId,11);
	}

	public void OpenBooster2List(int sverdListId){
		CloseCurrentUI ();
		uiState.Push (UISTATE.EQUIPMENTLIST);
		selectedSverdListId = sverdListId;
		selectedEquipSlot = EQUIPSLOT.BOOSTER2;
		GetComponent<UIItemList> ().OpenItemList (sverdListId,12);
	}

	public void SverdClicked(int sverdListId){
		if (uiState.Peek () == UISTATE.SVERDLIST) {
			//check if already is equipping a sverd
			if (Persistence.Data ().pcData [selectedUnitListId].sverdListId != -1) { //unequip
				Persistence.Data().sverdData[Persistence.Data().pcData[selectedUnitListId].sverdListId].pcListId = -1;
			}
			Persistence.Data ().pcData [selectedUnitListId].sverdListId = sverdListId;

			//check if item is already equipped by someone else
			if (Persistence.Data ().sverdData [sverdListId].pcListId != -1) {
				Persistence.Data ().pcData [Persistence.Data().sverdData[sverdListId].pcListId].sverdListId = -1;
			}

			//equip to current unit
			Persistence.Data().sverdData[sverdListId].pcListId = selectedUnitListId;
			Close ();
		}
	}

	public void OpenSkillList(int unitListId){
		GetComponent<UISkillList> ().OpenSkillList (unitListId);
	}

	public void OpenSverdSkillList(int sverdListId){
		GetComponent<UISkillList> ().OpenSverdSkillList (sverdListId);
	}

	public void ItemClicked(int itemListId){
		if (uiState.Peek() == UISTATE.EQUIPMENTLIST) {
			//check if already is equipping item
			switch (selectedEquipSlot) {
			case EQUIPSLOT.HEAD:
				{
					if (Persistence.Data ().pcData [selectedUnitListId].equipHead != -1) { //unequip
						Persistence.Data().items[Persistence.Data().pcData[selectedUnitListId].equipHead].equippedBy = -1;
					}
					Persistence.Data ().pcData [selectedUnitListId].equipHead = itemListId;
				}
				break;
			case EQUIPSLOT.BODY:
				{
					if (Persistence.Data ().pcData [selectedUnitListId].equipBody != -1) { //unequip
						Persistence.Data().items[Persistence.Data().pcData[selectedUnitListId].equipBody].equippedBy = -1;
					}
					Persistence.Data ().pcData [selectedUnitListId].equipBody = itemListId;
				}
				break;
			case EQUIPSLOT.LHAND:
				{
					if (Persistence.Data ().pcData [selectedUnitListId].equipLArm != -1) { //unequip
						Persistence.Data().items[Persistence.Data().pcData[selectedUnitListId].equipLArm].equippedBy = -1;
					}
					Persistence.Data ().pcData [selectedUnitListId].equipLArm = itemListId;
				}
				break;
			case EQUIPSLOT.RHAND:
				{
					if (Persistence.Data ().pcData [selectedUnitListId].equipRArm != -1) { //unequip
						Persistence.Data().items[Persistence.Data().pcData[selectedUnitListId].equipRArm].equippedBy = -1;
					}
					Persistence.Data ().pcData [selectedUnitListId].equipRArm = itemListId;
				}
				break;
			case EQUIPSLOT.ACC1:
				{
					if (Persistence.Data ().pcData [selectedUnitListId].equipAcc1 != -1) { //unequip
						Persistence.Data().items[Persistence.Data().pcData[selectedUnitListId].equipAcc1].equippedBy = -1;
					}
					Persistence.Data ().pcData [selectedUnitListId].equipAcc1 = itemListId;
				}
				break;
			case EQUIPSLOT.ACC2:
				{
					if (Persistence.Data ().pcData [selectedUnitListId].equipAcc2 != -1) { //unequip
						Persistence.Data().items[Persistence.Data().pcData[selectedUnitListId].equipAcc2].equippedBy = -1;
					}
					Persistence.Data ().pcData [selectedUnitListId].equipAcc2 = itemListId;
				}
				break;
			case EQUIPSLOT.ENGINE:
				{
					if (Persistence.Data ().sverdData [selectedSverdListId].equipHead != -1) { //unequip
						Persistence.Data().items[Persistence.Data ().sverdData [selectedSverdListId].equipHead].equippedBy = -1;
					}
					Persistence.Data ().sverdData [selectedSverdListId].equipHead = itemListId;
				}
				break;
			case EQUIPSLOT.FRAME:
				{
					if (Persistence.Data ().sverdData [selectedSverdListId].equipBody != -1) { //unequip
						Persistence.Data().items[Persistence.Data ().sverdData [selectedSverdListId].equipBody].equippedBy = -1;
					}
					Persistence.Data ().sverdData [selectedSverdListId].equipBody = itemListId;
				}
				break;
			case EQUIPSLOT.LARM:
				{
					if (Persistence.Data ().sverdData [selectedSverdListId].equipLArm != -1) { //unequip
						Persistence.Data().items[Persistence.Data ().sverdData [selectedSverdListId].equipLArm].equippedBy = -1;
					}
					Persistence.Data ().sverdData [selectedSverdListId].equipLArm = itemListId;
				}
				break;
			case EQUIPSLOT.RARM:
				{
					if (Persistence.Data ().sverdData [selectedSverdListId].equipRArm != -1) { //unequip
						Persistence.Data().items[Persistence.Data ().sverdData [selectedSverdListId].equipRArm].equippedBy = -1;
					}
					Persistence.Data ().sverdData [selectedSverdListId].equipRArm = itemListId;
				}
				break;
			case EQUIPSLOT.BOOSTER1:
				{
					if (Persistence.Data ().sverdData [selectedSverdListId].equipAcc1 != -1) { //unequip
						Persistence.Data().items[Persistence.Data().sverdData[selectedSverdListId].equipAcc1].equippedBy = -1;
					}
					Persistence.Data ().sverdData [selectedSverdListId].equipAcc1 = itemListId;
				}
				break;
			case EQUIPSLOT.BOOSTER2:
				{
					if (Persistence.Data ().sverdData [selectedSverdListId].equipAcc2 != -1) { //unequip
						Persistence.Data().items[Persistence.Data().sverdData[selectedSverdListId].equipAcc2].equippedBy = -1;
					}
					Persistence.Data ().sverdData [selectedSverdListId].equipAcc2 = itemListId;
				}
				break;
			}

			//check if item is already equipped by someone else
			if (Persistence.Data ().items [itemListId].equippedBy != -1) {
				//unequip from that person
				switch (selectedEquipSlot) {
				case EQUIPSLOT.HEAD:
					{
						Persistence.Data ().pcData [Persistence.Data().items[itemListId].equippedBy].equipHead = -1;
						Persistence.UpdatePCData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.BODY:
					{
						Persistence.Data ().pcData [Persistence.Data().items[itemListId].equippedBy].equipBody = -1;
						Persistence.UpdatePCData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.LHAND:
					{
						if (Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipLArm == itemListId)
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipLArm = -1;
						else
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipRArm = -1;
						Persistence.UpdatePCData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.RHAND:
					{
						if (Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipLArm == itemListId)
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipLArm = -1;
						else
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipRArm = -1;
						Persistence.UpdatePCData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.ACC1:
					{
						if (Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 == itemListId)
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 = -1;
						else
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipAcc2 = -1;
						Persistence.UpdatePCData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.ACC2:
					{
						if (Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 == itemListId)
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 = -1;
						else
							Persistence.Data ().pcData [Persistence.Data ().items [itemListId].equippedBy].equipAcc2 = -1;
						Persistence.UpdatePCData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.ENGINE:
					{
						Persistence.Data ().sverdData [Persistence.Data().items[itemListId].equippedBy].equipHead = -1;
						Persistence.UpdateSverdData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.FRAME:
					{
						Persistence.Data ().sverdData [Persistence.Data().items[itemListId].equippedBy].equipBody = -1;
						Persistence.UpdateSverdData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.LARM:
					{
						if (Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipLArm == itemListId)
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipLArm = -1;
						else
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipRArm = -1;
						Persistence.UpdateSverdData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.RARM:
					{
						if (Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipLArm == itemListId)
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipLArm = -1;
						else
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipRArm = -1;
						Persistence.UpdateSverdData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.BOOSTER1:
					{
						if (Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 == itemListId)
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 = -1;
						else
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipAcc2 = -1;
						Persistence.UpdateSverdData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				case EQUIPSLOT.BOOSTER2:
					{
						if (Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 == itemListId)
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipAcc1 = -1;
						else
							Persistence.Data ().sverdData [Persistence.Data ().items [itemListId].equippedBy].equipAcc2 = -1;
						Persistence.UpdateSverdData (Persistence.Data ().items [itemListId].equippedBy);
					}
					break;
				}
			}

			//equip to current unit
			if ((selectedEquipSlot == EQUIPSLOT.HEAD) || (selectedEquipSlot == EQUIPSLOT.BODY) || (selectedEquipSlot == EQUIPSLOT.RHAND) || (selectedEquipSlot == EQUIPSLOT.LHAND) || (selectedEquipSlot == EQUIPSLOT.ACC1) || (selectedEquipSlot == EQUIPSLOT.ACC2)) {
				Persistence.Data ().items [itemListId].equippedBy = selectedUnitListId;
				Persistence.UpdatePCData (selectedUnitListId);
			} else {
				Persistence.Data ().items [itemListId].equippedBy = selectedSverdListId;
				Persistence.UpdateSverdData (selectedSverdListId);
			}
			Close ();
		}else if(uiState.Peek() == UISTATE.SHOP){
			if(GetComponent<UIItemList>().buyOrSell)
				GetComponent<UIConfirmation> ().ConfirmSell(itemListId);
			else
				GetComponent<UIConfirmation> ().ConfirmPurchase(itemListId);
		}
	}
}
