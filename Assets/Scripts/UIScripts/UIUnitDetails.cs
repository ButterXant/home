﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUnitDetails : MonoBehaviour {
	public GameObject unitDetailsPanel;
	public int currentListId;
	public Text unitName;
	public Text health;
	public Text mp;
	public Text speed;
	public Text cooldown;
	public Text attack;
	public Text magic;
	public Text armor;
	public Text mr;
	public Text heal;
	public Text range;
	public Text accuracy;
	public Text dodge;
	public Image portrait;
	public Button sverdButton;
	public Button headBtn;
	public Button bodyBtn;
	public Button lhandBtn;
	public Button rhandBtn;
	public Button acc1Btn;
	public Button acc2Btn;
	public Sprite defaultBtnImg;

	void Awake(){
		defaultBtnImg = sverdButton.image.sprite;
	}

	// Use this for initialization
	void Start () {
		unitDetailsPanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenUnitDetails(int unitListId){
		currentListId = unitListId;
		PCData pcData = Persistence.Data ().pcData [unitListId];
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [pcData.id];
		unitName.text = unitDBItem.name;
		health.text = pcData.health._base.ToString();
		mp.text = pcData.mp._base.ToString ();
		speed.text = pcData.speed._base.ToString ();
		cooldown.text = pcData.cooldown._base.ToString ();
		attack.text = pcData.attack._base.ToString ();
		magic.text = pcData.magic._base.ToString ();
		armor.text = pcData.armor._base.ToString ();
		mr.text = pcData.mr._base.ToString ();
		heal.text = pcData.heal._base.ToString ();
		range.text = pcData.rangeAtk._base.ToString ()+"/"+pcData.rangeHeal._base.ToString ();
		accuracy.text = pcData.accuracy._base.ToString ();
		dodge.text = pcData.dodge._base.ToString ();
		Sprite[] facesets = Resources.LoadAll<Sprite>("Facesets");
		portrait.sprite = UIMain.GetSpriteByName (facesets, Persistence.GetUnitDB ().items [Persistence.Data ().pcData [unitListId].id].facesetId);//facesets [Persistence.GetUnitDB ().items [Persistence.Data ().pcData [unitListId].id].facesetId];
		if (pcData.sverdListId != -1) {
			sverdButton.image.sprite = UIMain.GetSpriteByName (facesets,Persistence.GetUnitDB ().items [Persistence.Data ().sverdData [pcData.sverdListId].id].facesetId);
		}else{
			sverdButton.image.sprite = defaultBtnImg;
		}

		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		if (pcData.equipBody != -1) {
			bodyBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipBody].id].icon);
		}else{
			bodyBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipHead != -1) {
			headBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipHead].id].icon);
		}else{
			headBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipLArm != -1) {
			lhandBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipLArm].id].icon);
		}else{
			lhandBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipRArm != -1) {
			rhandBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipRArm].id].icon);
		}else{
			rhandBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipAcc1 != -1) {
			acc1Btn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipAcc1].id].icon);
		}else{
			acc1Btn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipAcc2 != -1) {
			acc2Btn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipAcc2].id].icon);
		}else{
			acc2Btn.image.sprite = defaultBtnImg;
		}
			
		unitDetailsPanel.SetActive (true);
	}

	public void CloseUnitDetails(){
		unitDetailsPanel.SetActive (false);
	}

	public void OpenHeadgearList(){
		GetComponent<UIMain> ().OpenHeadgearList (currentListId);
	}

	public void OpenLWeaponList(){
		GetComponent<UIMain> ().OpenLWeaponList (currentListId);
	}

	public void OpenRWeaponList(){
		GetComponent<UIMain> ().OpenRWeaponList (currentListId);
	}

	public void OpenArmorList(){
		GetComponent<UIMain> ().OpenArmorList (currentListId);
	}

	public void OpenAcc1List(){
		GetComponent<UIMain> ().OpenAcc1List (currentListId);
	}

	public void OpenAcc2List(){
		GetComponent<UIMain> ().OpenAcc2List (currentListId);
	}

	public void OpenSverdList(){
		GetComponent<UIMain> ().OpenSverdList(currentListId);
	}
}
