﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISverdList : MonoBehaviour {
	public List<GameObject> sverdListItems = new List<GameObject> ();
	public GameObject sverdListParent;
	public GameObject sverdListItemPrefab;
	public GameObject sverdListPanel;

	public void Awake(){
		sverdListPanel.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenSverdList(int unitListId){
		sverdListPanel.SetActive (true);

		//set content
		for (int i = 0; i < sverdListItems.Count; i++)
			Destroy (sverdListItems [i]);
		sverdListItems.Clear ();

		Sprite[] facesets = Resources.LoadAll<Sprite>("Facesets");
		for (int i = 0; i < Persistence.Data ().sverdData.Count; i++) {
			
			/*unitSlots.Add (Instantiate (unitListSlotPrefab));
			unitSlots [i].GetComponent<UIUnitListSlot> ().id = i;
			unitSlots [i].transform.SetParent (unitSlotsParent.transform,false);
			unitSlots [i].GetComponent<UIUnitListSlot> ().uiUnitList = this;*/
			GameObject sverdItem = Instantiate (sverdListItemPrefab);
			//Debug.Log (@"Facesets/" + UnitDatabase.FetchUnitByID (Persistence.Data ().charData [Persistence.Data ().party [i]].id).slug);
			//unitItem.GetComponent<Image>().sprite = Resources.LoadAll<Sprite> (@"Facesets/" + UnitDatabase.FetchUnitByID (Persistence.Data ().charData [Persistence.Data ().party [i]].id).slug)[1];
			sverdItem.GetComponent<UISverdListItem> ().uiSverdList = this;
			//if(Persistence.GetUnitDB ().items [Persistence.Data ().sverdData [i].id].facesetId != -1)
			sverdItem.GetComponent<UISverdListItem> ().portrait.sprite = UIMain.GetSpriteByName(facesets,Persistence.GetUnitDB ().items [Persistence.Data ().sverdData [i].id].facesetId);//facesets [Persistence.GetUnitDB ().items [Persistence.Data ().sverdData [i].id].facesetId];
			//unitItem.GetComponent<UIUnitListItem>().item = Persistence.Data().charData[Persistence.Data().party[i]];
			sverdItem.GetComponent<UISverdListItem> ().slot = i;
			//Debug.Log ("this is the slot number: " + i);
			sverdItem.transform.SetParent (sverdListParent.transform, false);
			sverdListItems.Add (sverdItem);
		}
	}

	public void CloseSverdList(){
		sverdListPanel.SetActive (false);
		//Persistence.InstantiateActiveUnits ();
	}
}
