﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISkillTree : MonoBehaviour {
	public GameObject unitSkillTreePanel;
	public UISkillTreeItem [] items;
	public int currentListId;

	// Use this for initialization
	void Start () {
		unitSkillTreePanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenSkillTree(int unitListId){
		unitSkillTreePanel.SetActive (true);
		currentListId = unitListId;
		PCData pcData = Persistence.Data ().pcData [unitListId];
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [pcData.id];

		//parse skill list
		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		//Debug.Log ("icons amount: " + icons.Length);
		string [] skillIds = unitDBItem.skills.Split(',');
		for (int i = 0; i < skillIds.Length; i++) {
			items [i].skillId = int.Parse (skillIds [i]);
			//Debug.Log ("skill name: " + Persistence.GetSkillDB ().items [items [i].skillId].icon);
			items [i].gameObject.GetComponent<Image> ().sprite = UIMain.GetSpriteByName (icons, Persistence.GetSkillDB ().items [items [i].skillId].icon); 
			items [i].points.text = pcData.allocatedSkills [i].ToString();
		}
	}

	public void ResetSkillTree(){
		PCData pcData = Persistence.Data ().pcData [currentListId];
		for (int i = 0; i < items.Length; i++) {
			pcData.allocatedSkills [i] = 0;
			items [i].points.text = pcData.allocatedSkills [i].ToString();
		}
	}

	public void CloseSkillTree(){
		Persistence.UpdatePCData (currentListId);
		unitSkillTreePanel.SetActive (false);
	}
}
