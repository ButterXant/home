﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class UITitleScreen : MonoBehaviour {
	public GameObject newGameBtn;
	public GameObject loadGameBtn;

	// Use this for initialization
	void Start () {
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {
			loadGameBtn.SetActive (false);
		} else {
			loadGameBtn.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadGame(){
		Persistence.Load ();
	}

	public void NewGame(){
		Persistence.ResetPlayerData ();
		SceneManager.LoadScene ("Core");
	}

	public void Retry(){
		Persistence.ResetPlayerData ();
		SceneManager.LoadScene ("Demo");
	}
}
