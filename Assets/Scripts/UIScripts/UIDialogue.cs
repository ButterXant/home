﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDialogue : MonoBehaviour {
	//ui elements
	public GameObject dialoguePanel;
	public Text characterName;
	public Text text;
	public GameObject [] choices;
	public GameObject nextButton;
	public Image faceset;

	//logic
	public bool occupied = false;
	public bool isTyping = false;
	public GameEvent gameEvent = null;

	void Awake(){
		dialoguePanel.SetActive (false);
		isTyping = false;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartDialogue(GameEvent gameEvent, string name, string face, string text, string optionA = null, string optionB = null, string optionC = null){
		this.gameEvent = gameEvent;
		this.gameEvent.issuingCommand = true;
		occupied = true;
		dialoguePanel.SetActive (true);

		Sprite[] facesets = Resources.LoadAll<Sprite>("Facesets");
		faceset.sprite = UIMain.GetSpriteByName (facesets,face);

		for (int i = 0; i < 3; i++) 
			choices [i].SetActive (false);
		isTyping = false;
		characterName.text = name;
		StartCoroutine (DialogueCoroutine (text, optionA, optionB, optionC));
	}

	IEnumerator DialogueCoroutine(string text, string optionA = null, string optionB = null, string optionC = null){
		isTyping = true;
		this.text.text = "";
		for (int i = 0; i < text.Length; i++) {
			if (!isTyping)
				break;

			this.text.text += text[i];
			yield return null;
		}

		this.text.text = text;

		if (optionA != null) {
			choices [0].SetActive (true);
			Text textComponent = choices [0].transform.Find ("Text").gameObject.GetComponent<Text> ();
			textComponent.text = "";
			for (int i = 0; i < optionA.Length; i++) {
				if (!isTyping)
					break;

				textComponent.text += optionA[i];
				yield return null;
			}

			textComponent.text = optionA;
		}

		if (optionB != null) {
			choices [1].SetActive (true);
			Text textComponent = choices [1].transform.Find ("Text").gameObject.GetComponent<Text> ();
			textComponent.text = "";
			for (int i = 0; i < optionB.Length; i++) {
				if (!isTyping)
					break;

				textComponent.text += optionB[i];
				yield return null;
			}

			textComponent.text = optionB;
		}

		if (optionC != null) {
			choices [2].SetActive (true);
			Text textComponent = choices [2].transform.Find ("Text").gameObject.GetComponent<Text> ();
			textComponent.text = "";
			for (int i = 0; i < optionC.Length; i++) {
				if (!isTyping)
					break;

				textComponent.text += optionC[i];
				yield return null;
			}

			textComponent.text = optionC;
		}

		isTyping = false;
		yield return null;
	}

	public void StopDialogue(){
		this.gameEvent.issuingCommand = false;
		this.gameEvent = null;
		dialoguePanel.SetActive (false);
		for (int i = 0; i < 3; i++) 
			choices [i].SetActive (false);
		isTyping = false;
		occupied = false;
	}

	public void Next(){
		if (isTyping) {
			isTyping = false;
			return;
		}

		//check if choices are prompted
		if (choices[0].activeSelf)
			return;

		StopDialogue ();
	}

	public void Choice (int choiceId){
		gameEvent.branch = choiceId;
		StopDialogue ();
	}
}
