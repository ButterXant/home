﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIPCControl : MonoBehaviour, IPointerClickHandler {
	public Image portrait;
	public Image healthBar;
	public Text healthText;
	public Image mpBar;
	public Text mpText;
	public Image cooldownBar;
	public Button vanguardBtn;
	public Button sentinelBtn;
	public Button medicBtn;
	public Toggle vanguardToggle;
	public Toggle sentinelToggle;
	public Toggle medicToggle;
	public PC unitTarget;
	public int controlId;
	public Image fuelBar;
	public bool locked = false;
	public UIPCControlList uiPCControlList;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (unitTarget != null) {
			healthBar.fillAmount = unitTarget.health.ToPercent(); 
			healthText.text = unitTarget.health._current.ToString();
			mpBar.fillAmount = unitTarget.mp.ToPercent ();
			mpText.text = unitTarget.mp._current.ToString();
			if (unitTarget.fuel._base == 0.0f)
				fuelBar.fillAmount = 0.0f;
			else
				fuelBar.fillAmount = unitTarget.fuel.ToPercent ();
			cooldownBar.fillAmount = unitTarget.cooldown.ToPercent(); 
		}
	}


	public void CallSverd(){
		if (locked)
			return;

		if(unitTarget != null){
			if (unitTarget.pilotSverdId != -1) {
				if ((unitTarget.fuel._base != 0.0f) && (unitTarget.fuel.ToPercent () > 0.1f)) {
					locked = true;
					//can call sverd
					StartCoroutine(CallSverdRoutine());
				}
			}
		}
	}

	public void OpenSkillList(){
		if (unitTarget == null)
			return;
		if (unitTarget.isDown)
			return;
		Time.timeScale = 0;
		Control.SelectUnit(unitTarget.gameObject);
		uiPCControlList.GetComponent<UIMain> ().OpenSkillList (Persistence.Data().party[controlId]);
	}

	IEnumerator CallSverdRoutine(){
		Persistence.Data().sverdData[Persistence.Data().pcData[Persistence.Data().party[controlId]].sverdListId].fuel._current = unitTarget.fuel._current;
		Vector3 pos = unitTarget.transform.position;
		Vector3 ori = unitTarget.GetComponent<Unit> ().sprite.gameObject.transform.localScale;
		//unitTarget.GetComponent<Unit> ().state.Push (UNITSTATE.ANIMATING);
		unitTarget.GetComponent<Unit>().sprite.GetComponent<Animator> ().Play ("Exit");
		yield return new WaitForSeconds (1.0f);
		UnitMgr.DestroyUnit (unitTarget);
		GameObject newSverd = UnitMgr.InstantiateSverd(Persistence.Data().sverdData[Persistence.Data().pcData[Persistence.Data().party[controlId]].sverdListId].id,pos,ori,controlId);
		Control.SelectUnit(newSverd.GetComponentInChildren<Unit>().gameObject);
		//newSverd.GetComponentInChildren<Unit> ().state.Push (UNITSTATE.ANIMATING);
		newSverd.GetComponentInChildren<Unit>().sprite.GetComponent<Animator> ().Play ("Enter");
		if (newSverd.GetComponentInChildren<Unit> ().fuel.ToPercent () > 0.5f) {
			UISideDialogueList.AddSideDialogue (null,"Your fuel is above 50%. Increase your damage output. <sprite=\"outputIcon\" name=\"outputIcon\">");
		}
		Destroy (this.gameObject);
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		if (!Control.selectingSkillTarget) {
			Control.SelectUnit (unitTarget.gameObject);
		}
	}
	#endregion
}
