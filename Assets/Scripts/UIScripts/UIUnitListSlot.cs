﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIUnitListSlot : MonoBehaviour, IDropHandler {
	public int id;
	public UIUnitList uiUnitList;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region IDropHandler implementation
	public void OnDrop(PointerEventData eventData){
		UIUnitListItem droppedItem = eventData.pointerDrag.GetComponent<UIUnitListItem> ();
		if (droppedItem.slot != id) {
			int temp = Persistence.Data ().party [id];
			Persistence.Data ().party [id] = Persistence.Data ().party [droppedItem.slot];
			Persistence.Data ().party [droppedItem.slot] = temp;
			Transform item = this.transform.GetChild (0);
			item.GetComponent<UIUnitListItem> ().slot = droppedItem.slot;
			item.transform.SetParent (uiUnitList.unitSlots [droppedItem.slot].transform);
			item.transform.position = uiUnitList.unitSlots [droppedItem.slot].transform.position;
			droppedItem.slot = id;
			droppedItem.transform.SetParent (this.transform);
			droppedItem.transform.position = this.transform.position;
		}
	}

	#endregion
}
