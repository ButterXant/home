﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISkillTreeItem : MonoBehaviour, IPointerClickHandler{
	public int skillListId;
	public int skillId;
	public Text points;
	public UIMain uiMain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		SkillDBItem skill = Persistence.GetSkillDB ().items [skillId];

		int nAllocated = 0;
		for (int k = 0; k < 39; k++)
			nAllocated += Persistence.Data().pcData[uiMain.selectedUnitListId].allocatedSkills[k];

		//if (eventData.button == PointerEventData.InputButton.Left) {

		if(Persistence.Data().pcData [uiMain.selectedUnitListId].allocatedSkills[skillListId] == skill.maxlvl) return;
		if(nAllocated == Persistence.Data().pcData [uiMain.selectedUnitListId].lvl) return;

		int panel = skillListId / 13;
		int rem = skillListId % 13;

		int prereq = 0;
		if(0 <= rem && rem <= 1){
			prereq = 0;
		}else if(2 <= rem && rem <= 4){
			prereq = 5;
		}else if(rem == 5){
			prereq = 10;
		}else if(6 <= rem && rem <= 8){
			prereq = 11;
		}else if(9 <= rem && rem <= 11){
			prereq = 16;
		}else if(rem == 12){
			prereq = 21;
		}
		Debug.Log ("prereq: " + prereq);

		int accumulated = 0;
		int startSkillListId = panel * 13;
		int endSkillListId = (panel + 1) * 13;
		for(int i = startSkillListId; i < endSkillListId; i++){
			//Debug.Log ("allocated "+ (panel * 13 + i) + ": "+ Persistence.Data().pcData [uiMain.selectedUnitListId].allocatedSkills[panel * 13 + i]);
			accumulated += Persistence.Data().pcData [uiMain.selectedUnitListId].allocatedSkills[i];
		}

		//Debug.Log ("Skill prerequisite: " + skill.prerequisite);
		Debug.Log ("accumulated: " + accumulated);
		if(accumulated < prereq) return;

		Persistence.Data().pcData [uiMain.selectedUnitListId].allocatedSkills[skillListId]++;
		Debug.Log ("skill List IDictionary:" + skillListId);
		points.text = Persistence.Data().pcData [uiMain.selectedUnitListId].allocatedSkills[skillListId].ToString();
		/*} else if (eventData.button == PointerEventData.InputButton.Right){
			if(Persistence.Data().charData [Persistence.Data().party [UIUnits.selectedSlot]].allocatedSkills[skillId] == 0) return;

			int panel = skillId / 13;
			int rem = skillId % 13;

			int prereq = 0;
			if(0 <= rem && rem <= 1){
				prereq = 2;
			}else if(2 <= rem && rem <= 4){
				prereq = 5;
			}else if(rem == 5){
				prereq = 6;
			}else if(6 <= rem && rem <= 8){
				prereq = 9;
			}else if(9 <= rem && rem <= 11){
				prereq = 12;
			}else if(rem == 12){
				prereq = 13;
			}

			int j = prereq;
			int req = 0;
			bool allowed = false;
			bool found = false;
			while(!found && j < 13){
				if(Persistence.Data().charData [Persistence.Data().party [UIUnits.selectedSlot]].allocatedSkills[panel * 13 + j] != 0) {
					found = true;
					SkillData skill2 = SkillDatabase.FetchSkillByID(panel * 13 + j);
					req = skill2.Prerequisite;
				}else j++;
			}

			if(found){
				int accumulated = 0;
				for(int i = 0; i < prereq; i++)
					accumulated += Persistence.Data().charData [Persistence.Data().party [UIUnits.selectedSlot]].allocatedSkills[panel * 13 + i];

				if(accumulated - 1 >= req) allowed = true;
			}else allowed = true;

			if(!allowed) return;

			Persistence.Data().charData [Persistence.Data().party [UIUnits.selectedSlot]].allocatedSkills[skillId]--;
			points.text = Persistence.Data().charData [Persistence.Data().party [UIUnits.selectedSlot]].allocatedSkills[skillId].ToString();
		}*/
	}
	#endregion
}
