﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIConfirmation : MonoBehaviour {
	public GameObject confirmationPanel;
	public Button okButton;
	public Button cancelButton;
	public Text amount;
	public Text price;
	public Scrollbar amountScroll;

	//public delegate void ConfirmAction();
	//public ConfirmAction 

	public void Awake(){
		confirmationPanel.SetActive (false);
	}

	public void ConfirmPurchase(int purchasedId){
		ItemDBItem itemDBItem = Persistence.GetItemDB ().items [purchasedId];
		int currentGold = Persistence.Data ().gold;
		if(itemDBItem.buyFor == 0)
			amountScroll.numberOfSteps = 2;
		else
			amountScroll.numberOfSteps = currentGold / itemDBItem.buyFor + 1;

		confirmationPanel.SetActive (true);

		amountScroll.onValueChanged.RemoveAllListeners ();
		amountScroll.onValueChanged.AddListener ((input)=>{
			int currentAmount = 0;
			if(itemDBItem.buyFor != 0)
				currentAmount = Mathf.FloorToInt (amountScroll.value * currentGold / itemDBItem.buyFor);
			else
				currentAmount = Mathf.FloorToInt (amountScroll.value);
			amount.text = currentAmount.ToString ();
			price.text = (currentAmount * itemDBItem.buyFor).ToString();

		});

		okButton.onClick.RemoveAllListeners ();
		okButton.onClick.AddListener (()=>{
			int currentAmount = 0;
			if(itemDBItem.buyFor != 0)
				currentAmount = Mathf.FloorToInt (amountScroll.value * currentGold / itemDBItem.buyFor);
			else 
				currentAmount = Mathf.FloorToInt (amountScroll.value);
			int currentPrice = currentAmount * itemDBItem.buyFor;
			Persistence.SubtractGold(currentPrice);
			for(int i=0; i<currentAmount;i++)
				Persistence.AddItem (purchasedId);
			UIMain.instance.GetComponent<UIItemList>().OpenBuyList();
			confirmationPanel.SetActive (false);
		});
		cancelButton.onClick.RemoveAllListeners ();
		cancelButton.onClick.AddListener (()=>{
			confirmationPanel.SetActive (false);
		});

		amountScroll.value = 0;
		price.text = "0";
		amount.text = "0";
	}

	public void ConfirmSell(int sellId){
		ItemDBItem itemDBItem = Persistence.GetItemDB ().items [Persistence.Data ().items [sellId].id];
		int currentHeld = Persistence.Data ().items [sellId].amount;
		amountScroll.numberOfSteps = currentHeld + 1;

		confirmationPanel.SetActive (true);

		amountScroll.onValueChanged.RemoveAllListeners ();
		amountScroll.onValueChanged.AddListener ((input)=>{
			int currentAmount = Mathf.FloorToInt (amountScroll.value * currentHeld);
			amount.text = currentAmount.ToString ();
			price.text = (currentAmount * itemDBItem.sellFor).ToString();
		});

		okButton.onClick.RemoveAllListeners ();
		okButton.onClick.AddListener (()=>{
			int currentAmount = Mathf.FloorToInt (amountScroll.value * currentHeld);
			int currentPrice =  currentAmount * itemDBItem.sellFor;
			Persistence.AddGold(currentPrice);
			for(int i=0; i<currentAmount;i++)
				Persistence.RemoveItem(sellId);
			UIMain.instance.GetComponent<UIItemList>().OpenSellList();
			confirmationPanel.SetActive (false);
		});
		cancelButton.onClick.RemoveAllListeners ();
		cancelButton.onClick.AddListener (()=>{
			confirmationPanel.SetActive (false);
		});

		amountScroll.value = 0;
		price.text = "0";
		amount.text = "0";
	}
}
