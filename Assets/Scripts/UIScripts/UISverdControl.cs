﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISverdControl : MonoBehaviour ,IPointerClickHandler{
	public Image portrait;
	public Image healthBar;
	public Text healthText;
	public Image mpBar;
	public Text mpText;
	public Image cooldownBar;
	public Sverd unitTarget;
	public int controlId;
	public Image fuelBar;
	public bool locked = false;
	public UIPCControlList uiPCControlList;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (unitTarget != null) {
			healthBar.fillAmount = unitTarget.health.ToPercent(); 
			healthText.text = unitTarget.health._current.ToString ();
			if (unitTarget.fuel._base == 0.0f)
				fuelBar.fillAmount = 0.0f;
			else
				fuelBar.fillAmount = unitTarget.fuel.ToPercent ();
			cooldownBar.fillAmount = unitTarget.cooldown.ToPercent(); 

			if(((unitTarget.fuel._current <= 0) || (unitTarget.isDown)) && (!locked)){
				EjectPilot ();
			}
		}
	}

	public void OpenSkillList(){
		if (unitTarget == null)
			return;
		if (unitTarget.isDown)
			return;

		Time.timeScale = 0;
		//Control.SelectUnit(unitTarget.gameObject);
		uiPCControlList.GetComponent<UIMain> ().OpenSverdSkillList (Persistence.Data().pcData[Persistence.Data().party[controlId]].sverdListId);
	}

	public void EjectPilot(){
		if (locked)
			return;

		if(unitTarget != null){
			locked = true;
			StartCoroutine (EjectPilotRoutine ());
		}
	}

	public void AdjustPowerOutput(float value){
		unitTarget.powerOutput = 1.0f + value;
		Control.SelectUnit (unitTarget.gameObject);
	}

	public IEnumerator EjectPilotRoutine(){
		Vector3 pos = unitTarget.transform.position;
		Vector3 ori = unitTarget.GetComponent<Unit>().sprite.gameObject.transform.localScale;
		//unitTarget.GetComponent<Unit> ().state.Push (UNITSTATE.ANIMATING);
		unitTarget.GetComponent<Unit>().sprite.GetComponent<Animator> ().Play ("Exit");
		yield return new WaitForSeconds (2.0f);
		UnitMgr.DestroyUnit (unitTarget);
		GameObject newPC = UnitMgr.InstantiateUnit(Persistence.Data().pcData[Persistence.Data().party[controlId]].id,pos,ori,controlId);
		Control.SelectUnit(newPC.GetComponentInChildren<Unit>().gameObject);
		//newPC.GetComponentInChildren<Unit> ().state.Push (UNITSTATE.ANIMATING);
		newPC.GetComponentInChildren<Unit>().sprite.GetComponent<Animator> ().Play ("Enter");
		Destroy (this.gameObject);
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		if (!Control.selectingSkillTarget) {
			Control.SelectUnit (unitTarget.gameObject);
		}
	}
	#endregion
}
