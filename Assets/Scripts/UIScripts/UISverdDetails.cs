﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISverdDetails : MonoBehaviour {
	public GameObject unitDetailsPanel;
	public int currentListId;
	public Text unitName;
	public Text health;
	public Text mp;
	public Text speed;
	public Text cooldown;
	public Text attack;
	public Text magic;
	public Text armor;
	public Text mr;
	public Text heal;
	public Text range;
	public Text accuracy;
	public Text dodge;
	public Image portrait;
	public Button headBtn;
	public Button bodyBtn;
	public Button lhandBtn;
	public Button rhandBtn;
	public Button acc1Btn;
	public Button acc2Btn;
	public Sprite defaultBtnImg;

	void Awake(){
		defaultBtnImg = headBtn.image.sprite;
	}

	// Use this for initialization
	void Start () {
		unitDetailsPanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenSverdDetails(int sverdListId){
		currentListId = sverdListId;
		SverdData pcData = Persistence.Data ().sverdData [sverdListId];
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [pcData.id];
		unitName.text = unitDBItem.name;
		health.text = pcData.health._base.ToString();
		mp.text = pcData.fuel._base.ToString ();
		speed.text = pcData.speed._base.ToString ();
		cooldown.text = pcData.cooldown._base.ToString ();
		attack.text = pcData.attack._base.ToString ();
		magic.text = pcData.magic._base.ToString ();
		armor.text = pcData.armor._base.ToString ();
		mr.text = pcData.mr._base.ToString ();
		heal.text = pcData.heal._base.ToString ();
		range.text = pcData.rangeAtk._base.ToString ()+"/"+pcData.rangeHeal._base.ToString ();
		accuracy.text = pcData.accuracy._base.ToString ();
		dodge.text = pcData.dodge._base.ToString ();
		Sprite[] facesets = Resources.LoadAll<Sprite>("Facesets");
		portrait.sprite = UIMain.GetSpriteByName (facesets, Persistence.GetUnitDB ().items [Persistence.Data ().sverdData [sverdListId].id].facesetId);//facesets [Persistence.GetUnitDB ().items [Persistence.Data ().sverdData [sverdListId].id].facesetId];

		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		if (pcData.equipBody != -1) {
			bodyBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipBody].id].icon);
		}else{
			bodyBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipHead != -1) {
			headBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipHead].id].icon);
		}else{
			headBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipLArm != -1) {
			lhandBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipLArm].id].icon);
		}else{
			lhandBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipRArm != -1) {
			rhandBtn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipRArm].id].icon);
		}else{
			rhandBtn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipAcc1 != -1) {
			acc1Btn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipAcc1].id].icon);
		}else{
			acc1Btn.image.sprite = defaultBtnImg;
		}

		if (pcData.equipAcc2 != -1) {
			acc2Btn.image.sprite = UIMain.GetSpriteByName (icons,Persistence.GetItemDB ().items [Persistence.Data ().items [pcData.equipAcc2].id].icon);
		}else{
			acc2Btn.image.sprite = defaultBtnImg;
		}

		unitDetailsPanel.SetActive (true);
	}

	public void CloseSverdDetails(){
		unitDetailsPanel.SetActive (false);
	}

	public void OpenEngineList(){
		GetComponent<UIMain> ().OpenEngineList (currentListId);
	}

	public void OpenLArmamentList(){
		GetComponent<UIMain> ().OpenLArmList (currentListId);
	}

	public void OpenRArmamentList(){
		GetComponent<UIMain> ().OpenRArmList (currentListId);
	}

	public void OpenFrameList(){
		GetComponent<UIMain> ().OpenFrameList (currentListId);
	}

	public void OpenBooster1List(){
		GetComponent<UIMain> ().OpenBooster1List (currentListId);
	}

	public void OpenBooster2List(){
		GetComponent<UIMain> ().OpenBooster2List (currentListId);
	}
}
