﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISideDialogueList : MonoBehaviour {
	public List<GameObject> sideDialogues = new List<GameObject>();
	public GameObject sideDialogueFrame;
	public GameObject sideDialoguePrefab;
	public static UISideDialogueList instance;

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//test
		if (Input.GetKeyUp ("q")) {
			AddSideDialogue (null, "lala");
		}
	}

	public static void AddSideDialogue(string portrait, string dialogue){
		for (int i = 0; i < instance.sideDialogues.Count; i++) {
			GameObject currDialogue = instance.sideDialogues [i];
			RectTransform currDialogueTransform = currDialogue.GetComponent<RectTransform>();
			currDialogueTransform.localPosition = new Vector3 (currDialogueTransform.localPosition.x, currDialogueTransform.localPosition.y + 64, currDialogueTransform.localPosition.z);
		}

		GameObject newSideDialogue = Instantiate (instance.sideDialoguePrefab);
		instance.sideDialogues.Add (newSideDialogue);
		SideDialogue sideDialogue = newSideDialogue.GetComponent<SideDialogue> ();
		sideDialogue.text.text = dialogue;
		if (portrait == null) {
			sideDialogue.portraitPanel.SetActive (false);
		} else {
			sideDialogue.portraitPanel.SetActive (true);
			Sprite[] facesets = Resources.LoadAll<Sprite>("Facesets");
			sideDialogue.portraitImage.sprite = UIMain.GetSpriteByName (facesets,portrait);
		}
		//instance.sideDialogueFrame.transform.position = new Vector3 (0.0f,0.0f,instance.sideDialogueFrame.transform.position.y + 1.0f);
		newSideDialogue.transform.SetParent (instance.sideDialogueFrame.transform,false);
	}

	public static void RemoveSideDialogue(){
		instance.sideDialogues.RemoveAt (0);
	}
}
