﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISkillList : MonoBehaviour {
	public List<GameObject> items = new List<GameObject> ();
	public GameObject skillListParent;
	public GameObject skillListItemPrefab;
	public GameObject skillListPanel;
	public GameObject cancelBtn;

	public void Awake(){
		skillListPanel.SetActive (false);
		cancelBtn.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenSverdSkillList(int sverdListId){
		skillListPanel.SetActive (true);

		//set content
		for (int i = 0; i < items.Count; i++)
			Destroy (items [i]);
		items.Clear ();

		SverdData sverdData = Persistence.Data ().sverdData [sverdListId];
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [sverdData.id];
		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		string[] skillIds = unitDBItem.skills.Split (',');
		for (int i = 0; i < skillIds.Length; i++) {
			SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [int.Parse(skillIds[i])];
			if (skillDBItem.skilltype == 0) { //passive to probably unlock more skills
				string [] effects = skillDBItem.effect.Split(',');
				for (int j = 0; j < effects.Length; j++) {
					string [] param = effects[j].Split(':');
					if (param [0] == "unlock") {
						string[] skillIdToUnlock = param [1].Split ('_');
						for (int k = 0; k < skillIdToUnlock.Length; k++) {
							SkillDBItem unlockedSkill = Persistence.GetSkillDB ().items [int.Parse (skillIdToUnlock[k])];
							if (unlockedSkill.skilltype == 1) {
								int curItemCount = items.Count;
								items.Add (Instantiate (skillListItemPrefab));
								items [curItemCount].GetComponent<UISkillListItem> ().skillId = unlockedSkill.id;
								items [curItemCount].GetComponent<UISkillListItem> ().skillName.text = unlockedSkill.name;
								items [curItemCount].GetComponent<UISkillListItem> ().skillIcon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetSkillDB ().items [unlockedSkill.id].icon); 
								items [curItemCount].GetComponent<UISkillListItem> ().skillDescription.text = unlockedSkill.description;
								items [curItemCount].GetComponent<UISkillListItem> ().skillCost.text = unlockedSkill.cost.ToString ();
								items [curItemCount].transform.SetParent (skillListParent.transform, false);
								items [curItemCount].GetComponent<UISkillListItem> ().uiSkillList = this;
							}
						}
					}
				}
			} else if (skillDBItem.skilltype == 1) { //active skills
				int curItemCount = items.Count;
				items.Add (Instantiate (skillListItemPrefab));
				items [curItemCount].GetComponent<UISkillListItem> ().skillId = skillDBItem.id;
				items [curItemCount].GetComponent<UISkillListItem> ().skillName.text = skillDBItem.name;
				items [curItemCount].GetComponent<UISkillListItem> ().skillIcon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetSkillDB ().items [skillDBItem.id].icon);
				items [curItemCount].GetComponent<UISkillListItem> ().skillDescription.text = skillDBItem.description;
				items [curItemCount].GetComponent<UISkillListItem> ().skillCost.text = skillDBItem.cost.ToString();
				items [curItemCount].transform.SetParent (skillListParent.transform, false);
				items [curItemCount].GetComponent<UISkillListItem> ().uiSkillList = this;
			}
		}
	}

	public void OpenSkillList(int unitListId){
		skillListPanel.SetActive (true);

		//set content
		for (int i = 0; i < items.Count; i++)
			Destroy (items [i]);
		items.Clear ();

		PCData pcData = Persistence.Data ().pcData [unitListId];
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [pcData.id];
		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		string[] skillIds = unitDBItem.skills.Split (',');
		for (int i = 0; i < skillIds.Length; i++) {
			SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [int.Parse(skillIds[i])];
			//if (pcData.allocatedSkills [i] > 0) {
				if (skillDBItem.skilltype == 0) { //passive to probably unlock more skills
					string [] effects = skillDBItem.effect.Split(',');
					for (int j = 0; j < effects.Length; j++) {
						string [] param = effects[j].Split(':');
						if (param [0] == "unlock") {
							string[] skillIdToUnlock = param [1].Split ('_');
							for (int k = 0; k < skillIdToUnlock.Length; k++) {
								SkillDBItem unlockedSkill = Persistence.GetSkillDB ().items [int.Parse (skillIdToUnlock[k])];
								if (unlockedSkill.skilltype == 1) {
									int curItemCount = items.Count;
									items.Add (Instantiate (skillListItemPrefab));
									items [curItemCount].GetComponent<UISkillListItem> ().skillId = unlockedSkill.id;
									items [curItemCount].GetComponent<UISkillListItem> ().skillName.text = unlockedSkill.name;
									items [curItemCount].GetComponent<UISkillListItem> ().skillIcon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetSkillDB ().items [unlockedSkill.id].icon);
									items [curItemCount].GetComponent<UISkillListItem> ().skillDescription.text = unlockedSkill.description;
									items [curItemCount].GetComponent<UISkillListItem> ().skillCost.text = unlockedSkill.cost.ToString ();
									items [curItemCount].transform.SetParent (skillListParent.transform, false);
									items [curItemCount].GetComponent<UISkillListItem> ().uiSkillList = this;
								}
							}
						}
					}
				} else if (skillDBItem.skilltype == 1) { //active skills
					int curItemCount = items.Count;
					items.Add (Instantiate (skillListItemPrefab));
					items [curItemCount].GetComponent<UISkillListItem> ().skillId = skillDBItem.id;
					items [curItemCount].GetComponent<UISkillListItem> ().skillName.text = skillDBItem.name;
					items [curItemCount].GetComponent<UISkillListItem> ().skillIcon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetSkillDB ().items [skillDBItem.id].icon);
					items [curItemCount].GetComponent<UISkillListItem> ().skillDescription.text = skillDBItem.description;
					items [curItemCount].GetComponent<UISkillListItem> ().skillCost.text = skillDBItem.cost.ToString();
					items [curItemCount].transform.SetParent (skillListParent.transform, false);
					items [curItemCount].GetComponent<UISkillListItem> ().uiSkillList = this;
				}
			//}
		}
	}

	public void CloseSkillList(){
		skillListPanel.SetActive (false);
	}
}
