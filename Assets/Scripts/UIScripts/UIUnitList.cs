﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIUnitList : MonoBehaviour {
	public List<GameObject> unitSlots = new List<GameObject> ();
	public GameObject unitSlotsParent;
	public GameObject unitListSlotPrefab;
	public GameObject unitListItemPrefab;
	public GameObject unitListPanel;

	public void Awake(){
		unitListPanel.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenUnitList(){
		Persistence.UpdateActiveUnitsPersistentData ();

		unitListPanel.SetActive (true);

		//set content
		for (int i = 0; i < unitSlots.Count; i++)
			Destroy (unitSlots [i]);
		unitSlots.Clear ();

		Sprite[] facesets = Resources.LoadAll<Sprite>("Facesets");
		for (int i = 0; i < Persistence.Data ().party.Count; i++) {
			
			unitSlots.Add (Instantiate (unitListSlotPrefab));
			unitSlots [i].GetComponent<UIUnitListSlot> ().id = i;
			unitSlots [i].transform.SetParent (unitSlotsParent.transform,false);
			unitSlots [i].GetComponent<UIUnitListSlot> ().uiUnitList = this;
			GameObject unitItem = Instantiate (unitListItemPrefab);
			//Debug.Log (@"Facesets/" + UnitDatabase.FetchUnitByID (Persistence.Data ().charData [Persistence.Data ().party [i]].id).slug);
			//unitItem.GetComponent<Image>().sprite = Resources.LoadAll<Sprite> (@"Facesets/" + UnitDatabase.FetchUnitByID (Persistence.Data ().charData [Persistence.Data ().party [i]].id).slug)[1];
			unitItem.GetComponent<UIUnitListItem> ().uiUnitList = this;
			unitItem.GetComponent<UIUnitListItem> ().portrait.sprite = UIMain.GetSpriteByName(facesets,Persistence.GetUnitDB ().items [Persistence.Data ().pcData [Persistence.Data ().party [i]].id].facesetId);//facesets [Persistence.GetUnitDB ().items [Persistence.Data ().pcData [Persistence.Data ().party [i]].id].facesetId];
			//unitItem.GetComponent<UIUnitListItem>().item = Persistence.Data().charData[Persistence.Data().party[i]];
			unitItem.GetComponent<UIUnitListItem> ().slot = i;
			//Debug.Log ("this is the slot number: " + i);
			unitItem.transform.SetParent (unitSlots [i].transform, false);
		}
	}

	public void CloseUnitList(){
		unitListPanel.SetActive (false);
		//Persistence.InstantiateActiveUnits ();
	}
}
