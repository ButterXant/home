﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SideDialogue : MonoBehaviour {
	private float duration  = 6.0f;
	private float halftime = 5.0f;
	private float lifetime = 0.0f;
	public GameObject mainPanel;
	public GameObject portraitPanel;
	public Image portraitImage;
	public GameObject textPanel;
	public TextMeshProUGUI text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		lifetime += Time.deltaTime;
		//rectTransform.localPosition = new Vector3 (rectTransform.position.x, rectTransform.position.y, rectTransform.position.z + Time.deltaTime * 4);
		if(lifetime >= halftime){
			RectTransform rectTransform = GetComponent<RectTransform>();
			rectTransform.localPosition = new Vector3 (rectTransform.localPosition.x,rectTransform.localPosition.y + Time.deltaTime * 100,rectTransform.localPosition.z);
			rectTransform.localScale = new Vector3 (1 - ((lifetime - halftime) /(duration- halftime)), 1 - ((lifetime - halftime) / (duration- halftime)), 1 - ((lifetime - halftime) / (duration- halftime)));
			mainPanel.GetComponent<Image> ().color = new Color (1.0f, 1.0f,1.0f,1 - ((lifetime -halftime)/ (duration-halftime)));
			portraitPanel.GetComponent<Image> ().color = new Color (1.0f, 1.0f,1.0f,1 - ((lifetime -halftime)/ (duration-halftime)));
			portraitImage.color = new Color (1.0f, 1.0f,1.0f,1 - ((lifetime -halftime)/ (duration-halftime)));
			textPanel.GetComponent<Image> ().color = new Color (1.0f, 1.0f,1.0f,1 - ((lifetime -halftime)/ (duration-halftime)));
			text.color = new Color (text.color.r,text.color.g,text.color.b,1 - ((lifetime -halftime)/ (duration-halftime)));
			//text.text = "lalala <sprite=\"UI\" name=\"UI_Sentinel\">X<sprite=\"UI\" name=\"UI_Vanguard\">";
		}
		if (lifetime > duration) {
			UISideDialogueList.RemoveSideDialogue ();
			Destroy (this.gameObject);
		}
	}
}
