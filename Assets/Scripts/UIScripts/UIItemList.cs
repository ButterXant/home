﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemList : MonoBehaviour {
	public List<GameObject> items = new List<GameObject> ();
	public GameObject itemListParent;
	public GameObject itemListItemPrefab;
	public GameObject itemListPanel;
	public GameObject buyButton;
	public GameObject sellButton;
	public Text gold;

	//for shop
	public List<int> shopItemIds;
	public bool buyOrSell = false;

	public void Awake(){
		itemListPanel.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenBuyList(){
		gold.text = "Gold: " + Persistence.Data ().gold.ToString ();
		itemListPanel.SetActive (true);
		buyOrSell = false;

		buyButton.SetActive (true);
		buyButton.GetComponent<Button> ().interactable = false;
		sellButton.SetActive (true);
		sellButton.GetComponent<Button> ().interactable = true;

		//set content
		for (int i = 0; i < items.Count; i++)
			Destroy (items [i]);
		items.Clear ();

		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		for (int i = 0; i < shopItemIds.Count; i++) {
			ItemDBItem itemDBItem = Persistence.GetItemDB ().items [shopItemIds[i]];
			int curItemCount = items.Count;
			items.Add (Instantiate (itemListItemPrefab));
			items [curItemCount].GetComponent<UIItemListItem> ().id = itemDBItem.id;
			items [curItemCount].GetComponent<UIItemListItem> ().itemName.text = itemDBItem.name;
			items [curItemCount].GetComponent<UIItemListItem> ().icon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetItemDB ().items [itemDBItem.id].icon);
			items [curItemCount].GetComponent<UIItemListItem> ().itemDescription.text = itemDBItem.description;
			items [curItemCount].GetComponent<UIItemListItem> ().itemPrice.text = itemDBItem.buyFor.ToString ();

			//search how manywe have in the inventory
			int amountInInventory = 0;
			for (int j = 0; j < Persistence.Data ().items.Count; j++) {
				if (Persistence.Data ().items [j].id == itemDBItem.id)
					amountInInventory+=Persistence.Data ().items [j].amount;
			}
			items [curItemCount].GetComponent<UIItemListItem> ().itemValue.text = amountInInventory.ToString ();	

			items [curItemCount].transform.SetParent (itemListParent.transform, false);
			items [curItemCount].GetComponent<UIItemListItem> ().uiItemList = this;
		}
	}

	public void OpenSellList(){
		gold.text = "Gold: " + Persistence.Data ().gold.ToString ();
		buyOrSell = true;

		buyButton.SetActive (true);
		buyButton.GetComponent<Button> ().interactable = true;
		sellButton.SetActive (true);
		sellButton.GetComponent<Button> ().interactable = false;

		//set content
		for (int i = 0; i < items.Count; i++)
			Destroy (items [i]);
		items.Clear ();

		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		for (int i = 0; i < Persistence.Data ().items.Count; i++) {
			Debug.Log ("item count: " + i);
			ItemDBItem itemDBItem = Persistence.GetItemDB ().items [Persistence.Data ().items [i].id];
			int curItemCount = items.Count;
			items.Add (Instantiate (itemListItemPrefab));
			items [curItemCount].GetComponent<UIItemListItem> ().id = i;
			items [curItemCount].GetComponent<UIItemListItem> ().itemName.text = itemDBItem.name;
			items [curItemCount].GetComponent<UIItemListItem> ().icon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetItemDB ().items [itemDBItem.id].icon);
			items [curItemCount].GetComponent<UIItemListItem> ().itemDescription.text = itemDBItem.description;
			items [curItemCount].GetComponent<UIItemListItem> ().itemValue.text = Persistence.Data ().items [i].amount.ToString ();
			items [curItemCount].GetComponent<UIItemListItem> ().itemPrice.text = itemDBItem.sellFor.ToString ();
			items [curItemCount].transform.SetParent (itemListParent.transform, false);
			items [curItemCount].GetComponent<UIItemListItem> ().uiItemList = this;
		}
	}

	public void OpenItemList(int unitListId = -1, int category = -1){
		gold.text = "Gold: " + Persistence.Data ().gold.ToString ();
		itemListPanel.SetActive (true);

		//set content
		for (int i = 0; i < items.Count; i++)
			Destroy (items [i]);
		items.Clear ();

		buyButton.SetActive (false);
		sellButton.SetActive (false);

		Sprite[] icons = Resources.LoadAll<Sprite>("Icons");
		for (int i = 0; i < Persistence.Data ().items.Count; i++) {
			ItemDBItem itemDBItem = Persistence.GetItemDB ().items [Persistence.Data ().items [i].id];
			Debug.Log ("name: " + itemDBItem.name);
			Debug.Log ("category: " + itemDBItem.category);
			//filter category
			bool filtered = false;
			if ((category == 4) || (category == 3)) {
				if ((itemDBItem.category != 3) && (itemDBItem.category != 4))
					filtered = true;
			} else if ((category == 5) || (category == 6)) {
				if ((itemDBItem.category != 5) && (itemDBItem.category != 6))
					filtered = true;
			} else if ((category == 9) || (category == 10)) {
				if ((itemDBItem.category != 9) && (itemDBItem.category != 10))
					filtered = true;
			} else if ((category == 12) || (category == 11)) {
				if ((itemDBItem.category != 12) && (itemDBItem.category != 11))
					filtered = true;
			} else if ((category != -1) && (itemDBItem.category != category))
				filtered = true;
			
			if (!filtered) {
				int curItemCount = items.Count;
				items.Add (Instantiate (itemListItemPrefab));
				items [curItemCount].GetComponent<UIItemListItem> ().id = i;
				items [curItemCount].GetComponent<UIItemListItem> ().itemName.text = itemDBItem.name;
				items [curItemCount].GetComponent<UIItemListItem> ().icon.sprite = UIMain.GetSpriteByName (icons, Persistence.GetItemDB ().items [itemDBItem.id].icon);
				items [curItemCount].GetComponent<UIItemListItem> ().itemDescription.text = itemDBItem.description;
				items [curItemCount].GetComponent<UIItemListItem> ().itemValue.text = Persistence.Data ().items [i].amount.ToString ();
				items [curItemCount].transform.SetParent (itemListParent.transform, false);
				items [curItemCount].GetComponent<UIItemListItem> ().uiItemList = this;
			}
		}
	}

	public void CloseItemList(){
		itemListPanel.SetActive (false);
	}
}
