﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPCControlList : MonoBehaviour {
	public List<GameObject> controlSlots = new List<GameObject> ();
	public GameObject pcControlPrefab;
	public GameObject sverdControlPrefab;
	public GameObject switchStancePrefab;

	public void Awake(){
		for (int i = 0; i < controlSlots.Count; i++)
			controlSlots [i].SetActive (false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void AddControl(Unit unit, int controlId){
		if (unit.GetComponent<PC> () != null) {
			controlSlots [controlId].SetActive (true);
			GameObject newControl = Instantiate (pcControlPrefab);
			newControl.transform.SetParent (controlSlots [controlId].transform, false);
			newControl.GetComponent<UIPCControl> ().controlId = controlId;
			newControl.GetComponent<UIPCControl> ().unitTarget = (PC)unit;
			Sprite[] facesets = Resources.LoadAll<Sprite> ("Facesets");
			newControl.GetComponent<UIPCControl> ().uiPCControlList = this;
			newControl.GetComponent<UIPCControl> ().portrait.sprite = UIMain.GetSpriteByName (facesets, Persistence.GetUnitDB ().items [Persistence.Data ().pcData [Persistence.Data ().party [controlId]].id].facesetId);//facesets [Persistence.GetUnitDB ().items [Persistence.Data ().pcData [Persistence.Data ().party [controlId]].id].facesetId];

			//this.unit.GetComponent<PlayableBio> ().control = this.gameObject;
			newControl.GetComponent<UIPCControl> ().vanguardToggle.onValueChanged.AddListener ((on) => {
				//Mouse.FocusOnUnit (this.unit);
				//Control.SelectUnit(unit.gameObject);
				if (on){
					GameObject newSwitchStance = Instantiate(switchStancePrefab);
					newSwitchStance.GetComponent<StanceIcon>().vanguardIcon.SetActive(true);
					newSwitchStance.transform.SetParent(unit.transform);
					newSwitchStance.transform.localPosition = new Vector3(0.0f,0.0f,unit.size);
					unit.GetComponent<PC> ().stance = STANCE.VANGUARD;
				}
			});
			newControl.GetComponent<UIPCControl> ().sentinelToggle.onValueChanged.AddListener ((on) => {
				//Mouse.FocusOnUnit (this.unit);
				//Control.SelectUnit(unit.gameObject);
				if (on){
					GameObject newSwitchStance = Instantiate(switchStancePrefab);
					newSwitchStance.GetComponent<StanceIcon>().sentinelIcon.SetActive(true);
					newSwitchStance.transform.SetParent(unit.transform);
					newSwitchStance.transform.localPosition = new Vector3(0.0f,0.0f,unit.size);
					unit.GetComponent<PC> ().stance = STANCE.SENTINEL;
				}
			});
			newControl.GetComponent<UIPCControl> ().medicToggle.onValueChanged.AddListener ((on) => {
				//Mouse.FocusOnUnit (this.unit);
				//Control.SelectUnit(unit.gameObject);
				if (on){
					GameObject newSwitchStance = Instantiate(switchStancePrefab);
					newSwitchStance.GetComponent<StanceIcon>().medicIcon.SetActive(true);
					newSwitchStance.transform.SetParent(unit.transform);
					newSwitchStance.transform.localPosition = new Vector3(0.0f,0.0f,unit.size);
					unit.GetComponent<PC> ().stance = STANCE.MEDIC;
					unit.GetComponent<PC>().ResetUnitAction();
				}
			});
		} else if (unit.GetComponent<Sverd> () != null) {
			controlSlots [controlId].SetActive (true);
			GameObject newControl = Instantiate (sverdControlPrefab);
			newControl.transform.SetParent (controlSlots [controlId].transform, false);
			newControl.GetComponent<UISverdControl> ().uiPCControlList = this;
			newControl.GetComponent<UISverdControl> ().controlId = controlId;
			newControl.GetComponent<UISverdControl> ().unitTarget = (Sverd)unit;
			Sprite[] facesets = Resources.LoadAll<Sprite> ("Facesets");
			newControl.GetComponent<UISverdControl> ().portrait.sprite = UIMain.GetSpriteByName (facesets,Persistence.GetUnitDB ().items [Persistence.Data().sverdData[Persistence.Data ().pcData [Persistence.Data ().party [controlId]].sverdListId].id].facesetId);
		}
	}

	public void RemoveControl(int controlId){
		if (controlSlots [controlId].transform.childCount > 0) {
			//controlSlots [controlId].transform.GetChild (0).gameObject.SetActive (false);
			Destroy (controlSlots [controlId].transform.GetChild (0).gameObject);
			controlSlots [controlId].SetActive (false);
		}
	}
}
