﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIItemListItem : MonoBehaviour,IPointerClickHandler {
	public int id;
	public UIItemList uiItemList; 
	public Image icon;
	public Text itemName;
	public Text itemDescription;
	public Text itemValue;
	public Text itemPrice;
	public GameObject pricePanel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		uiItemList.GetComponent<UIMain> ().ItemClicked (id);
	}
	#endregion
}
