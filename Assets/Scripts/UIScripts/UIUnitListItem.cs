﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIUnitListItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,IPointerClickHandler{
	public int slot;
	private Vector2 offset;
	private Transform originalParent;
	public UIUnitList uiUnitList;
	public Image portrait;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		Debug.Log ("code reached begin drag");
		offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
		originalParent = this.transform.parent;
		this.transform.SetParent (this.transform.parent.parent);
		this.transform.position = eventData.position - offset;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		this.transform.position = eventData.position - offset;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		//Debug.Log ("code reached end drag");
		//this.transform.SetParent (uiUnitList.unitSlots[slot].transform);
		//this.transform.position = uiUnitList.unitSlots[slot].transform.position;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}

	#endregion

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		uiUnitList.GetComponent<UIMain> ().OpenUnitDetails (Persistence.Data().pcData[Persistence.Data().party[slot]].listId);
		//uiUnits.SelectChar (slot);
		//uiUnits.OpenDetailPanel ();
		//Debug.Log ("SelectedChar = " + PartyPanel.selectedChar);*/
	}
	#endregion
}
