﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISverdListItem : MonoBehaviour, IPointerClickHandler, IPointerDownHandler{
	public int slot;
	private Vector2 offset;
	private Transform originalParent;
	public UISverdList uiSverdList;
	public Image portrait;
	public float downTime;
	public bool pointerDown = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!pointerDown)
			return;
		
		float currentTime = Time.unscaledTime;
		if (currentTime - downTime >= 0.5f) {
			pointerDown = false;
			uiSverdList.GetComponent<UIMain> ().OpenSverdDetails (slot);
		}
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		pointerDown = false;
		float currentTime = Time.unscaledTime;
		Debug.Log ("down time: "+(currentTime - downTime));
		if (currentTime - downTime < 0.5f)
			uiSverdList.GetComponent<UIMain> ().SverdClicked (slot);
		else {
			Debug.Log ("long click");
			uiSverdList.GetComponent<UIMain> ().OpenSverdDetails (slot);
		}
	}
	#endregion

	#region IPointerDownHandler implementation
	public void OnPointerDown (PointerEventData eventData)
	{
		pointerDown = true;
		downTime = Time.unscaledTime;
		//uiSverdList.GetComponent<UIMain> ().SverdClicked (slot);
		//uiSverdList.GetComponent<UIMain> ().OpenUnitDetails (Persistence.Data().pcData[Persistence.Data().party[slot]].listId);
	}
	#endregion
}
