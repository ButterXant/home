﻿//controllable robots
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sverd : Unit {
	// Update is called once per frame
	public override void Update () {
		//call base update
		base.Update ();

		if (isDown) //unit is down, return right away
			return;

		fuel.Add (-1 * Time.deltaTime * powerOutput);

		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["SverdUpdate"], this,Time.deltaTime);
	}
}
