﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pathfinding;
using Pathfinding.RVO;
using UnityEngine.UI;
using MoonSharp.Interpreter;


//================================================================
//custom inspector
//================================================================
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Unit),true)]
public class UnitPreview: Editor{
	public override void OnInspectorGUI(){
		base.OnInspectorGUI ();
		Unit theUnit = (Unit)target;
		GUILayout.Label("State");
		if (theUnit.state == null)
			return;
		int [] theStack = theUnit.state.ToArray();
		for(int i = 0; i < theStack.Length; i++)
			GUILayout.Label(theStack[i].ToString()); 
		Enmity[] theEnmities = theUnit.enmities.ToArray ();
		for(int i = 0; i < theEnmities.Length; i++)
			GUILayout.Label(theEnmities[i].unit.unitName+":"+theEnmities[i].value.ToString());
	}
}
#endif

//public enum UNITSTATE {IDLE, LOCKON, CHASEORATTACK, CHASEORSPECIAL, CHASEORMAGIC, CHASING, ATTACKING, SPECIAL, MAGIC, CHASEORINTERACT, CHASEORHEAL, INTERACTING, HEALING, CHASEORACT, CASTING,CHECKTOUSESKILL,ANIMATING,FOCUSATTACK,FOCUSHEAL};
public enum UNITACTION {ATTACK, HEAL, INTERACT, GETCLOSE}
public enum ACTIONEVENT {ATTACK0, ATTACK2, ATTACK1, ATTACK3, ATTACK4, ATTACK5, HEAL, SPECIAL, ABILITY0};

//================================================================
//enmity / hate data structure
//================================================================
public class Enmity{
	public Unit unit { get; set;}
	public float value { get; set;}
}


//================================================================
//unit main class
//================================================================
public class Unit : MonoBehaviour {
	//system
	public bool locked = false;				//is the unit locked from control / interaction or not
	public GameObject selectedBase;			//indicator that the character is selected
	public GameObject sprite; 				//character sprite to be linked
	public GameObject healthbar;			//the healthbar image if needed

	//hit frames
	public List<float> hitFrames;
	public List<float> refHitFrames;
	public int actionFrame;
	public float actionTime;

	//identifier
	public string unitName = "Default";		//name of the unit
	public int id = -1;						//id of the unit
	public int controlId = -1;				//id in party of unit
	public int lvl = 1;						//unit level

	//delegate functions for comparing 2 units, used to sort list of found units for each unit
	public delegate Unit CompareUnits(Unit unit1, Unit unit2);
	public Dictionary<string,CompareUnits> comparisons = new Dictionary<string, CompareUnits>();
	public Unit CloserDistance(Unit unit1, Unit unit2){
		float distance1 = Vector3.Distance(unit1.transform.position,this.transform.position) - unit1.size - this.size;
		float distance2 = Vector3.Distance(unit2.transform.position,this.transform.position) - unit2.size - this.size;
		return (distance1 <= distance2) ? unit1 : unit2;
	}
	public Unit LowerHealth(Unit unit1, Unit unit2){
		float health1 = unit1.health.ToPercent ();
		float health2 = unit2.health.ToPercent ();
		return (health1 <= health2) ? unit1 : unit2;
	}

	//enmity
	public List<Enmity> enmities; 

	//for casting;
	public AttributeFloat castTime;
	public int skillIdToUse;
	public Vector3 skillTarget;

	//unit attributes
	public float sight;
	public string attackFrames;
	public string healFrames;
	public AttributeInt health; 
	public AttributeInt mp;
	public AttributeInt shield;
	public AttributeInt barrier;
	public AttributeFloat speed;
	public AttributeFloat cooldown;
	public AttributeFloat cooldownSpecial;
	public AttributeFloat rangeAtk;
	public AttributeFloat rangeHeal;
	public AttributeInt attack;
	public AttributeInt magic;
	public AttributeInt heal;
	public AttributeInt armor;
	public AttributeInt mr;
	//elemental resist
	public AttributeFloat resistSol;
	public AttributeFloat resistLuna;
	public AttributeFloat resistStella;
	public AttributeFloat resistVoid;
	public AttributeFloat resistWind;
	public AttributeFloat resistEarth;
	public AttributeFloat resistWater;
	public AttributeFloat resistFire;
	//dodge and accuracy
	public AttributeInt dodge;
	public AttributeInt accuracy;

	//secondary stats
	public AttributeFloat regenHealth;
	public AttributeFloat cooldownRegenHealth;

	//for sverd
	public AttributeFloat fuel;
	public int pilotSverdId = -1;
	public float powerOutput = 1.0f;

	//for pathfinding
	public bool moving = false;
	private Seeker seeker;
	public Path path;
	private int currentWaypoint;
	public float defaultNextWaypointDistance = 1; //max distance from the AI to a waypoint for it to continue
	public Vector3 targetPos;
	public float stopDistance;
	public float size = 1.0f;
	public RVOController controller;
	public AttributeFloat repathRate;

	public void SetPath(Vector3 target, float stopDistance = Constants.DEFAULTSTOPDISTANCE){
		this.targetPos = target;
		this.stopDistance = stopDistance;
		moving = true;
		sprite.GetComponent<Animator> ().SetBool ("moving", moving);
		seeker.StartPath (transform.position, targetPos, OnPathComplete);
	}

	public void Repath(){
		if (target != null) {
			seeker.StartPath (transform.position, target.transform.position, OnPathComplete);
		}
	}

	public void OnPathComplete(Path p){
		if (!p.error) {
			path = p;
			currentWaypoint = 0; //reset waypoint counter
		}
	}

	//AI
	public Stack<int> state; 
	public GameObject target = null;

	//condition
	public bool isDown = false;

	//initialization
	public virtual void Awake(){
		//sprite = transform.parent.gameObject.GetComponentInParent<SpriteEventMgr> ().gameObject;
		seeker = GetComponent<Seeker> (); //get seeker
		repathRate = new AttributeFloat(0.5f,0.0f,0.5f);
		state = new Stack<int>();
		state.Push (0); //push idle
		controller = GetComponent<RVOController>();
		hitFrames = new List<float> ();

		//initialize attributes
		health = new AttributeInt(1000);
		mp = new AttributeInt (100);
		shield = new AttributeInt(0);
		barrier = new AttributeInt(0);
		speed = new AttributeFloat (2.5f);
		cooldown = new AttributeFloat(5.0f);
		cooldownSpecial = new AttributeFloat(10.0f);
		rangeAtk = new AttributeFloat(1.0f);
		rangeHeal = new AttributeFloat(2.5f);
		attack = new AttributeInt(100);
		magic = new AttributeInt(100);
		heal = new AttributeInt(50);
		armor = new AttributeInt(20);
		mr = new AttributeInt(20);
		//elemental resist
		resistSol = new AttributeFloat (0.0f);
		resistLuna = new AttributeFloat (0.0f);
		resistStella = new AttributeFloat (0.0f);
		resistVoid = new AttributeFloat (0.0f);
		resistWind = new AttributeFloat (0.0f);
		resistEarth = new AttributeFloat (0.0f);
		resistWater = new AttributeFloat (0.0f);
		resistFire = new AttributeFloat (0.0f);
		//dodge and accuracy
		dodge = new AttributeInt (100);
		accuracy = new AttributeInt (100);

		//secondary stats
		//regenHealth = new AttributeFloat(0.0f);
		cooldownRegenHealth = new AttributeFloat (Constants.REGENTIME);

		//for sverd
		fuel = new AttributeFloat (0.0f);

		//initialize enmities
		enmities = new List<Enmity> (); 

		comparisons.Add ("CloserDistance", CloserDistance);
		comparisons.Add ("LowerHealth", LowerHealth);
	}

	//for customized action overrode with child's function - need rework
	public virtual void ActionEvent(ACTIONEVENT action){
		float stanceMultiplier = 1.0f;
		if (GetComponent<PC> () != null) {
			if (GetComponent<PC> ().stance == STANCE.MEDIC)
				stanceMultiplier = 0.75f;
			else if (GetComponent<PC> ().stance == STANCE.SENTINEL)
				stanceMultiplier = 0.5f;
		}

		if (action == ACTIONEVENT.ABILITY0) {
			//Debug.Log ("ability used");
			//get the skill
			SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [skillIdToUse];

			//get skill effect
			string [] effects = skillDBItem.effect.Split(',');
			for (int i = 0; i < effects.Length; i++) {
				string[] param = effects [i].Split (':');
				switch (param [0]) {
				case "aoe":
					{
						string[] details = param [1].Split ('$');
						float range = float.Parse (details [0]);
						int affectedTarget = int.Parse (details [1]);
						float multiplier = float.Parse (details [2]);
						if (affectedTarget == 5) {
							//find all enemies
							Enemy [] enemies = GameObject.FindObjectsOfType<Enemy>();
							for (int j = 0; j < enemies.Length; j++) {
								if (Vector3.Distance (this.transform.position, enemies [j].transform.position) - size - enemies[j].size <= range) {
									UnitMgr.Engage (this, enemies [j].GetComponent<Unit> (), -1 * (int)Mathf.Floor (attack._current * multiplier * ((GetComponent<Sverd>()!=null)?powerOutput:1.0f) * stanceMultiplier), ENGAGEMENTTYPE.ATTACK);
								}
							}
						}
					}
					break;
				case "magic":
					{
						if (target == null)
							return;
						string [] details = param [1].Split('$');
						GameObject newMagic = (GameObject) Instantiate(Resources.Load ("MagicPrefabs\\" + details[0]));
						newMagic.transform.position = this.transform.position;
						newMagic.GetComponent<Magic> ().caster = this;
						newMagic.GetComponent<Magic> ().target = target.GetComponent<Unit>();
						newMagic.GetComponent<Magic> ().denom = (int)Mathf.Floor(-1 * float.Parse(details[1]) * magic._current * ((GetComponent<Sverd>()!=null)?powerOutput:1.0f) * stanceMultiplier);
					}
					break;
				}
			}
		} else if(action == ACTIONEVENT.HEAL){
			if ((target != null) && !target.GetComponent<Unit> ().isDown) {
				UnitMgr.Engage (this, target.GetComponent<Unit> (), heal._current, ENGAGEMENTTYPE.HEAL);
			}
		}else {
			if ((target != null) && !target.GetComponent<Unit> ().isDown) {
				UnitMgr.Engage (this, target.GetComponent<Unit> (), (int)Mathf.Floor(-1 * attack._current * ((GetComponent<Sverd>()!=null)?powerOutput:1.0f) * stanceMultiplier), ENGAGEMENTTYPE.ATTACK);
			}
		}
	}

	//for customized down handler overrode with child's function 
	public virtual void DownEvent(){
		UnitMgr.DestroyUnit (this);
	}
		
	//to get the enmity of a specific unit
	public int GetIndexOfEnmity(Unit unitChecked){
		int i = 0;
		while (i < enmities.Count) {
			if (enmities [i].unit != null) {
				if (enmities [i].unit == unitChecked)
					return i;
				else
					i++;
			} else
				i++;
		}

		return -1;
	}

	//to pop things on top of characters
	public Vector3 GetOverheadScreenPos(){
		Vector3 newPos = this.transform.position + new Vector3 (0, GetComponent<BoxCollider> ().size.z, 0);
		return Camera.main.WorldToScreenPoint (newPos);
	}

	// Update is called once per frame
	public virtual void Update () {
		//update healthbar position
		if (healthbar != null) {
			Vector3 screenPos = GetOverheadScreenPos ();
			healthbar.GetComponent<RectTransform> ().position = new Vector3 (screenPos.x, screenPos.y, 0);
			healthbar.transform.Find ("Health").transform.GetChild (0).gameObject.GetComponent<Image> ().fillAmount = (float)health._current / (float)health._base;
			healthbar.transform.Find ("Cooldown").transform.GetChild (0).gameObject.GetComponent<Image> ().fillAmount = cooldown._current / cooldown._base;
		}

		//if unit is down, no need to update anything
		if (isDown)
			return;

		//health regen
		cooldownRegenHealth.Add(Time.deltaTime);
		if (cooldownRegenHealth._current >= cooldownRegenHealth.AdjustedValue ()) {
			health.Add ((int)Math.Floor(health.AdjustedValue() * regenHealth._current));
			cooldownRegenHealth._current = 0.0f;
		}

		//enmity reduction
		for (int i = enmities.Count - 1; i >= 0; i--) {
			enmities [i].value -= Time.deltaTime * 10;
			if (enmities [i].value <= 0 || enmities [i].unit == null) {
				enmities.RemoveAt (i);
			}
		}

		//repath rate
		repathRate.Add(Time.deltaTime);
	}
		
	//for movement
	public void FixedUpdate(){
		//if unit is down, no need to update anything
		if (isDown)
			return;

		sprite.transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);

		if (!moving) 
			return;
		if (path == null)
			return;
		if (currentWaypoint >= path.vectorPath.Count) {
			return;
		}

		//check if going to a point or chasing a target to see if we've reached the location
		if (target == null) { // going to a point
			//check whether unit is close enough
			if (AstarPath.active.GetNearest (targetPos).node.Walkable) { //target is close to a walkable node
				if ((Vector3.Distance (transform.position, targetPos) < stopDistance)) {
					moving = false;
					sprite.GetComponent<Animator> ().SetBool ("moving", moving);
					path = null;
					return;
				}
			} else { //target doesn't have a close enough walkable node, can only go as far as the last given waypoint
				if ((currentWaypoint == path.vectorPath.Count - 1) && ((Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]) <= 0.0f))){
					moving = false;
					sprite.GetComponent<Animator> ().SetBool ("moving", moving);
					path = null;
					return;
				}
			}
		} else { // chasing a target
			if (Vector3.Distance (transform.position, target.transform.position) < stopDistance) {
				moving = false;
				sprite.GetComponent<Animator> ().SetBool ("moving", moving);
				path = null;
				return;
			}
		}

		//calculate direction of unit
		Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
		if (dir.x < 0)
			sprite.transform.localScale = new Vector3 (-1, 1, 1);
		else if(dir.x > 0)
			sprite.transform.localScale = new Vector3 (1, 1, 1);

		//move unit
		if (controller != null) {
			controller.SetTarget (path.vectorPath [currentWaypoint], speed._current, speed._current);
			var delta = controller.CalculateMovementDelta (transform.position, Time.deltaTime);
			transform.position = transform.position + delta;
		} else {
			transform.position = Vector3.MoveTowards(transform.position,path.vectorPath[currentWaypoint],Time.deltaTime * speed._current);
			sprite.transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
		}

		//get next waypoint
		float nextWaypointDistance = defaultNextWaypointDistance;
		if (currentWaypoint == path.vectorPath.Count - 1)
			nextWaypointDistance = 0.0f;

		//if close enough to the current waypoint, proceed to next waypoint
		if (Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]) < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
	}

	//reset unit state whenever giving command
	public void ResetUnitAction(){
		moving = false;
		sprite.GetComponent<Animator> ().SetBool ("moving", moving);
		path = null;
		state.Clear ();
		state.Push (0);
	}

	//set target to a certain object
	public void SetTarget(GameObject target){
		this.target = target;

		if (target == null)
			return;

		//calculate direction of unit
		Vector3 dir = (target.transform.position - transform.position).normalized;
		//change the facing of the sprite
		if (dir.x < 0)
			sprite.transform.localScale = new Vector3 (-1, 1, 1);
		else
			sprite.transform.localScale = new Vector3 (1, 1, 1);
	}

	//Interact action
	public void Interact (GameObject interactTarget){
		//calculate direction of unit
		Vector3 dir = (interactTarget.transform.position - transform.position).normalized;
		//change the facing of the sprite
		if (dir.x < 0)
			sprite.transform.localScale = new Vector3 (-1, 1, 1);
		else if(dir.x > 0)
			sprite.transform.localScale = new Vector3 (1, 1, 1);

		interactTarget.GetComponent<Unit> ().Interacted (this);
	}

	//get highest enmity
	public GameObject GetHighestEnmity(){
		GameObject mostHated = null;
		float highestEnmity = 0.0f;
		for (int j = 0; j < enmities.Count; j++) {
			if ((enmities [j].value > highestEnmity) && !enmities [j].unit.isDown) {
				mostHated = enmities [j].unit.gameObject;
				highestEnmity = enmities [j].value;
			}
		}
		return mostHated;
	}

	//what happens if someone interacts with this unit
	public virtual void Interacted(Unit interactingUnit){
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["DefaultInteracted"], this, interactingUnit);
	}

	public void ResetHitFrames(string hitFrames){
		actionFrame = 0;
		actionTime = 0.0f;
		this.hitFrames.Clear ();
		string theFrames;
		if (hitFrames == "attack")
			theFrames = attackFrames;
		else if (hitFrames == "heal")
			theFrames = healFrames;
		else 
			theFrames = hitFrames;

		//parse theframes
		string [] parsedFrames = theFrames.Split('_');
		for (int i = 0; i < parsedFrames.Length; i++) {
			this.hitFrames.Add (float.Parse(parsedFrames[i]));
		}
	}
}

//proxy class for Behavior Lua
class UnitProxy{
	Unit target; //the proxied unit

	[MoonSharpHidden]
	public UnitProxy(Unit target){
		this.target = target;
	}

	//what kind of unit is this
	public bool IsPC(){ return (target.GetComponent<PC>() != null); }
	public bool IsSverd(){ return (target.GetComponent<Sverd>() != null); }
	public bool IsEnemy(){ return (target.GetComponent<Enemy>() != null); }

	//state accessors
	public int GetUnitState(){ return (int)target.state.Peek (); }
	public void PushState(int newState){ target.state.Push (newState); }
	public void PopState(){ target.state.Pop (); }
	public void ResetState(){ target.ResetUnitAction (); }

	//stance
	public int GetUnitStance(){
		if (target.GetComponent<PC> () == null)
			throw new ScriptRuntimeException ("Not a PC unit. Can't find stance property.");
		else
			return (int)target.GetComponent<PC> ().stance;
	}

	//lock unlock a unit 
	public bool IsLocked() {return target.locked;}
	public void Lock() {target.locked = true;}
	public void Unlock() {target.locked = false;}

	//attribute accessors
	public bool IsMoving(){ return target.moving; }
	public float GetSight(){ return target.sight; }
	public AttributeFloat GetRangeAtk(){ return target.rangeAtk; }
	public AttributeFloat GetRangeHeal(){ return target.rangeHeal; }
	public AttributeFloat GetCooldown(){ return target.cooldown; }
	public AttributeFloat GetRepathRate(){ return target.repathRate;}

	//skills related
	public bool IsSkillTargetUnit(){ 
		SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [target.skillIdToUse];
		return skillDBItem.target != 0;
	}
	public void SetupCasting(){
		//get the skill
		SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [target.skillIdToUse];
		target.castTime = new AttributeFloat(skillDBItem.casttime,0.0f,0.0f);
	}
	public AttributeFloat GetCastTime(){ return target.castTime; }

	//unit finders
	//get highest enmity of the proxied unit
	public Unit GetHighestEnmity(){
		Unit mostHated = null;
		float highestEnmity = 0.0f;
		for (int j = 0; j < target.enmities.Count; j++) {
			if ((target.enmities [j].value > highestEnmity) && !target.enmities [j].unit.isDown) {
				mostHated = target.enmities [j].unit;
				highestEnmity = target.enmities [j].value;
			}
		}

		return mostHated;
	}

	//find a gameobject to target with specified arguments
	public Unit FindTarget(string [] args){
		Unit targetUnit = null; 
		string comparison = "CloserDistance";
		float range = 0.0f;
		bool isDown = false;
		bool includeSelf = false;
		List<Unit> foundUnits = new List<Unit> ();

		foreach (string arg in args) {
			string[] parsed = arg.Split (':');
			switch (parsed [0]) {
			case "Type":
				{
					Unit [] unitArrays = (Unit[])GameObject.FindObjectsOfType(Type.GetType(parsed[1]));
					for(int i=0; i < unitArrays.Length; i++)
						foundUnits.Add(unitArrays[i]);
				}
				break;
			case "Comparison":
				{
					comparison = parsed [1];
				}
				break;
			case "Range":
				{
					range = float.Parse (parsed[1]);
				}
				break;
			case "IsDown":
				{
					isDown = int.Parse (parsed [1])==0?false:true;
				}
				break;
			case "IncludeSelf":
				{
					includeSelf = int.Parse (parsed [1])==0?false:true;
				}
				break;
			}
		}
			
		for (int i = 0; i < foundUnits.Count; i++) {
			if ((foundUnits [i] == target) && !includeSelf)
				continue;
			if (foundUnits [i].isDown == isDown) { //compare isdown condition
				float distance = Vector3.Distance(foundUnits[i].transform.position,target.transform.position) - foundUnits[i].size - target.size;
				if (distance <= range) { //check within range
					if (targetUnit == null)
						targetUnit = foundUnits [i];
					else
						targetUnit = target.comparisons [comparison] (targetUnit, foundUnits [i]);
				}
			}
		}

		return targetUnit;
	}

	//target's target accessors
	public Unit GetTarget(){ return this.target.target?this.target.target.GetComponent<Unit>():null;}
	public void SetTarget(Unit setTarget){ this.target.SetTarget (setTarget.gameObject);}
	public void ClearTarget() { this.target.target = null;}

	//make a target unit face its target if it's possible
	public void FaceTarget(){
		if (target.target != null) {
			Vector3 dir = (target.target.transform.position - target.transform.position).normalized;
			//change the facing of the sprite
			if (dir.x < 0)
				target.sprite.transform.localScale = new Vector3 (-1, 1, 1);
			else
				target.sprite.transform.localScale = new Vector3 (1, 1, 1);
		}
	}

	//get target unit to interact with its currently targetted unit
	public void Interact(){ this.target.Interact (this.target.target); }
	//query list of attached game event when this unit is interacted with
	public void QueryGameEvent(){
		GameEvent gameEvent = target.GetComponent<GameEvent> ();
		if (gameEvent != null)
			gameEvent.Interact ();
	}

	//get the unit to scout around. this will set a random point within its sight as its target.
	//might want to change the state of the unit to chasing after calling this
	public void Scout(){
		//get random point inside scout range
		Vector3 randomPoint = UnityEngine.Random.insideUnitCircle * target.sight;
		this.target.SetPath (this.target.transform.parent.transform.position + randomPoint);
	}

	//get target unit to use a skill (basically just telling it to play a certain skill animation and setting up the hitframes) 
	public void UseSkill(){
		//get the skill
		SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [target.skillIdToUse];

		//play the animation template that is listed on the skill
		target.sprite.GetComponent<Animator> ().Play (skillDBItem.animation);
		target.ResetHitFrames (skillDBItem.hitFrames);
	}

	//check for target's target
	public bool IsTargetLost(){ return this.target.target == null; } 
	public bool IsTargetDown(){ return this.target.target.GetComponent<Unit>().isDown; } 
	public bool IsTargetOutOfRange(float range){ return Vector3.Distance (target.target.transform.position, target.transform.position) - target.size - target.target.GetComponent<Unit> ().size > range; }
	public bool IsOutOfSight(Unit targetUnit){ //check whether input target is out of sight. this is not to check whether current target's target is out of sight
		return Vector3.Distance (targetUnit.transform.position, target.transform.parent.position) - targetUnit.size > target.sight;
	}
	public bool IsSkillTargetOutOfRange(){ //to determine whether we need to chase the target before we can use a certain skill
		//get the skill
		SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [target.skillIdToUse];

		if (skillDBItem.target == 0) { //target ground
			if (skillDBItem.range >= Vector3.Distance (this.target.transform.position, target.skillTarget)) { //in range
				return false;
			} else { //out of range, have to go there
				return true;
			}		
		} else {
			if (skillDBItem.range >= (Vector3.Distance (target.target.transform.position, target.transform.position) - target.size - target.target.GetComponent<Unit> ().size)) { //in range
				return false;
			} else { //out of range, have to go there
				return true;
			}
		}
	}

	//pathfinding
	public Unit GetLeader(){
		GameObject selectedUnit = Control.selectedUnit;
		if (selectedUnit == null)
			return null;
		else {
			Unit leader = selectedUnit.GetComponent<Unit> ();
			if (Vector3.Distance (leader.transform.position, target.transform.position) > Constants.TOOFAR) {
				return leader;
			} else
				return null;
		}
	}
	public void Repath(){ target.Repath ();}
	public void SetPathToTarget(int intent){
		INTENT iintent = (INTENT)intent;
		if (iintent == INTENT.ATTACK) {
			this.target.SetPath (this.target.target.transform.position, target.rangeAtk._current + target.target.GetComponent<Unit> ().size + target.size);
		} else if (iintent == INTENT.HEAL) {
			this.target.SetPath (this.target.target.transform.position, target.rangeHeal._current + target.target.GetComponent<Unit> ().size + target.size);
		} else {
			this.target.SetPath (this.target.target.transform.position, 1.0f + target.target.GetComponent<Unit> ().size + target.size);
		}
	}
	public void SetSkillPath(){
		//get the skill
		SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [target.skillIdToUse];

		if (skillDBItem.target == 0) { //target ground
			target.SetPath (target.skillTarget);
		} else {
			target.SetPath (target.target.transform.position, skillDBItem.range + target.target.GetComponent<Unit> ().size + target.size);
		}
	}

	//play sprite animation
	public void PlayAnimation(string anim){
		target.sprite.GetComponent<Animator> ().Play (anim);
	}

	//hit frames to determine when a hit connects
	public void ResetHitFrames(string hitFrames){ target.ResetHitFrames (hitFrames);}

	//action function calls to be called every frame when doing an action to check the hitframes when the action connects
	//for attack action
	public bool Attack(){
		if (target.hitFrames.Count == 0)
			return false;

		//if (target.hitFrames [0] == target.actionFrame) {
		if (target.hitFrames [0] <= target.actionTime) {
			if (target.hitFrames.Count == 1)
				return false;
			else{
				target.ActionEvent (ACTIONEVENT.ATTACK0);
				target.hitFrames.RemoveAt (0);
			}
		}

		target.actionFrame++;
		target.actionTime += Time.deltaTime;
		return true;
	}
	//for heal action
	public bool Heal(){
		if (target.hitFrames.Count == 0)
			return false;

		//if (target.hitFrames [0] == target.actionFrame) {
		if (target.hitFrames [0] <= target.actionTime) {
			if (target.hitFrames.Count == 1)
				return false;
			else{
				target.ActionEvent (ACTIONEVENT.HEAL);
				target.hitFrames.RemoveAt (0);
			}
		}

		target.actionFrame++;
		target.actionTime += Time.deltaTime;
		return true;
	}
	//for skill action
	public bool Skill(){
		if (target.hitFrames.Count == 0)
			return false;

		//if (target.hitFrames [0] == target.actionFrame) {
		if (target.hitFrames [0] <= target.actionTime) {
			if (target.hitFrames.Count == 1)
				return false;
			else{
				target.ActionEvent (ACTIONEVENT.ABILITY0);
				target.hitFrames.RemoveAt (0);
			}
		}

		target.actionFrame++;
		target.actionTime += Time.deltaTime;
		return true;
	}

	//for debugging
	public void Print(string debugString){
		Debug.Log (debugString);
	}
}

//================================================================
//classes to store unit attributes
//================================================================
[Serializable]
public class Attribute<T>{
	public T _base; //initial value
	public T _buff; //buff value
	public T _current; //current (dynamic) value
}

[MoonSharpUserData]
[Serializable]
public class AttributeInt:Attribute<int>{
	//alt constructor
	public AttributeInt(int _base){
		this._base = _base;
		this._buff = 0;
		this._current = _base;
	}

	public AttributeInt(int _base, int _buff, int _current){
		this._base = _base;
		this._buff = _buff;
		this._current = _current;
	}

	//return max value with buff
	public int AdjustedValue(){
		return _base + _buff;
	}

	//return percentage, useful for UIs
	public float ToPercent(){
		return 1.0f * _current / (_base + _buff);
	}

	//buff the value
	public void Buff(int buff){
		_buff += buff;
		if (_current > AdjustedValue ())
			_current = AdjustedValue ();
	}

	//update value on runtime
	public void UpdateBase(int _base){
		this._base = _base;
	}

	//initialize value on runtime
	public void Initialize(int _base){
		this._base = _base;
		this._buff = 0;
		this._current = _base;
	}

	//initialize value on runtime using another Attribute
	public void Initialize(AttributeInt val, bool adjust = true){
		this._base = val._base;
		this._buff = val._buff;

		if (adjust)
			this._current = AdjustedValue ();
		else
			this._current = val._current;
	}

	//add value to current
	public void Add(int denom){
		_current += denom;
		if (_current < 0)
			_current = 0;
		else if (_current > AdjustedValue ())
			_current = AdjustedValue ();
	}
}

[MoonSharpUserData]
[Serializable]
public class AttributeFloat:Attribute<float>{
	//alt constructor
	public AttributeFloat(float _base){
		this._base = _base;
		this._buff = 0;
		this._current = _base;
	}

	//alt constructor
	public AttributeFloat(float _base, float _buff, float _current){
		this._base = _base;
		this._buff = _buff;
		this._current = _current;
	}

	//return max value with buff
	public float AdjustedValue(){
		return _base + _buff;
	}

	//return percentage, useful for UIs
	public float ToPercent(){
		return _current / (_base + _buff);
	}

	//buff the value
	public void Buff(float buff){
		_buff += buff;
		if (_current > AdjustedValue ())
			_current = AdjustedValue ();
	}

	//update value on runtime
	public void UpdateBase(float _base){
		this._base = _base;
	}

	//initialize value on runtime
	public void Initialize(float _base){
		this._base = _base;
		this._buff = 0.0f;
		this._current = _base;
	}

	//initialize value on runtime using another attribute
	public void Initialize(AttributeFloat val, bool adjust = true){
		this._base = val._base;
		this._buff = val._buff;

		if (adjust)
			this._current = AdjustedValue ();
		else
			this._current = val._current;
	}

	//add value to current
	public void Add(float denom){
		_current += denom;
		if (_current < 0)
			_current = 0;
		else if (_current > AdjustedValue ())
			_current = AdjustedValue ();
	}
}

[MoonSharpUserData]
[Serializable]
public class AttributeBool:Attribute<bool>{
	//alt constructor
	public AttributeBool(bool _base){
		this._base = _base;
		this._buff = false;
		this._current = _base;
	}

	//buff the value
	public void Buff(bool buff){
		_buff = buff;
		_current = buff;
	}

	//initialize on runtime
	public void Initialize(bool _base){
		this._base = _base;
		this._buff = false;
		this._current = _base;
	}

	//initialize with other attribute
	public void Initialize(AttributeBool val, bool adjust = true){
		this._base = val._base;
		this._buff = val._buff;

		if (adjust)
			this._current = val._base;
		else
			this._current = val._current;
	}
}