﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public class NPC : Unit {
	// Update is called once per frame
	public override void Update () {
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["NPCUpdate"], this,Time.deltaTime);
	}

	//what happens if someone interacts with this unit
	public override void Interacted(Unit interactingUnit){
		interactingUnit.locked = true;
		locked = true;
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["DefaultNPCInteracted"], this, interactingUnit);
	}
}
