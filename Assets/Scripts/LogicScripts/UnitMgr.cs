﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum ENGAGEMENTTYPE{ATTACK, HEAL, MAGIC};

public class UnitMgr : MonoBehaviour {
	public static UnitMgr instance;
	public GameObject hitNumberPrefab;
	public GameObject canvas;
	public GameObject unitOverlay;
	public GameObject healthbarPrefab;

	void OnEnable(){
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	public void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		Resources.UnloadUnusedAssets ();

		if (scene.name == "Main")
			return; 

		canvas = GameObject.Find ("Canvas");
		unitOverlay = canvas.transform.Find ("UnitOverlay").gameObject;
	}


	/*public Vector3 ScreenToCanvas(Vector3 screenPos, Canvas canvas){
		float x = (screenPos.x - Screen.width / 2) / (Screen.width / 2) * canvas.gameObject.GetComponent<RectTransform> ().rect.width/2;
		float y = (screenPos.y - Screen.height / 2) / (Screen.height / 2) * canvas.gameObject.GetComponent<RectTransform> ().rect.height/2; 
		return new Vector3 (x,y,0);
	}*/

	void Awake(){
		//for singleton
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if(instance != this){
			Destroy (gameObject);
		}

		//instance = this;
	}

	// Use this for initialization
	void Start () {
		//canvas = GameObject.Find ("Canvas");
		//unitOverlay = canvas.transform.Find ("UnitOverlay").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		//testing
		if (Input.GetMouseButtonUp (1)) {
			//get mouse pos
			Vector2 mousePos = Input.mousePosition;

			//instantiate hit number
			GameObject hitNumber = Instantiate (hitNumberPrefab);
			hitNumber.transform.SetParent (unitOverlay.transform);
			//hitNumber.GetComponent<RectTransform> ().localPosition = ScreenToCanvas(Input.mousePosition,canvas.GetComponent<Canvas>());
		}
	}

	public static void DestroyUnit(Unit toDestroy){
		if (toDestroy.healthbar != null) {
			Destroy (toDestroy.healthbar);
			Destroy (toDestroy.transform.parent.gameObject);
		}
	}
		
	public static bool IsAnyEngaged(){
		bool result = false;
		Unit[] allUnits = GameObject.FindObjectsOfType<Unit> ();
		for (int i = 0; i < allUnits.Length; i++) {
			result = result || (allUnits [i].target != null);
		}
		return result;
	}

	//instantiate guest unit
	//instantiate enemy
	//instantiate NPC
	public static GameObject InstantiateUnit(int unitId, Vector3 pos,Vector3 ori, bool withHealthBar,int lvl = 1){
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [unitId];
		GameObject newUnit = (GameObject) Instantiate(Resources.Load ("UnitPrefabs\\" + unitDBItem.prefab),pos,Quaternion.identity);
		//newUnit.GetComponentInChildren<Unit> ().transform.position = pos;
		newUnit.GetComponentInChildren<Unit> ().sprite.gameObject.transform.localScale = ori;

		if (withHealthBar) {
			GameObject newHealthbar = (GameObject)Instantiate (instance.healthbarPrefab);
			newHealthbar.transform.SetParent (instance.unitOverlay.transform, false);
			newUnit.GetComponentInChildren<Unit> ().healthbar = newHealthbar;

			//initialize stats
			Unit unitLogic = newUnit.GetComponentInChildren<Unit> ();
			unitLogic.lvl = lvl;
			unitLogic.id = unitId;
			//initialize attributes
			unitLogic.sight = unitDBItem.sight;
			unitLogic.attackFrames = unitDBItem.attackFrames;
			unitLogic.healFrames = unitDBItem.healFrames;
			unitLogic.health = new AttributeInt(unitDBItem.health.ValueAtLvl(lvl));
			unitLogic.mp = new AttributeInt (unitDBItem.mp.ValueAtLvl(lvl));
			unitLogic.shield = new AttributeInt(unitDBItem.shield.ValueAtLvl(lvl));
			unitLogic.barrier = new AttributeInt(unitDBItem.barrier.ValueAtLvl(lvl));
			unitLogic.speed = new AttributeFloat (unitDBItem.speed.ValueAtLvl(lvl));
			unitLogic.cooldown = new AttributeFloat(unitDBItem.cooldown.ValueAtLvl(lvl));
			unitLogic.cooldownSpecial = new AttributeFloat(unitDBItem.cooldownSpecial.ValueAtLvl(lvl));
			unitLogic.rangeAtk = new AttributeFloat(unitDBItem.rangeAtk.ValueAtLvl(lvl));
			unitLogic.rangeHeal = new AttributeFloat(unitDBItem.rangeHeal.ValueAtLvl(lvl));
			unitLogic.attack = new AttributeInt(unitDBItem.attack.ValueAtLvl(lvl));
			unitLogic.magic = new AttributeInt(unitDBItem.magic.ValueAtLvl(lvl));
			unitLogic.heal = new AttributeInt(unitDBItem.heal.ValueAtLvl(lvl));
			unitLogic.armor = new AttributeInt(unitDBItem.armor.ValueAtLvl(lvl));
			unitLogic.mr = new AttributeInt(unitDBItem.mr.ValueAtLvl(lvl));
			//elemental resist
			unitLogic.resistSol = new AttributeFloat (unitDBItem.resistSol.ValueAtLvl(lvl));
			unitLogic.resistLuna = new AttributeFloat (unitDBItem.resistLuna.ValueAtLvl(lvl));
			unitLogic.resistStella = new AttributeFloat (unitDBItem.resistStella.ValueAtLvl(lvl));
			unitLogic.resistVoid = new AttributeFloat (unitDBItem.resistVoid.ValueAtLvl(lvl));
			unitLogic.resistWind = new AttributeFloat (unitDBItem.resistWind.ValueAtLvl(lvl));
			unitLogic.resistEarth = new AttributeFloat (unitDBItem.resistEarth.ValueAtLvl(lvl));
			unitLogic.resistWater = new AttributeFloat (unitDBItem.resistWater.ValueAtLvl(lvl));
			unitLogic.resistFire = new AttributeFloat (unitDBItem.resistFire.ValueAtLvl(lvl));
			//dodge and accuracy
			unitLogic.dodge = new AttributeInt (unitDBItem.dodge.ValueAtLvl(lvl));
			unitLogic.accuracy = new AttributeInt (unitDBItem.accuracy.ValueAtLvl(lvl));

			//secondary stats
			unitLogic.regenHealth = new AttributeFloat(unitDBItem.regenHealth.ValueAtLvl(lvl));
		}

		return newUnit;
	}

	public static void InitializeUnitAttributes(Unit initializedUnit, int partyId){
		//initialize stats
		PCData pcData = Persistence.Data().pcData[Persistence.Data ().party [partyId]];
		Unit unitLogic = initializedUnit.GetComponentInChildren<Unit> ();
		unitLogic.lvl = pcData.lvl;
		unitLogic.id = pcData.id;
		unitLogic.controlId = partyId;
		//initialize attributes
		unitLogic.health = new AttributeInt(pcData.health._base,pcData.health._buff,pcData.health._current);
		unitLogic.mp = new AttributeInt (pcData.mp._base,pcData.mp._buff,pcData.mp._current);
		unitLogic.shield = new AttributeInt(pcData.shield._base,pcData.shield._buff,pcData.shield._current);
		unitLogic.barrier = new AttributeInt(pcData.barrier._base,pcData.barrier._buff,pcData.barrier._current);
		unitLogic.speed = new AttributeFloat (pcData.speed._base,pcData.speed._buff,pcData.speed._current);
		unitLogic.cooldown = new AttributeFloat(pcData.cooldown._base,pcData.cooldown._buff,pcData.cooldown._current);
		unitLogic.cooldownSpecial = new AttributeFloat(pcData.cooldownSpecial._base,pcData.cooldownSpecial._buff,pcData.cooldownSpecial._current);
		unitLogic.rangeAtk = new AttributeFloat(pcData.rangeAtk._base,pcData.rangeAtk._buff,pcData.rangeAtk._current);
		unitLogic.rangeHeal = new AttributeFloat(pcData.rangeHeal._base,pcData.rangeHeal._buff,pcData.rangeHeal._current);
		unitLogic.attack = new AttributeInt(pcData.attack._base,pcData.attack._buff,pcData.attack._current);
		unitLogic.magic = new AttributeInt(pcData.magic._base,pcData.magic._buff,pcData.magic._current);
		unitLogic.heal = new AttributeInt(pcData.heal._base,pcData.heal._buff,pcData.heal._current);
		unitLogic.armor = new AttributeInt(pcData.armor._base,pcData.armor._buff,pcData.armor._current);
		unitLogic.mr = new AttributeInt(pcData.mr._base,pcData.mr._buff,pcData.mr._current);
		//elemental resist
		unitLogic.resistSol = new AttributeFloat (pcData.resistSol._base,pcData.resistSol._buff,pcData.resistSol._current);
		unitLogic.resistLuna = new AttributeFloat (pcData.resistLuna._base,pcData.resistLuna._buff,pcData.resistLuna._current);
		unitLogic.resistStella = new AttributeFloat (pcData.resistStella._base,pcData.resistStella._buff,pcData.resistStella._current);
		unitLogic.resistVoid = new AttributeFloat (pcData.resistVoid._base,pcData.resistVoid._buff,pcData.resistVoid._current);
		unitLogic.resistWind = new AttributeFloat (pcData.resistWind._base,pcData.resistWind._buff,pcData.resistWind._current);
		unitLogic.resistEarth = new AttributeFloat (pcData.resistEarth._base,pcData.resistEarth._buff,pcData.resistEarth._current);
		unitLogic.resistWater = new AttributeFloat (pcData.resistWater._base,pcData.resistWater._buff,pcData.resistWater._current);
		unitLogic.resistFire = new AttributeFloat (pcData.resistFire._base,pcData.resistFire._buff,pcData.resistFire._current);
		//dodge and accuracy
		unitLogic.dodge = new AttributeInt (pcData.dodge._base,pcData.dodge._buff,pcData.dodge._current);
		unitLogic.accuracy = new AttributeInt (pcData.accuracy._base,pcData.accuracy._buff,pcData.accuracy._current);
		//secondary stats
		unitLogic.regenHealth = new AttributeFloat(pcData.regenHealth._base,pcData.regenHealth._buff,pcData.regenHealth._current);

		if (pcData.sverdListId != -1) {
			unitLogic.fuel = new AttributeFloat (Persistence.Data ().sverdData [pcData.sverdListId].fuel._base, 0.0f, 0.0f);
			unitLogic.pilotSverdId = pcData.sverdListId;
		}else
			unitLogic.fuel = new AttributeFloat (0.0f);
	}

	//instantiate party unit
	public static GameObject InstantiateUnit(int unitId, Vector3 pos, Vector3 ori, int partyId){
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [unitId];
		GameObject newUnit = (GameObject) Instantiate(Resources.Load ("UnitPrefabs\\" + unitDBItem.prefab),pos,Quaternion.identity);
		newUnit.GetComponentInChildren<Unit> ().unitName = Persistence.GetUnitDB ().items [Persistence.Data ().pcData [Persistence.Data ().party [partyId]].id].name;
		//newUnit.GetComponentInChildren<Unit> ().transform.position = pos;
		newUnit.GetComponentInChildren<Unit> ().sprite.gameObject.transform.localScale = ori;
		newUnit.GetComponentInChildren<Unit> ().attackFrames = unitDBItem.attackFrames;
		newUnit.GetComponentInChildren<Unit> ().healFrames = unitDBItem.healFrames;

		GameObject newHealthbar = (GameObject)Instantiate (instance.healthbarPrefab);
		newHealthbar.transform.SetParent (instance.unitOverlay.transform,false);
		newUnit.GetComponentInChildren<Unit> ().healthbar = newHealthbar;

		InitializeUnitAttributes (newUnit.GetComponentInChildren<Unit>(),partyId);

		//link unit with controls
		UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList>();
		//uiPCControlList.RemoveControl (partyId);
		uiPCControlList.AddControl (newUnit.GetComponentInChildren<Unit> (), partyId);

		return newUnit;
	}

	//instantiate sverd
	public static GameObject InstantiateSverd(int unitId, Vector3 pos, Vector3 ori, int partyId){
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [unitId];
		GameObject newUnit = (GameObject) Instantiate(Resources.Load ("UnitPrefabs\\" + unitDBItem.prefab),pos,Quaternion.identity);
		newUnit.GetComponentInChildren<Unit> ().unitName = Persistence.GetUnitDB ().items [unitId].name;
		//newUnit.GetComponentInChildren<Unit> ().transform.position = pos;
		newUnit.GetComponentInChildren<Unit> ().sprite.gameObject.transform.localScale = ori;

		//initialize stats
		SverdData sverdData = Persistence.Data().sverdData[Persistence.Data().pcData[Persistence.Data ().party [partyId]].sverdListId];
		Unit unitLogic = newUnit.GetComponentInChildren<Unit> ();
		unitLogic.id = unitId;
		unitLogic.controlId = partyId;
		//initialize attributes
		unitLogic.attackFrames = unitDBItem.attackFrames;
		unitLogic.healFrames = unitDBItem.healFrames;
		unitLogic.health = new AttributeInt(sverdData.health._base,sverdData.health._buff,sverdData.health._current);
		unitLogic.mp = new AttributeInt (sverdData.mp._base,sverdData.mp._buff,sverdData.mp._current);
		unitLogic.shield = new AttributeInt(sverdData.shield._base,sverdData.shield._buff,sverdData.shield._current);
		unitLogic.barrier = new AttributeInt(sverdData.barrier._base,sverdData.barrier._buff,sverdData.barrier._current);
		unitLogic.speed = new AttributeFloat (sverdData.speed._base,sverdData.speed._buff,sverdData.speed._current);
		unitLogic.cooldown = new AttributeFloat(sverdData.cooldown._base,sverdData.cooldown._buff,sverdData.cooldown._current);
		unitLogic.cooldownSpecial = new AttributeFloat(sverdData.cooldownSpecial._base,sverdData.cooldownSpecial._buff,sverdData.cooldownSpecial._current);
		unitLogic.rangeAtk = new AttributeFloat(sverdData.rangeAtk._base,sverdData.rangeAtk._buff,sverdData.rangeAtk._current);
		unitLogic.rangeHeal = new AttributeFloat(sverdData.rangeHeal._base,sverdData.rangeHeal._buff,sverdData.rangeHeal._current);
		unitLogic.attack = new AttributeInt(sverdData.attack._base,sverdData.attack._buff,sverdData.attack._current);
		unitLogic.magic = new AttributeInt(sverdData.magic._base,sverdData.magic._buff,sverdData.magic._current);
		unitLogic.heal = new AttributeInt(sverdData.heal._base,sverdData.heal._buff,sverdData.heal._current);
		unitLogic.armor = new AttributeInt(sverdData.armor._base,sverdData.armor._buff,sverdData.armor._current);
		unitLogic.mr = new AttributeInt(sverdData.mr._base,sverdData.mr._buff,sverdData.mr._current);
		//elemental resist
		unitLogic.resistSol = new AttributeFloat (sverdData.resistSol._base,sverdData.resistSol._buff,sverdData.resistSol._current);
		unitLogic.resistLuna = new AttributeFloat (sverdData.resistLuna._base,sverdData.resistLuna._buff,sverdData.resistLuna._current);
		unitLogic.resistStella = new AttributeFloat (sverdData.resistStella._base,sverdData.resistStella._buff,sverdData.resistStella._current);
		unitLogic.resistVoid = new AttributeFloat (sverdData.resistVoid._base,sverdData.resistVoid._buff,sverdData.resistVoid._current);
		unitLogic.resistWind = new AttributeFloat (sverdData.resistWind._base,sverdData.resistWind._buff,sverdData.resistWind._current);
		unitLogic.resistEarth = new AttributeFloat (sverdData.resistEarth._base,sverdData.resistEarth._buff,sverdData.resistEarth._current);
		unitLogic.resistWater = new AttributeFloat (sverdData.resistWater._base,sverdData.resistWater._buff,sverdData.resistWater._current);
		unitLogic.resistFire = new AttributeFloat (sverdData.resistFire._base,sverdData.resistFire._buff,sverdData.resistFire._current);
		//dodge and accuracy
		unitLogic.dodge = new AttributeInt (sverdData.dodge._base,sverdData.dodge._buff,sverdData.dodge._current);
		unitLogic.accuracy = new AttributeInt (sverdData.accuracy._base,sverdData.accuracy._buff,sverdData.accuracy._current);
		//secondary stats
		unitLogic.regenHealth = new AttributeFloat(sverdData.regenHealth._base,sverdData.regenHealth._buff,sverdData.regenHealth._current);
		unitLogic.fuel = new AttributeFloat (sverdData.fuel._base,sverdData.fuel._buff,sverdData.fuel._current);

		GameObject newHealthbar = (GameObject)Instantiate (instance.healthbarPrefab);
		newHealthbar.transform.SetParent (instance.unitOverlay.transform,false);
		newUnit.GetComponentInChildren<Unit> ().healthbar = newHealthbar;

		//link unit with controls
		UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList>();
		//uiPCControlList.RemoveControl (partyId);
		uiPCControlList.AddControl (newUnit.GetComponentInChildren<Unit> (), partyId);

		return newUnit;
	}

	public static void Engage(Unit unitA, Unit unitB, int denom, ENGAGEMENTTYPE engagementType){
		float stanceMultiplier = 1.0f;
		if (unitB.GetComponent<PC> () != null) {
			if (unitB.GetComponent<PC> ().stance == STANCE.SENTINEL)
				stanceMultiplier = 0.5f;
		}

		switch (engagementType) {
		case ENGAGEMENTTYPE.ATTACK:{
				//miss chance
				if (Random.Range (0.0f, 1.0f) > (float)unitA.accuracy._current / (float)unitB.dodge._current) {
					GameObject missed = Instantiate(instance.hitNumberPrefab);
					missed.transform.SetParent (instance.unitOverlay.transform);
					Vector3 screenMissedPos = unitB.GetOverheadScreenPos ();
					missed.GetComponent<RectTransform> ().position = new Vector3(screenMissedPos.x,screenMissedPos.y,0);
					missed.GetComponent<Text>().text = "MISSED";
					missed.GetComponent<Text> ().color = new Color (1, 0, 0);
					return;
				}

				//penetration and enemy's def
				int res = unitB.armor._current;// - Mathf.FloorToInt(unitB.armor._current * unitA.penetration._current);
				float multiplier;
				if (res >= 0)
					multiplier = 100 / (100 + (float)res);
				else
					multiplier = 2 - (100 / (100 - (float)res));
				denom = Mathf.FloorToInt (multiplier * (float)denom * stanceMultiplier);

				//generate hitnumber
				GameObject hitNumber = Instantiate(instance.hitNumberPrefab);
				hitNumber.transform.SetParent (instance.unitOverlay.transform);
				Vector3 screenPos = unitB.GetOverheadScreenPos();
				hitNumber.GetComponent<RectTransform> ().position = new Vector3(screenPos.x,screenPos.y);
				hitNumber.GetComponent<Text>().text = Mathf.Abs (denom).ToString();
				if (denom > 0)
					hitNumber.GetComponent<Text> ().color = new Color (0, 1, 0);
				else
					hitNumber.GetComponent<Text> ().color = new Color (1, 0, 0);

				//update enmity
				int enmityMultiplier = 1;
				if (unitA.GetComponent<PC> () != null) {
					if (unitA.GetComponent<PC> ().stance == STANCE.SENTINEL)
						enmityMultiplier *= 4;
				}

				int idOfEnmity = unitB.GetIndexOfEnmity(unitA);
				if (idOfEnmity != -1) {
					//Debug.Log ("existing enmity");
					unitB.enmities [idOfEnmity].value += -1* denom * enmityMultiplier;
				} else {
					//Debug.Log ("new enmity");
					Enmity newEnmity = new Enmity();
					newEnmity.unit = unitA;
					newEnmity.value = -1* denom * enmityMultiplier;
					unitB.enmities.Add (newEnmity);
				}

				//update health
				unitB.health.Add(denom);

				//update healthbars
				//unitB.UpdateHealthbar();
				//GameObject unitBHealthbar = unitB.gameObject.transform.FindChild("Healthbar").gameObject;
				//unitBHealthbar.GetComponent<Healthbar> ().UpdateHealthbar();

				if (unitB.health._current <= 0) {
					unitB.isDown = true;
					unitB.ResetUnitAction ();
					unitB.DownEvent ();
				}
			}break;
		case ENGAGEMENTTYPE.HEAL:{
				//generate hitnumber
				GameObject hitNumber = Instantiate(instance.hitNumberPrefab);
				hitNumber.transform.SetParent (instance.unitOverlay.transform);
				Vector3 screenPos = unitB.GetOverheadScreenPos();
				hitNumber.GetComponent<RectTransform> ().position = new Vector3(screenPos.x,screenPos.y);
				hitNumber.GetComponent<Text>().text = Mathf.Abs (denom).ToString();
				if (denom > 0)
					hitNumber.GetComponent<Text> ().color = new Color (0, 1, 0);
				else
					hitNumber.GetComponent<Text> ().color = new Color (1, 0, 0);

				//update health
				Debug.Log("HEALING");
				unitB.health.Add(denom);

				//update healthbars
				//unitB.UpdateHealthbar();
				//GameObject unitBHealthbar = unitB.gameObject.transform.FindChild("Healthbar").gameObject;
				//unitBHealthbar.GetComponent<Healthbar> ().UpdateHealthbar();

				if (unitB.health._current <= 0) {
					unitB.isDown = true;
					unitB.ResetUnitAction ();
					unitB.DownEvent ();
				}
			}break;
		}
	}
}
