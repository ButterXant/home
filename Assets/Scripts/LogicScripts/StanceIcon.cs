﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StanceIcon : MonoBehaviour {
	public float duration  = 1.0f;
	private float lifetime = 0.0f;
	public GameObject medicIcon;
	public GameObject sentinelIcon;
	public GameObject vanguardIcon;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		lifetime += Time.deltaTime;
		//rectTransform.localPosition = new Vector3 (rectTransform.position.x, rectTransform.position.y, rectTransform.position.z + Time.deltaTime * 4);
		medicIcon.GetComponent<SpriteRenderer>().color = new Color(1.0f,1.0f,1.0f,0.5f - 0.5f * (lifetime / duration));
		sentinelIcon.GetComponent<SpriteRenderer>().color = new Color(1.0f,1.0f,1.0f,0.5f - 0.5f * (lifetime / duration));
		vanguardIcon.GetComponent<SpriteRenderer>().color = new Color(1.0f,1.0f,1.0f,0.5f - 0.5f * (lifetime / duration));
		if (lifetime > duration)
			Destroy (this.gameObject);
	}
}
