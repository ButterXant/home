﻿//script to control the camera movement to follow currently selected character
//or to focus on a certain coordinate for when there are events

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the camera mode
//FOLLOW: stick on currently selected character
//CATCHUP: move camera to point to currently selected character
//FREE: not constrained, moved by user through dragging
//FOCUS: go to a certain position
public enum CAMERAMODE {FOLLOW, CATCHUP, FREE, FOCUS};

public class CameraMgr : MonoBehaviour {
	public static CAMERAMODE cameraMode = CAMERAMODE.CATCHUP; //initialize mode to catchup
	public static Vector3 focusTarget; //variable to store the coordinate we're focussing on
	
	// Update is called once per frame
	void Update () {
		switch (cameraMode) { //check the camera mode to determine its behavior
		case CAMERAMODE.FOCUS:
			{
				//FOCUS: go to a certain position
				Vector3 targetPos = focusTarget;
				float deltaPos = Vector3.Distance (Camera.main.gameObject.transform.position, targetPos);
				float step = deltaPos * Time.deltaTime;
				Camera.main.gameObject.transform.position = Vector3.MoveTowards (Camera.main.gameObject.transform.position, targetPos, step);
				if (Vector3.Distance (Camera.main.gameObject.transform.position, targetPos) <= 0.01f) //switch to FREE mode once target is reached
					cameraMode = CAMERAMODE.FREE;
			}
			break;
		case CAMERAMODE.FOLLOW:
			{
				//FOLLOW: stick on currently selected character
				if (Control.selectedUnit != null) { //make sure we have a selected unit
					//use different ortographic size between focusing on a PC and a Sverd
					if (Control.selectedUnit.GetComponent<Sverd> () != null) { 
						float targetSize = 15;
						float curSize = Camera.main.orthographicSize;
						float deltaSize = targetSize - curSize;
						float newSize = Camera.main.orthographicSize + deltaSize * Time.deltaTime;
						if (newSize > 15)
							newSize = 15;
						Camera.main.orthographicSize = newSize;
					} else if (Control.selectedUnit.GetComponent<PC> () != null) {
						float targetSize = 5;
						float curSize = Camera.main.orthographicSize;
						float deltaSize = targetSize - curSize;
						float newSize = Camera.main.orthographicSize + deltaSize * Time.deltaTime;
						if (newSize < 5)
							newSize = 5;
						Camera.main.orthographicSize = newSize;
					}

					//keep moving the camera to follow the target
					Camera.main.gameObject.transform.position = new Vector3 (Control.selectedUnit.transform.position.x, Control.selectedUnit.transform.position.y, -10);
				}
			}
			break;
		case CAMERAMODE.CATCHUP:
			{
				//CATCHUP: move camera to point to currently selected character
				if (Control.selectedUnit != null) { //make sure we have a selected unit
					//use different ortographic size between focusing on a PC and a Sverd
					if (Control.selectedUnit.GetComponent<Sverd> () != null) {
						float targetSize = 15;
						float curSize = Camera.main.orthographicSize;
						float deltaSize = targetSize - curSize;
						float newSize = Camera.main.orthographicSize + deltaSize * Time.deltaTime;
						if (newSize > 15)
							newSize = 15;
						Camera.main.orthographicSize = newSize;
					} else if (Control.selectedUnit.GetComponent<PC> () != null) {
						float targetSize = 5;
						float curSize = Camera.main.orthographicSize;
						float deltaSize = targetSize - curSize;
						float newSize = Camera.main.orthographicSize + deltaSize * Time.deltaTime;
						if (newSize < 5)
							newSize = 5;
						Camera.main.orthographicSize = newSize;
					}

					//close in onto the selected target
					Vector3 targetPos = new Vector3 (Control.selectedUnit.transform.position.x, Control.selectedUnit.transform.position.y, -10);
					float deltaPos = Vector3.Distance (Camera.main.gameObject.transform.position, targetPos);
					float step = deltaPos * Time.deltaTime;
					Camera.main.gameObject.transform.position = Vector3.MoveTowards (Camera.main.gameObject.transform.position, targetPos, step);

					//if we're close enough to the target, switch to FOLLOW
					if (Vector3.Distance (Camera.main.gameObject.transform.position, targetPos) <= 0.01f)
						cameraMode = CAMERAMODE.FOLLOW;
				}
			}
			break;
		}
	}
}
