﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit {
	//when an enemy is down, tally exp and gold before destroying it
	public override void DownEvent(){
		UnitDBItem unitDBItem = Persistence.GetUnitDB ().items [id];
		Persistence.TallyExp (unitDBItem.exp.ValueAtLvl(lvl));
		UnitMgr.DestroyUnit (this);
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["EnemyUpdate2"], this,Time.deltaTime);
	}
}
