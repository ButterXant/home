﻿//gameevent class handles in game events (e.g. popping up dialogues, opening shop ui, etc.)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

//the kind of trigger that will activate the event
public enum Trigger {ONPLAYERINTERACT, ONNPCINTERACT, AUTORUN, PARALLELPROCESS, ONPLAYERENTER};

//1 event page represents 1 event
[Serializable]
public class EventPage{ //define a set of logic that the event should execute
	public Trigger trigger = Trigger.ONPLAYERINTERACT; //how this event is triggered
	public List<EventSwitch> switches; //conditions global
	public EventSwitch selfSwitch; //condition local
	public List<String> commands; //list of logic in the event (will be parsed on update when the event page is active)
}

//a conditional switch that determines whether the event should be activated when triggered
[Serializable]
public class EventSwitch{ //define the condition that an event page should be executed
	public String switchName = "";
	//public bool value = false;
	public int value = 0;

	public EventSwitch(){}
	public EventSwitch(string switchName, int value){
		this.switchName = switchName;
		this.value = value;
	}
}

public class GameEvent : MonoBehaviour {
	public List<EventPage> eventPages; //the list of event pages associated with this unit
	public List<EventSwitch> selfSwitches; //local flags to this unit
	public int activePage = -1; //the active event page index

	//for dialogue
	public bool issuingCommand = false; //when the dialogue is active, we will hald the update from checking for other events
	public int branch = 0; //if a dialogue has options, we store what users select in this variable
	
	// Update is called once per frame
	void Update () {
		if (activePage < 0) { 
			//there's no active page, search for possible event page from the last event page in the list
			int i = eventPages.Count - 1;
			bool found = false;
			while ((i >= 0) && !found) {
				EventPage curPage = eventPages [i];
				bool conditionsMet = true;
				int j = 0;
				//check all the switches 
				while ((conditionsMet) && (j < curPage.switches.Count)) { 
					conditionsMet = conditionsMet && CheckSwitch (curPage.switches [j]);
					if (conditionsMet)
						j++;
				}

				//only check autorun or parallel process events, check for selfswitch as well
				found = conditionsMet && CheckSelf (curPage.selfSwitch) && ((curPage.trigger == Trigger.AUTORUN) || (curPage.trigger == Trigger.PARALLELPROCESS));
				if (!found)
					i--;
			}	
			if (found) {
				//start an eventpage coroutine
				StartCoroutine (EventPageCoroutine (i));
			}
		} 
	}

	//coroutines for running an eventpage
	IEnumerator EventPageCoroutine(int pageId){
		//set the active page
		activePage = pageId;

		//parse each command in the event page from top to bottom and do the logic of the command
		for (int i = 0; i < eventPages [pageId].commands.Count; i++) {
			String[] commandParams = eventPages [pageId].commands [i].Split('$');
			switch (commandParams [0]) {
			case "incrementgameswitch":
				{
					//syntax: incrementgameswitch$<switch_name>
					//increment game switch in persistent data
					String switchName = commandParams [1];
					bool found = false;
					int j = 0;
					while ((j < Persistence.Data().switches.Count) && !found) {
						if (Persistence.Data().switches [j].switchName == switchName) {
							Persistence.Data ().switches [j].value++;
							found = true;
						} else
							j++;
					}

					//if no existing switch with that name found, add one
					if (!found)
						Persistence.Data ().switches.Add (new EventSwitch (switchName, 1));//(switchValue == "1") ? true : false));
				}
				break;
			case "incrementselfswitch":
				{
					//syntax: incrementselfswitch$<switch_name>
					//increment local switch
					String switchName = commandParams [1];
					bool found = false;
					int j = 0;
					while ((j < selfSwitches.Count) && !found) {
						if (selfSwitches [j].switchName == switchName) {
							selfSwitches [j].value++;
							found = true;
						} else
							j++;
					}

					//if no existing switch with that name found, add one
					if (!found)
						selfSwitches.Add (new EventSwitch (switchName, 1));//(switchValue == "1") ? true : false));
				}
				break;
			case "selfswitch":{
					//syntax: selfswitch$<switch_name>$<value>
					//set self switch
					String switchName = commandParams [1];
					String switchValue = commandParams [2];
					bool found = false;
					int j = 0;
					while ((j < selfSwitches.Count) && !found) {
						if (selfSwitches [j].switchName == switchName) {
							selfSwitches [j].value = int.Parse (switchValue);//(switchValue == "1") ? true : false;
							found = true;
						} else
							j++;
					}

					if (!found)
						selfSwitches.Add (new EventSwitch (switchName, int.Parse (switchValue)));//(switchValue == "1") ? true : false));
				}break;
			case "gameswitch":{
					//syntax: gameswitch$<switch_name>$<value>
					//set game switch
					String switchName = commandParams [1];
					String switchValue = commandParams [2];
					bool found = false;
					int j = 0;
					while ((j < Persistence.Data().switches.Count) && !found) {
						if (Persistence.Data().switches [j].switchName == switchName) {
							Persistence.Data ().switches [j].value = int.Parse (switchValue);//(switchValue == "1") ? true : false;
							found = true;
						} else
							j++;
					}

					if (!found)
						Persistence.Data ().switches.Add (new EventSwitch (switchName, int.Parse (switchValue)));//(switchValue == "1") ? true : false));
				}break;
			case "save":
				{
					//syntax: save 
					//save persistent data to file
					Persistence.Save ();
				}
				break;
			case "shop":
				{
					//syntax: shop$<item_id1>,<item_id2>,...,<item_idn>
					//open the shop interface selling item_id1 to item_idn
					string[] itemIdsString = commandParams[1].Split(',');
					List<int> itemIds = new List<int> ();
					for(int j=0; j < itemIdsString.Length; j++)
						itemIds.Add(int.Parse(itemIdsString[j]));
					UIMain.instance.OpenShop (itemIds);
				}
				break;
			case "camera":
				{
					//syntax: camera$<x>,<y>,<z>
					//move camera to focus on a coordinate
					string[] targetPosString = commandParams [1].Split (',');
					Vector3 targetPos = new Vector3 (float.Parse (targetPosString [0]), float.Parse (targetPosString [1]), float.Parse (targetPosString [2]));
					CameraMgr.focusTarget = targetPos;
					CameraMgr.cameraMode = CAMERAMODE.FOCUS;
				}
				break;
			case "switchscene":
				{
					//syntax: switchscene$<scene_name>$<x1,y1,z1>$<x2,y2,z2>$<x3,y3,z3>$<x4,y4,z4>$<h1,p1,r1>$<h2,p2,r3>$<h4,p4,r4>
					//switch to the scene <scene_name> with initial unit positions and orientations specified by the rest of the parameters
					string sceneName = commandParams [1];
					Persistence.Data ().unitPos.Clear ();
					Persistence.Data ().unitPos.Add(commandParams[2]);
					Persistence.Data ().unitPos.Add(commandParams[3]);
					Persistence.Data ().unitPos.Add(commandParams[4]);
					Persistence.Data ().unitPos.Add(commandParams[5]);
					Persistence.Data ().unitOri.Clear ();
					Persistence.Data ().unitOri.Add(commandParams[6]);
					Persistence.Data ().unitOri.Add(commandParams[7]);
					Persistence.Data ().unitOri.Add(commandParams[8]);
					Persistence.Data ().unitOri.Add(commandParams[9]);
					SceneManager.LoadScene(sceneName);
				}
				break;
			case "destroy":
				{
					//syntax: destroy
					//destroy this object (useful to destroy unneeded event )
					Destroy (this.transform.parent.gameObject);
				}
				break;
			case "unlock":
				{
					//syntax: unlock
					//if this gameevent is attached to a unit, unlock this unit
					Unit unit = GetComponent<Unit> ();
					if (unit) {
						unit.locked = false;
						if (unit.target)
							unit.target.GetComponent<Unit> ().locked = false;
					}
				}
				break;
			case "talk":
				{
					//syntax: talk$<name>$<faceset_id>$<text>
					//open ui dialogue
					UIDialogue uiDialogue = GameObject.FindObjectOfType<UIDialogue>();
					while(uiDialogue.occupied)
						yield return null;
					uiDialogue.StartDialogue (this,commandParams[1],commandParams [2],commandParams [3]);
					while (issuingCommand)
						yield return null;
				}
				break;
			case "option":
				{
					//syntax: option$<name>$<faceset_id>$<text>$<option1>:<command_id1>$<option2>:<command_id2>$<option3>:<command_id3>
					//open option dialogue
					UIDialogue uiDialogue = GameObject.FindObjectOfType<UIDialogue>();
					while(uiDialogue.occupied)
						yield return null;

					int nOptions = commandParams.Length - 4;
					branch = 0;
					uiDialogue.StartDialogue (this,commandParams[1], commandParams [2],commandParams[3], (nOptions>0)?((commandParams[4].Split(':'))[0]):null,(nOptions>1)?((commandParams[5].Split(':'))[0]):null,(nOptions>2)?((commandParams[6].Split(':'))[0]):null);
					while (issuingCommand)
						yield return null;
					i = int.Parse (commandParams [4 + branch].Split (':') [1]) - 1;
				}
				break;
			case "sidedialogue":
				{
					//syntax: sidedialogue$<faceset_id>$<text>
					//open side dialogue
					UISideDialogueList.AddSideDialogue (commandParams [1], commandParams [2]);
				}
				break;
			case "face":
				{
					//syntax: face
					//if this gameevent is attached to a gameobject, make it face its currently targetted unit
					Unit unit = GetComponent<Unit> ();
					if (unit) {
						GameObject targetOfUnit = unit.target;
						if (targetOfUnit) {
							//calculate direction of unit
							Vector3 dir = (targetOfUnit.transform.position - transform.position).normalized;
							if (dir.x < 0)
								unit.sprite.transform.localScale = new Vector3 (-1, 1, 1);
							else if (dir.x > 0)
								unit.sprite.transform.localScale = new Vector3 (1, 1, 1);
						}
					}
				}
				break;
			case "addpc":
				{
					//syntax: addpc$<unit_id>$<level>$<position>&<orientation>
					//add pc to persistent data and instantiate it in the position and orientation if possible
					int addedUnitId = int.Parse(commandParams [1]);
					int addedUnitLvl = int.Parse(commandParams [2]);
					string[] posstring = commandParams [3].Split(',');
					Vector3 pos = new Vector3(float.Parse(posstring[0]),float.Parse(posstring[1]),float.Parse(posstring[2]));
					string[] oristring = commandParams [4].Split(',');
					Vector3 ori = new Vector3(float.Parse(oristring[0]),float.Parse(oristring[1]),float.Parse(oristring[2]));
					Persistence.AddPC (addedUnitId, addedUnitLvl,pos,ori);
				}
				break;
			case "addsverd":
				{
					//syntax: addsverd$<sverd_id>
					//add new sverd in the inventory
					int addedUnitId = int.Parse(commandParams [1]);
					Persistence.AddSverd (addedUnitId);
				}
				break;
			case "assignsverd":
				{
					//syntax: assignsverd$<assigned_id>$<assignee_id>
					//assign owned sverd to one of the units
					int assignedListId = int.Parse(commandParams [1]);
					int assigneeListId = int.Parse (commandParams [2]);
					Persistence.AssignSverd (assignedListId, assigneeListId);

					//check if assignee is an instantiated party
					int j = 0;
					bool instantiatedParty = false;
					while (!instantiatedParty && (j < 4) && (j < Persistence.Data ().party.Count)) {
						if (assigneeListId == Persistence.Data ().party [j]) {
							instantiatedParty = true;
							UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList> ();
							UIPCControl uiPCControl = uiPCControlList.controlSlots [j].GetComponentInChildren<UIPCControl> ();
							if (uiPCControl != null) {
								if (uiPCControl.unitTarget != null) {
									uiPCControl.unitTarget.fuel = new AttributeFloat (Persistence.Data ().sverdData [assignedListId].fuel._base, 0.0f, 0.0f);
									uiPCControl.unitTarget.pilotSverdId = assignedListId;
								}
							}
						} else
							j++;
					}
				}
				break;
			case "spawn":
				{
					//syntax: spawn$<time_to_spawn>$<unit_id>$<unit_lvl>$<x,y,z>$<h,p,r>
					//spawn a unit every <time_to_spawn>
					string waitSpawn = commandParams [1];
					int addedUnitId = int.Parse(commandParams [2]);
					int addedUnitLvl = int.Parse(commandParams [3]);
					string[] posstring = commandParams [4].Split(',');
					Vector3 pos = new Vector3(float.Parse(posstring[0]),float.Parse(posstring[1]),float.Parse(posstring[2]));
					string[] oristring = commandParams [5].Split(',');
					Vector3 ori = new Vector3(float.Parse(oristring[0]),float.Parse(oristring[1]),float.Parse(oristring[2]));
					//Persistence.AddPC (addedUnitId, addedUnitLvl,pos);
					GameObject spawned = UnitMgr.InstantiateUnit(addedUnitId,pos,ori,true,addedUnitLvl);

					if(waitSpawn == "1"){
						while (spawned != null)
							yield return null;
					}
				}
				break;
			case "yield":
				{
					//syntax: yield$<wait_time>
					//make the game event idle for <wait_time>
					float yieldTime = float.Parse (commandParams [1]);
					float startTime = Time.time;
					float curTime = startTime;
					while(curTime - startTime < yieldTime) {
						curTime = Time.time;
						yield return null;
					} 
				}
				break;
			case "additem":
				{
					//syntax: additem$<item_id>
					//add item
					int addedItemId = int.Parse(commandParams [1]);
					Persistence.AddItem (addedItemId);
				}
				break;
			case "addgold":
				{
					//syntax: addgold$<amount>
					//add gold to persistent data
					Persistence.AddGold (int.Parse (commandParams [1]));
				}
				break;
			}
			yield return null;
		}
		activePage = -1;
	}

	//check whether global switch conditions are met
	public bool CheckSwitch(EventSwitch eventSwitch){
		bool found = false;
		bool result = false;
		int i = 0;
		while (!found && (i < Persistence.Data ().switches.Count)) {
			EventSwitch curSwitch = Persistence.Data ().switches [i];
			if (curSwitch.switchName == eventSwitch.switchName) {
				found = true;
				result = (curSwitch.value == eventSwitch.value);
				return result;
			} else
				i++;
		}

		if (!found) {
			if (eventSwitch.value == 1)
				return true;
			else 
				return false;
		}

		return result;
	}

	//check whether selfswitch conditions are met
	public bool CheckSelf(EventSwitch selfSwitch){
		if (selfSwitch.switchName == "")
			return true;

		bool found = false;
		bool result = false;
		int i = 0;
		while (!found && (i < selfSwitches.Count)) {
			EventSwitch curSwitch = selfSwitches [i];
			if (curSwitch.switchName == selfSwitch.switchName) {
				found = true;
				result = (curSwitch.value == selfSwitch.value);
				return result;
			} else
				i++;
		}

		if (!found) {
			if (selfSwitch.value == 0)
				return true;
			else 
				return false;
		}

		return result;
	} 

	//triggerred wen a unit interacts with this game event
	public void Interact(){
		Debug.Log ("NPC Interacted");
		
		if (activePage >= 0) { //there's an active event, stop the coroutine
			StopCoroutine(EventPageCoroutine(activePage));
			//ResetUnitAction ();
			activePage = -1;
		}

		//find interactive event
		int i = eventPages.Count - 1;
		bool found = false;
		while ((i >= 0) && !found) {
			EventPage curPage = eventPages [i];
			bool conditionsMet = true;
			int j = 0;
			while ((conditionsMet) && (j < curPage.switches.Count)) { 
				conditionsMet = conditionsMet && CheckSwitch (curPage.switches [j]);
				if (conditionsMet)
					j++;
			}

			//only check event pages that require interaction
			found = conditionsMet && CheckSelf (curPage.selfSwitch) && ((curPage.trigger == Trigger.ONPLAYERINTERACT) || (curPage.trigger == Trigger.ONNPCINTERACT));
			if (!found)
				i--;
			else {
				//target = targetUnit;
				StartCoroutine (EventPageCoroutine (i));
			}
		}
	}

	//triggered when player enters
	public void OnTriggerEnter(Collider other){
		if (other.gameObject.GetComponent<PC> () == null)
			return;

		GameObject targetUnit = other.gameObject; 

		if (activePage >= 0) { //there's an active event, stop the coroutine
			StopCoroutine(EventPageCoroutine(activePage));
			//ResetUnitAction ();
			activePage = -1;
		}

		//find interactive event
		int i = eventPages.Count - 1;
		bool found = false;
		while ((i >= 0) && !found) {
			EventPage curPage = eventPages [i];
			bool conditionsMet = true;
			int j = 0;
			while ((conditionsMet) && (j < curPage.switches.Count)) { 
				conditionsMet = conditionsMet && CheckSwitch (curPage.switches [j]);
				if (conditionsMet)
					j++;
			}

			//only check event pages that require interaction
			found = conditionsMet && CheckSelf (curPage.selfSwitch) && (curPage.trigger == Trigger.ONPLAYERENTER);
			if (!found)
				i--;
			else {
				//target = targetUnit;
				StartCoroutine (EventPageCoroutine (i));
			}
		}

		targetUnit.GetComponent<Unit>().target = null;
		targetUnit.GetComponent<Unit> ().ResetUnitAction ();
	}
}
