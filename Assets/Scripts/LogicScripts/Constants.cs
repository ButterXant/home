﻿//helper class to keep all commonly used constants

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants{
	public const float TOOFAR = 10.0f; 	//determine a unit is too far from the selected unit
	public const float DEFAULTSTOPDISTANCE = 0.05f; //minimum distance to stop pathfinding
	public const float HEALTHRESHOLD = 0.8f; //minimum health percentage to consider healing the unit
	public const float REGENTIME = 5.0f; //regen timer duration
	public const int MAXLVL = 99; //maximum level
	public static DatabaseAttributeInt EXPNEEDED = new DatabaseAttributeInt (100,999999,10); //experience needed to level up curve
}
