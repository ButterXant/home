﻿//persistent data. holds database as well and moonsharp behaviorlua interpreter
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using MoonSharp.Interpreter;

public class Persistence : MonoBehaviour {
	public static Persistence instance; //singleton
	public PlayerData data;
	public UnitDB unitDB;
	public ItemDB itemDB;
	public SkillDB skillDB;
	public Script behaviorLua;

	//attach an OnSceneLoaded event to scene manager to be called whenever the scene changes
	void OnEnable(){
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		if (scene.name == "Main")
			return; 

		//instantiates active units
		InstantiateActiveUnits ();
	}

	public static void InstantiateActiveUnits(){
		PC [] activePCs = GameObject.FindObjectsOfType<PC>();
		for (int j = activePCs.Length - 1; j >= 0; j--)
			UnitMgr.DestroyUnit(activePCs[j]);

		Sverd [] activeSverds = GameObject.FindObjectsOfType<Sverd>();
		for (int j = activeSverds.Length - 1; j >= 0; j--)
			UnitMgr.DestroyUnit(activeSverds[j]);

		//load party members
		int i = 0;
		while((i < 4) && (i < instance.data.party.Count)){
			string formatPosString = instance.data.unitPos [i].Replace ("(", ""); 
			formatPosString = formatPosString.Replace (")", "");
			string[] posstring = formatPosString.Split(',');
			string formatOriString = instance.data.unitOri [i].Replace ("(", "");
			formatOriString = formatOriString.Replace (")", "");
			string[] oristring = formatOriString.Split(',');

			//focus camera to first instantiated unit by default
			if (i == 0) 
				Camera.main.gameObject.transform.position = new Vector3 (float.Parse (posstring [0]), float.Parse (posstring [1]), -10);

			//link unit with controls
			UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList>();
			uiPCControlList.RemoveControl (i);
			UnitMgr.InstantiateUnit (instance.data.pcData[instance.data.party[i]].id,new Vector3(float.Parse(posstring[0]),float.Parse(posstring[1]),float.Parse(posstring[2])),new Vector3(float.Parse(oristring[0]),float.Parse(oristring[1]),float.Parse(oristring[2])),i);
			i++;
		}			
	}

	//call this whenever a pc is downed to check whether there's no pc anymoe in the scene, if not load main title
	public static void CheckAllPCDown(){
		//get pc controller list
		UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList>();
		if (uiPCControlList == null)
			return;

		//check pc down
		bool allDown = true;
		for (int i = 0; i < uiPCControlList.controlSlots.Count; i++) {
			//get pc controller
			if (uiPCControlList.controlSlots [i].transform.childCount > 0) {
				GameObject control = uiPCControlList.controlSlots [i].transform.GetChild(0).gameObject;
				if (control.GetComponent<UIPCControl> () != null) {
					UIPCControl controlLogic = control.GetComponent<UIPCControl> ();
					if (controlLogic.unitTarget != null) { 
						allDown = allDown && controlLogic.unitTarget.isDown;
					}
				}else if(control.GetComponent<UISverdControl> () != null){
					allDown = allDown && false;
				}
			}
		}

		if (allDown) {
			SceneManager.LoadScene("Main");
		}
	}
		
	void Awake(){
		//for singleton. to maintain persisence. don't destroy on scene change
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if(instance != this){
			Destroy (gameObject);
		}
	}

	public string FormatGeneralDBJson(string dataAsJson){
		//dataAsJson = dataAsJson.Replace ("},\n {\"id\":", "}},{\"id\":");
		dataAsJson = dataAsJson.Replace ("}]", "}]}");
		dataAsJson = dataAsJson.Replace ("[{", "{\"items\":[{");
		Debug.Log (dataAsJson);
		return dataAsJson;
	}

	//to format loaded unit.json before passed onto serializer
	public string FormatUnitDBJson(string dataAsJson){
		dataAsJson = dataAsJson.Replace ("\"health_min\":", "\"health\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"health_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"health_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"mp_min\":", "},\"mp\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"mp_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"mp_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"shield_min\":", "},\"shield\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"shield_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"shield_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"barrier_min\":", "},\"barrier\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"barrier_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"barrier_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"speed_min\":", "},\"speed\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"speed_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"speed_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"cooldown_min\":", "},\"cooldown\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"cooldown_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"cooldown_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"cooldownSpecial_min\":", "},\"cooldownSpecial\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"cooldownSpecial_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"cooldownSpecial_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"rangeAtk_min\":", "},\"rangeAtk\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"rangeAtk_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"rangeAtk_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"rangeHeal_min\":", "},\"rangeHeal\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"rangeHeal_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"rangeHeal_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"attack_min\":", "},\"attack\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"attack_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"attack_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"magic_min\":", "},\"magic\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"magic_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"magic_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"heal_min\":", "},\"heal\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"heal_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"heal_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"armor_min\":", "},\"armor\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"armor_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"armor_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"mr_min\":", "},\"mr\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"mr_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"mr_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistSol_min\":", "},\"resistSol\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistSol_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistSol_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistLuna_min\":", "},\"resistLuna\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistLuna_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistLuna_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistStella_min\":", "},\"resistStella\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistStella_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistStella_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistVoid_min\":", "},\"resistVoid\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistVoid_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistVoid_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistWind_min\":", "},\"resistWind\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistWind_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistWind_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistEarth_min\":", "},\"resistEarth\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistEarth_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistEarth_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistWater_min\":", "},\"resistWater\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistWater_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistWater_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"resistFire_min\":", "},\"resistFire\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"resistFire_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"resistFire_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"dodge_min\":", "},\"dodge\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"dodge_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"dodge_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"accuracy_min\":", "},\"accuracy\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"accuracy_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"accuracy_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"regenHealth_min\":", "},\"regenHealth\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"regenHealth_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"regenHealth_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace (", \"exp_min\":", "},\"exp\":{\"min\":");
		dataAsJson = dataAsJson.Replace ("\"exp_max\":", "\"max\":");
		dataAsJson = dataAsJson.Replace ("\"exp_mod\":", "\"modifier\":");
		dataAsJson = dataAsJson.Replace ("},\n {\"id\":", "}},{\"id\":");
		dataAsJson = dataAsJson.Replace ("}]", "}}]}");
		dataAsJson = dataAsJson.Replace ("[{", "{\"items\":[{");
		return dataAsJson;
	}

	//to initiate behaviorlua
	void SetupBehaviorLua(){
		UserData.RegisterAssembly ();
		UserData.RegisterProxyType<UnitProxy,Unit> (r => new UnitProxy (r));
		behaviorLua = new Script ();
	}

	// Use this for initialization
	void Start () {
		//load LUA
		SetupBehaviorLua();
		string filePath = Path.Combine (Application.streamingAssetsPath, "behavior.lua");
		if (File.Exists (filePath)) {
			//read lua script
			string luaString = File.ReadAllText (filePath);
			DynValue result = behaviorLua.DoString (luaString);
			Debug.Log (result.String);
		} else {
			Debug.LogError ("Cannot find behavior lua!");
		}


		//load unit database
		filePath = Path.Combine (Application.streamingAssetsPath, "unit.json");
		if (File.Exists (filePath)) {
			string dataAsJson = File.ReadAllText (filePath);
			dataAsJson = FormatUnitDBJson(dataAsJson);
			//Debug.Log (dataAsJson);
			unitDB = JsonUtility.FromJson<UnitDB> (dataAsJson);

			//shift all id to start from 0 since sql starts from 1
			for (int i = 0; i < unitDB.items.Length; i++) 
				unitDB.items [i].id = i;

			Debug.Log ("=== unit database loaded");
		} else {
			Debug.LogError ("Cannot find unit database file!");
		}

		//load item database
		filePath = Path.Combine (Application.streamingAssetsPath, "item.json");
		if (File.Exists (filePath)) {
			string dataAsJson = File.ReadAllText (filePath);
			dataAsJson = FormatGeneralDBJson (dataAsJson);
			itemDB = JsonUtility.FromJson<ItemDB> (dataAsJson);

			//shift all id to start from 0 since sql starts from 1
			for (int i = 0; i < itemDB.items.Length; i++) 
				itemDB.items [i].id = i;

			Debug.Log ("===item database loaded");
		} else {
			Debug.LogError ("Cannot find item database file!");
		}

		//load skill database
		filePath = Path.Combine(Application.streamingAssetsPath, "skill.json");
		if (File.Exists (filePath)) {
			string dataAsJson = File.ReadAllText (filePath);
			dataAsJson = FormatGeneralDBJson (dataAsJson);
			skillDB = JsonUtility.FromJson<SkillDB> (dataAsJson);

			//shift all id to start from 0 since sql starts from 1
			for (int i = 0; i < skillDB.items.Length; i++) 
				skillDB.items [i].id = i;

			Debug.Log ("===skill database loaded");
		} else {
			Debug.LogError ("Cannot find skill database file!");
		}

		//instantiate playerdata once at the beginning
		data = new PlayerData ();
	}

	//clean up on new game
	public static void ResetPlayerData(){
		instance.data = new PlayerData ();
	}
	
	// Update is called once per frame. to debug save and load
	void Update () {
		if(Input.GetKeyUp(KeyCode.S)){
			Save();
		}else if(Input.GetKeyUp(KeyCode.L)){
			Load();
		}
	}

	//call this before openning up ui main to synchronize instantiated units data with the persistent data
	public static void UpdateActiveUnitsPersistentData(){
		//get pc controller list
		UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList>();
		if (uiPCControlList == null)
			return;

		//update pos of units
		for (int i = 0; i < uiPCControlList.controlSlots.Count; i++) {
			//get pc controller
			if (uiPCControlList.controlSlots [i].transform.childCount > 0) {
				GameObject control = uiPCControlList.controlSlots [i].transform.GetChild(0).gameObject;
				if (control.GetComponent<UIPCControl> () != null) {
					UIPCControl controlLogic = control.GetComponent<UIPCControl> ();
					if (controlLogic.unitTarget != null) {
						instance.data.unitPos [i] = controlLogic.unitTarget.transform.localPosition.ToString ();
						instance.data.unitOri [i] = controlLogic.unitTarget.GetComponentInChildren<Unit>().sprite.gameObject.transform.localScale.ToString ();
					}
				}
			}
		}
	}

	//to save
	public static void Save(){
		Debug.Log("Saving data*************");

		//update scene name
		instance.data.sceneName = SceneManager.GetActiveScene().name;

		UpdateActiveUnitsPersistentData ();

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");
		if (file == null)
			Debug.Log ("file is null");
		else {
			if (instance.data == null)
				Debug.Log ("instance data = null");
			else
				bf.Serialize (file, instance.data);
		}
		file.Close ();
	}

	//to load
	public static bool Load(){
		Debug.Log("Loading data*************");
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			instance.data = (PlayerData)bf.Deserialize (file);
			file.Close ();

			//check for additional uninitialized field
			InitializeNewData();

			//load scene
			SceneManager.LoadScene(instance.data.sceneName);
			return true;
		} else {
			Debug.Log ("no data to load");
			return false;
		}
	}

	//initialize additional attributes dute to update
	public static void InitializeNewData(){
		Debug.Log ("initialize new data");
		if (instance.data.pcData == null)
			instance.data.pcData = new List<PCData> ();
		if (instance.data.party == null)
			instance.data.party = new List<int> ();
		if (instance.data.switches == null)
			instance.data.switches = new List<EventSwitch> ();
		if (instance.data.items == null)
			instance.data.items = new List<ItemData> ();
		if (instance.data.unitPos == null)
			instance.data.unitPos = new List<string> ();
		for (int i = instance.data.unitPos.Count; i < 4; i++) 
			instance.data.unitPos.Add ((new Vector3 (0.0f, 0.0f, 0.0f)).ToString());
		if (instance.data.unitOri == null)
			instance.data.unitOri = new List<string> ();
		for (int i = instance.data.unitOri.Count; i < 4; i++) 
			instance.data.unitOri.Add ((new Vector3 (1.0f, 1.0f, 1.0f)).ToString());
		for (int i = 0; i < instance.data.pcData.Count; i++) {
			if (instance.data.pcData [i].health == null)
				instance.data.pcData [i].health = new AttributeInt (0);
			if (instance.data.pcData [i].mp == null)
				instance.data.pcData [i].mp = new AttributeInt (0);
			if (instance.data.pcData [i].shield == null)
				instance.data.pcData [i].shield = new AttributeInt (0);
			if (instance.data.pcData [i].barrier == null)
				instance.data.pcData [i].barrier = new AttributeInt (0);
			if (instance.data.pcData [i].speed == null)
				instance.data.pcData [i].speed = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].cooldown == null)
				instance.data.pcData [i].cooldown = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].cooldownSpecial == null)
				instance.data.pcData [i].cooldownSpecial = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].rangeAtk == null)
				instance.data.pcData [i].rangeAtk = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].rangeHeal == null)
				instance.data.pcData [i].rangeHeal = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].attack == null)
				instance.data.pcData [i].attack = new AttributeInt (0);
			if (instance.data.pcData [i].magic == null)
				instance.data.pcData [i].magic = new AttributeInt (0);
			if (instance.data.pcData [i].heal == null)
				instance.data.pcData [i].heal = new AttributeInt (0);
			if (instance.data.pcData [i].armor == null)
				instance.data.pcData [i].armor = new AttributeInt (0);
			if (instance.data.pcData [i].mr == null)
				instance.data.pcData [i].mr = new AttributeInt (0);
			if (instance.data.pcData [i].resistSol == null)
				instance.data.pcData [i].resistSol = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistLuna == null)
				instance.data.pcData [i].resistLuna = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistStella == null)
				instance.data.pcData [i].resistStella = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistVoid == null)
				instance.data.pcData [i].resistVoid = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistWind == null)
				instance.data.pcData [i].resistWind = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistEarth == null)
				instance.data.pcData [i].resistEarth = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistWater == null)
				instance.data.pcData [i].resistWater = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].resistFire == null)
				instance.data.pcData [i].resistFire = new AttributeFloat (0.0f);
			if (instance.data.pcData [i].accuracy == null)
				instance.data.pcData [i].accuracy = new AttributeInt (0);
			if (instance.data.pcData [i].dodge == null)
				instance.data.pcData [i].dodge = new AttributeInt (0);
			if (instance.data.pcData [i].regenHealth == null)
				instance.data.pcData [i].regenHealth = new AttributeFloat (0.0f);
			instance.data.pcData [i].equipHead = -1;
			instance.data.pcData [i].equipBody = -1;
			instance.data.pcData [i].equipRArm = -1;
			instance.data.pcData [i].equipLArm = -1;
			instance.data.pcData [i].equipAcc1 = -1;
			instance.data.pcData [i].equipAcc2 = -1;
		}
	}

	//accessors for data, and databases
	public static PlayerData Data(){ return instance.data; }
	public static UnitDB GetUnitDB(){ return instance.unitDB; }
	public static ItemDB GetItemDB(){ return instance.itemDB; }
	public static SkillDB GetSkillDB(){ return instance.skillDB; }

	//to add pc to persistent data
	public static void AddPC(int unitId, int lvl, Vector3 pos, Vector3 ori){
		PCData newPCData = new PCData (unitId, instance.data.pcData.Count, lvl);
		instance.data.pcData.Add (newPCData);
		instance.data.party.Add (instance.data.pcData.Count - 1);
		if(newPCData.listId < 4)
			UnitMgr.InstantiateUnit (newPCData.id,pos,ori,newPCData.listId);
	}

	//to add sverd to persistent data
	public static void AddSverd(int unitId){
		SverdData newSverdData = new SverdData (unitId, instance.data.sverdData.Count);
		instance.data.sverdData.Add (newSverdData);
	}

	//to assign sverd to a particular unit from code
	public static void AssignSverd(int sverdListId, int unitListId){
		//check if already is equipping a sverd
		if (instance.data.pcData [unitListId].sverdListId != -1) { //unequip
			instance.data.sverdData[instance.data.pcData[unitListId].sverdListId].pcListId = -1;
		}
		instance.data.pcData [unitListId].sverdListId = sverdListId;

		//check if item is already equipped by someone else
		if (instance.data.sverdData[sverdListId].pcListId != -1) {
			instance.data.pcData [instance.data.sverdData[sverdListId].pcListId].sverdListId = -1;
		}

		//equip to current unit
		instance.data.sverdData[sverdListId].pcListId = unitListId;
	}

	//to add item to persisten data
	public static void AddItem(int itemId){
		ItemDBItem itemDBItem = instance.itemDB.items [itemId];
		if (itemDBItem.category == 0) { //stackable or consummable
			//find if there's already a stack in inventory
			bool found = false;
			int i = 0;
			while (!found && (i < instance.data.items.Count)) {
				if (instance.data.items [i].id == itemId) {
					found = true;
					instance.data.items [i].amount++;
				} else
					i++;
			}
			if (!found) { //add a new entry
				instance.data.items.Add (new ItemData (itemId, instance.data.items.Count,1));
			}
		} else {
			instance.data.items.Add (new ItemData (itemId, instance.data.items.Count,1));
		}
	}

	//to remove item from persistent data
	public static void RemoveItem(int itemListId){
		ItemDBItem itemDBItem = instance.itemDB.items [instance.data.items[itemListId].id];
		if (itemDBItem.category == 0) { //stackable or consummable
			instance.data.items[itemListId].amount--;
			if(instance.data.items[itemListId].amount <= 0){
				instance.ShiftInventoryFrom (itemListId);
				instance.data.items.RemoveAt (itemListId);
			}
		} else {
			DetachItem (itemListId);
			instance.ShiftInventoryFrom (itemListId);
			instance.data.items.RemoveAt (itemListId);
		}
	}

	//detach equipped item from someone who's equipping it
	public static void DetachItem(int itemListId){
		ItemDBItem itemDBItem = instance.itemDB.items [instance.data.items[itemListId].id];
		int equippedBy = instance.data.items [itemListId].equippedBy;
		if (equippedBy != -1) {
			switch (itemDBItem.category) {
			case 1:
				{
					instance.data.pcData [equippedBy].equipHead = -1;
				}
				break;
			case 2:
				{
					instance.data.pcData [equippedBy].equipBody = -1;
				}
				break;
			case 3:
				{
					if(instance.data.pcData [equippedBy].equipLArm == itemListId)
						instance.data.pcData [equippedBy].equipLArm = -1;
					if(instance.data.pcData [equippedBy].equipRArm == itemListId)
						instance.data.pcData [equippedBy].equipRArm = -1;
				}
				break;
			case 4:
				{
					if(instance.data.pcData [equippedBy].equipLArm == itemListId)
						instance.data.pcData [equippedBy].equipLArm = -1;
					if(instance.data.pcData [equippedBy].equipRArm == itemListId)
						instance.data.pcData [equippedBy].equipRArm = -1;
				}
				break;
			case 5:
				{
					if(instance.data.pcData [equippedBy].equipAcc1 == itemListId)
						instance.data.pcData [equippedBy].equipAcc1 = -1;
					if(instance.data.pcData [equippedBy].equipAcc2 == itemListId)
						instance.data.pcData [equippedBy].equipAcc2 = -1;
				}
				break;
			case 6:
				{
					if(instance.data.pcData [equippedBy].equipAcc1 == itemListId)
						instance.data.pcData [equippedBy].equipAcc1 = -1;
					if(instance.data.pcData [equippedBy].equipAcc2 == itemListId)
						instance.data.pcData [equippedBy].equipAcc2 = -1;
				}
				break;
			case 7:
				{
					instance.data.sverdData [equippedBy].equipHead = -1;
				}
				break;
			case 8:
				{
					instance.data.sverdData [equippedBy].equipBody = -1;
				}
				break;
			case 9:
				{
					if(instance.data.sverdData [equippedBy].equipLArm == itemListId)
						instance.data.sverdData [equippedBy].equipLArm = -1;
					if(instance.data.sverdData [equippedBy].equipRArm == itemListId)
						instance.data.sverdData [equippedBy].equipRArm = -1;
				}
				break;
			case 10:
				{
					if(instance.data.sverdData [equippedBy].equipLArm == itemListId)
						instance.data.sverdData [equippedBy].equipLArm = -1;
					if(instance.data.sverdData [equippedBy].equipRArm == itemListId)
						instance.data.sverdData [equippedBy].equipRArm = -1;
				}
				break;
			case 11:
				{
					if(instance.data.sverdData [equippedBy].equipAcc1 == itemListId)
						instance.data.sverdData [equippedBy].equipAcc1 = -1;
					if(instance.data.sverdData [equippedBy].equipAcc2 == itemListId)
						instance.data.sverdData [equippedBy].equipAcc2 = -1;
				}
				break;
			case 12:
				{
					if(instance.data.sverdData [equippedBy].equipAcc1 == itemListId)
						instance.data.sverdData [equippedBy].equipAcc1 = -1;
					if(instance.data.sverdData [equippedBy].equipAcc2 == itemListId)
						instance.data.sverdData [equippedBy].equipAcc2 = -1;
				}
				break;
			}
		}
	}

	//shift inventory item before removing an item
	public void ShiftInventoryFrom(int itemListId){
		//iterate all the item beyond this id and take care of the reference on the units equipping the item
		for (int i = itemListId + 1; i < instance.data.items.Count; i++) {
			ItemDBItem itemDBItem = instance.itemDB.items [instance.data.items[i].id];
			int equippedBy = instance.data.items [i].equippedBy;
			if (equippedBy != -1) {
				switch (itemDBItem.category) {
				case 1:
					{
						instance.data.pcData [equippedBy].equipHead--;
					}
					break;
				case 2:
					{
						instance.data.pcData [equippedBy].equipBody--;
					}
					break;
				case 3:
					{
						if(instance.data.pcData [equippedBy].equipLArm == i)
							instance.data.pcData [equippedBy].equipLArm--;
						if(instance.data.pcData [equippedBy].equipRArm == i)
							instance.data.pcData [equippedBy].equipRArm--;
					}
					break;
				case 4:
					{
						if(instance.data.pcData [equippedBy].equipLArm == i)
							instance.data.pcData [equippedBy].equipLArm--;
						if(instance.data.pcData [equippedBy].equipRArm == i)
							instance.data.pcData [equippedBy].equipRArm--;
					}
					break;
				case 5:
					{
						if(instance.data.pcData [equippedBy].equipAcc1 == i)
							instance.data.pcData [equippedBy].equipAcc1--;
						if(instance.data.pcData [equippedBy].equipAcc2 == i)
							instance.data.pcData [equippedBy].equipAcc2--;
					}
					break;
				case 6:
					{
						if(instance.data.pcData [equippedBy].equipAcc1 == i)
							instance.data.pcData [equippedBy].equipAcc1--;
						if(instance.data.pcData [equippedBy].equipAcc2 == i)
							instance.data.pcData [equippedBy].equipAcc2--;
					}
					break;
				case 7:
					{
						instance.data.sverdData [equippedBy].equipHead--;
					}
					break;
				case 8:
					{
						instance.data.sverdData [equippedBy].equipBody--;
					}
					break;
				case 9:
					{
						if(instance.data.sverdData [equippedBy].equipLArm == i)
							instance.data.sverdData [equippedBy].equipLArm--;
						if(instance.data.sverdData [equippedBy].equipRArm == i)
							instance.data.sverdData [equippedBy].equipRArm--;
					}
					break;
				case 10:
					{
						if(instance.data.sverdData [equippedBy].equipLArm == i)
							instance.data.sverdData [equippedBy].equipLArm--;
						if(instance.data.sverdData [equippedBy].equipRArm == i)
							instance.data.sverdData [equippedBy].equipRArm--;
					}
					break;
				case 11:
					{
						if(instance.data.sverdData [equippedBy].equipAcc1 == i)
							instance.data.sverdData [equippedBy].equipAcc1--;
						if(instance.data.sverdData [equippedBy].equipAcc2 == i)
							instance.data.sverdData [equippedBy].equipAcc2--;
					}
					break;
				case 12:
					{
						if(instance.data.sverdData [equippedBy].equipAcc1 == i)
							instance.data.sverdData [equippedBy].equipAcc1--;
						if(instance.data.sverdData [equippedBy].equipAcc2 == i)
							instance.data.sverdData [equippedBy].equipAcc2--;
					}
					break;
				}
			}
		}
	}

	//tally exp to all active units
	public static void TallyExp(int exp){
		int i = 0;
		while (i < 4 && i < Persistence.Data ().pcData.Count) {
			if(instance.data.pcData [instance.data.party [i]].lvl != Constants.MAXLVL){
				int remExp = exp;
				while (remExp > 0) {
					int toLvl = instance.data.pcData [instance.data.party [i]].exp._base - instance.data.pcData [instance.data.party [i]].exp._current;
					if (remExp >= toLvl) {
						instance.data.pcData [instance.data.party [i]].exp._current = instance.data.pcData [instance.data.party [i]].exp._base;
						instance.data.pcData [instance.data.party [i]].lvl++;
						if (instance.data.pcData [instance.data.party [i]].lvl != Constants.MAXLVL)
							instance.data.pcData [instance.data.party [i]].exp._base += Constants.EXPNEEDED.ValueAtLvl (instance.data.pcData [instance.data.party [i]].lvl);
						UpdatePCData (instance.data.party[i]);
						UpdateInstantiatedUnit(i);
					}else{
						instance.data.pcData [instance.data.party [i]].exp.Add (remExp);
					}
					remExp -= toLvl;
				}
			}
			i++;
		}
	}

	//to add gold
	public static void AddGold(int gold){
		Persistence.Data ().gold += gold;
	}

	//to subtract gold
	public static void SubtractGold(int gold){
		Persistence.Data ().gold -= gold;
	}

	//update current state of instantiated unit without reinstantiating
	public static void UpdateInstantiatedUnit(int controlId){
		UIPCControlList uiPCControlList = GameObject.FindObjectOfType<UIPCControlList> ();
		GameObject uiControl = uiPCControlList.controlSlots [controlId];
		if (uiControl.transform.childCount > 0) {
			GameObject childControl = uiControl.transform.GetChild (0).gameObject;
			if (childControl.GetComponent<UIPCControl> () != null) {
				Unit unitTarget = childControl.GetComponent<UIPCControl> ().unitTarget;
				if ((unitTarget != null) && !unitTarget.isDown) {
					//update the stats
					UnitMgr.InitializeUnitAttributes(unitTarget,controlId);
				}
			}
		}
	}

	//update pc data
	public static void UpdatePCData(int unitListId){
		PCData pcData = instance.data.pcData [unitListId];
		UnitDBItem unitDBItem = instance.unitDB.items [pcData.id];

		//get base level from database
		int lvl = pcData.lvl;
		int baseHealth, health; 
		baseHealth = health = unitDBItem.health.ValueAtLvl (lvl);
		int mp = unitDBItem.mp.ValueAtLvl (lvl);
		int shield = unitDBItem.shield.ValueAtLvl (lvl);
		int barrier = unitDBItem.barrier.ValueAtLvl (lvl);
		float speed = unitDBItem.speed.ValueAtLvl (lvl);
		float cooldown = unitDBItem.cooldown.ValueAtLvl (lvl);
		float cooldownSpecial = unitDBItem.cooldownSpecial.ValueAtLvl (lvl);
		float rangeAtk = unitDBItem.rangeAtk.ValueAtLvl (lvl);
		float rangeHeal = unitDBItem.rangeHeal.ValueAtLvl (lvl);
		int attack = unitDBItem.attack.ValueAtLvl (lvl);
		int magic = unitDBItem.magic.ValueAtLvl (lvl);
		int heal = unitDBItem.heal.ValueAtLvl (lvl);
		int armor = unitDBItem.armor.ValueAtLvl (lvl);
		int mr = unitDBItem.mr.ValueAtLvl (lvl);
		float resistSol = unitDBItem.resistSol.ValueAtLvl (lvl);
		float resistLuna = unitDBItem.resistLuna.ValueAtLvl (lvl);
		float resistStella = unitDBItem.resistStella.ValueAtLvl (lvl);
		float resistVoid = unitDBItem.resistVoid.ValueAtLvl (lvl);
		float resistWind = unitDBItem.resistWind.ValueAtLvl (lvl);
		float resistEarth = unitDBItem.resistEarth.ValueAtLvl (lvl);
		float resistWater = unitDBItem.resistWater.ValueAtLvl (lvl);
		float resistFire = unitDBItem.resistFire.ValueAtLvl (lvl);
		int accuracy = unitDBItem.accuracy.ValueAtLvl (lvl);
		int dodge = unitDBItem.dodge.ValueAtLvl (lvl);
		float regenHealth = unitDBItem.regenHealth.ValueAtLvl (lvl);

		//get boost from equipment
		List<int> equipped = new List<int>();
		equipped.Add (pcData.equipHead);
		equipped.Add (pcData.equipBody);
		equipped.Add (pcData.equipLArm);
		equipped.Add (pcData.equipRArm);
		equipped.Add (pcData.equipAcc1);
		equipped.Add (pcData.equipAcc2);

		for(int j = 0; j < equipped.Count; j++){
			if (equipped [j] != -1) {
				ItemDBItem itemDBItem = instance.itemDB.items [instance.data.items [equipped[j]].id];
				string[] itemEffects = itemDBItem.effect.Split (',');
				for (int i = 0; i < itemEffects.Length; i++) {
					string[] param = itemEffects [i].Split (':');
					switch (param [0]) {
					case "mr":
						{
							mr += int.Parse (param [1]);
						}
						break;
					case "armor":
						{
							armor += int.Parse (param [1]);
						}
						break;
					case "attack":
						{
							attack += int.Parse (param [1]);
						}
						break;
					case "magic":
						{
							magic += int.Parse (param [1]);
						}
						break;
					}
				}
			}
		}

		//get boost from skills
		string [] skillIds = unitDBItem.skills.Split(','); 
		for (int j = 0; j < skillIds.Length; j++) {
			SkillDBItem skillDBItem = instance.skillDB.items [int.Parse (skillIds [j])];
			if (skillDBItem.skilltype == 0) {
				string[] skillEffects = skillDBItem.effect.Split (',');
				for (int i = 0; i < skillEffects.Length; i++) {
					string[] param = skillEffects [i].Split (':');
					switch (param [0]) {
					case "mr":
						{
							mr += int.Parse (param [1]);
						}
						break;
					case "armor":
						{
							armor += int.Parse (param [1]);
						}
						break;
					case "attack":
						{
							attack += int.Parse (param [1]);
						}
						break;
					case "magic":
						{
							magic += int.Parse (param [1]);
						}
						break;
					case "health%+":
						{
							health += (int)Mathf.Floor(pcData.allocatedSkills[j] * baseHealth * float.Parse (param [1])/100.0f);
						}
						break;
					}
				}
			}
		}

		//assign new value
		pcData.health.Initialize(health);
		pcData.mp.Initialize(mp);
		pcData.shield.Initialize(shield);
		pcData.barrier.Initialize(barrier);
		pcData.speed.Initialize(speed);
		pcData.cooldown.Initialize(cooldown);
		pcData.cooldownSpecial.Initialize(cooldownSpecial);
		pcData.rangeAtk.Initialize(rangeAtk);
		pcData.rangeHeal.Initialize(rangeHeal);
		pcData.attack.Initialize(attack);
		pcData.magic.Initialize(magic);
		pcData.heal.Initialize(heal);
		pcData.armor.Initialize(armor);
		pcData.mr.Initialize(mr);
		pcData.resistSol.Initialize(resistSol);
		pcData.resistLuna.Initialize(resistLuna);
		pcData.resistStella.Initialize(resistStella);
		pcData.resistVoid.Initialize(resistVoid);
		pcData.resistWind.Initialize(resistWind);
		pcData.resistEarth.Initialize(resistEarth);
		pcData.resistWater.Initialize(resistWater);
		pcData.resistFire.Initialize(resistFire);
		pcData.accuracy.Initialize(accuracy);
		pcData.dodge.Initialize(dodge);
		pcData.regenHealth.Initialize(regenHealth);
	}

	public static void UpdateSverdData(int sverdListId){
		SverdData pcData = instance.data.sverdData [sverdListId];
		UnitDBItem unitDBItem = instance.unitDB.items [pcData.id];

		//get base level from database
		int lvl = 1;
		int health = unitDBItem.health.ValueAtLvl (lvl);
		int mp = unitDBItem.mp.ValueAtLvl (lvl);
		int shield = unitDBItem.shield.ValueAtLvl (lvl);
		int barrier = unitDBItem.barrier.ValueAtLvl (lvl);
		float speed = unitDBItem.speed.ValueAtLvl (lvl);
		float cooldown = unitDBItem.cooldown.ValueAtLvl (lvl);
		float cooldownSpecial = unitDBItem.cooldownSpecial.ValueAtLvl (lvl);
		float rangeAtk = unitDBItem.rangeAtk.ValueAtLvl (lvl);
		float rangeHeal = unitDBItem.rangeHeal.ValueAtLvl (lvl);
		int attack = unitDBItem.attack.ValueAtLvl (lvl);
		int magic = unitDBItem.magic.ValueAtLvl (lvl);
		int heal = unitDBItem.heal.ValueAtLvl (lvl);
		int armor = unitDBItem.armor.ValueAtLvl (lvl);
		int mr = unitDBItem.mr.ValueAtLvl (lvl);
		float resistSol = unitDBItem.resistSol.ValueAtLvl (lvl);
		float resistLuna = unitDBItem.resistLuna.ValueAtLvl (lvl);
		float resistStella = unitDBItem.resistStella.ValueAtLvl (lvl);
		float resistVoid = unitDBItem.resistVoid.ValueAtLvl (lvl);
		float resistWind = unitDBItem.resistWind.ValueAtLvl (lvl);
		float resistEarth = unitDBItem.resistEarth.ValueAtLvl (lvl);
		float resistWater = unitDBItem.resistWater.ValueAtLvl (lvl);
		float resistFire = unitDBItem.resistFire.ValueAtLvl (lvl);
		int accuracy = unitDBItem.accuracy.ValueAtLvl (lvl);
		int dodge = unitDBItem.dodge.ValueAtLvl (lvl);
		float regenHealth = unitDBItem.regenHealth.ValueAtLvl (lvl);
		float fuel = 180;

		//get boost from equipment
		List<int> equipped = new List<int>();
		equipped.Add (instance.data.sverdData [sverdListId].equipHead);
		equipped.Add (instance.data.sverdData [sverdListId].equipBody);
		equipped.Add (instance.data.sverdData [sverdListId].equipLArm);
		equipped.Add (instance.data.sverdData [sverdListId].equipRArm);
		equipped.Add (instance.data.sverdData [sverdListId].equipAcc1);
		equipped.Add (instance.data.sverdData [sverdListId].equipAcc2);

		Debug.Log ("updating sverd data");

		for(int j = 0; j < equipped.Count; j++){
			if (equipped [j] != -1) {
				ItemDBItem itemDBItem = instance.itemDB.items [instance.data.items [equipped[j]].id];
				string[] itemEffects = itemDBItem.effect.Split (',');
				for (int i = 0; i < itemEffects.Length; i++) {
					Debug.Log (itemEffects [i]);
					string[] param = itemEffects [i].Split (':');
					switch (param [0]) {
					case "mr":
						{
							mr += int.Parse (param [1]);
						}
						break;
					case "armor":
						{
							armor += int.Parse (param [1]);
						}
						break;
					case "attack":
						{
							attack += int.Parse (param [1]);
						}
						break;
					case "magic":
						{
							magic += int.Parse (param [1]);
						}
						break;
					case "fuel":
						{
							Debug.Log ("code reached");
							fuel += float.Parse (param [1]);
						}
						break;
					}
				}
			}
		}

		//assign new value
		pcData.health.Initialize(health);
		pcData.mp.Initialize(mp);
		pcData.shield.Initialize(shield);
		pcData.barrier.Initialize(barrier);
		pcData.speed.Initialize(speed);
		pcData.cooldown.Initialize(cooldown);
		pcData.cooldownSpecial.Initialize(cooldownSpecial);
		pcData.rangeAtk.Initialize(rangeAtk);
		pcData.rangeHeal.Initialize(rangeHeal);
		pcData.attack.Initialize(attack);
		pcData.magic.Initialize(magic);
		pcData.heal.Initialize(heal);
		pcData.armor.Initialize(armor);
		pcData.mr.Initialize(mr);
		pcData.resistSol.Initialize(resistSol);
		pcData.resistLuna.Initialize(resistLuna);
		pcData.resistStella.Initialize(resistStella);
		pcData.resistVoid.Initialize(resistVoid);
		pcData.resistWind.Initialize(resistWind);
		pcData.resistEarth.Initialize(resistEarth);
		pcData.resistWater.Initialize(resistWater);
		pcData.resistFire.Initialize(resistFire);
		pcData.accuracy.Initialize(accuracy);
		pcData.dodge.Initialize(dodge);
		pcData.regenHealth.Initialize(regenHealth);
		pcData.fuel.Initialize (fuel);
	}
}

[System.Serializable]
public class PlayerData{
	public List<PCData> pcData;
	public List<int> party;
	public List<EventSwitch> switches;
	public string sceneName;
	public List<ItemData> items;
	public List<string> unitPos;
	public List<string> unitOri;
	public List<SverdData> sverdData;
	public int gold;

	public PlayerData(){
		pcData = new List<PCData> ();
		sverdData = new List<SverdData> ();
		party = new List<int> ();
		switches = new List<EventSwitch> ();
		items = new List<ItemData> ();
		unitPos = new List<string> ();
		unitOri = new List<string> ();
		for (int i = 0; i < 4; i++) {
			unitPos.Add ((new Vector3 (0.0f, 0.0f, 0.0f)).ToString());
			unitOri.Add ((new Vector3 (1.0f, 1.0f, 1.0f)).ToString());
		}
		gold = 0;
	}
}


[System.Serializable]
public class ItemData{
	public int id;
	public int listId;
	public int amount;
	public int equippedBy;

	public ItemData(int id, int listId, int amount){
		this.id = id;
		this.listId = listId;
		this.amount = amount;
		this.equippedBy = -1;
	}
}

[System.Serializable]
public class UnitData{
	public int id; //id to reference the database
	public int listId; //order in which the unit is added to player data

	//unit attribute
	public AttributeInt health; 
	public AttributeInt mp;
	public AttributeInt shield;
	public AttributeInt barrier;
	public AttributeFloat speed;
	public AttributeFloat cooldown;
	public AttributeFloat cooldownSpecial;
	public AttributeFloat rangeAtk;
	public AttributeFloat rangeHeal;
	public AttributeInt attack;
	public AttributeInt magic;
	public AttributeInt heal;
	public AttributeInt armor;
	public AttributeInt mr;
	//elemental resist
	public AttributeFloat resistSol;
	public AttributeFloat resistLuna;
	public AttributeFloat resistStella;
	public AttributeFloat resistVoid;
	public AttributeFloat resistWind;
	public AttributeFloat resistEarth;
	public AttributeFloat resistWater;
	public AttributeFloat resistFire;
	//dodge and accuracy
	public AttributeInt dodge;
	public AttributeInt accuracy;
	//secondary stats
	public AttributeFloat regenHealth;

	public int equipHead;
	public int equipBody;
	public int equipRArm;
	public int equipLArm;
	public int equipAcc1;
	public int equipAcc2;

	public UnitData(int id, int listId){
		this.id = id;
		this.listId = listId;
		this.health = new AttributeInt (0);
		this.mp = new AttributeInt (0);
		this.shield = new AttributeInt (0);
		this.barrier = new AttributeInt (0);
		this.speed = new AttributeFloat (0.0f);
		this.cooldown = new AttributeFloat (0.0f);
		this.cooldownSpecial = new AttributeFloat (0.0f);
		this.rangeAtk = new AttributeFloat (0.0f);
		this.rangeHeal = new AttributeFloat (0.0f);
		this.attack = new AttributeInt (0);
		this.magic = new AttributeInt (0);
		this.heal = new AttributeInt (0);
		this.armor = new AttributeInt (0);
		this.mr = new AttributeInt (0);
		this.resistSol = new AttributeFloat (0.0f);
		this.resistLuna = new AttributeFloat (0.0f);
		this.resistStella = new AttributeFloat (0.0f);
		this.resistVoid = new AttributeFloat (0.0f);
		this.resistWind = new AttributeFloat (0.0f);
		this.resistEarth = new AttributeFloat (0.0f);
		this.resistWater = new AttributeFloat (0.0f);
		this.resistFire = new AttributeFloat (0.0f);
		this.dodge = new AttributeInt (0);
		this.accuracy = new AttributeInt (0);
		this.regenHealth = new AttributeFloat (0.0f);

		//equipment
		this.equipHead = -1;
		this.equipBody = -1;
		this.equipRArm = -1;
		this.equipLArm = -1;
		this.equipAcc1 = -1;
		this.equipAcc2 = -1;
	}
}

[System.Serializable]
public class PCData:UnitData{
	public int lvl;
	public AttributeInt exp;
	public int sverdListId = -1;
	public int[] allocatedSkills;

	public PCData(int id, int listId, int lvl):base(id, listId){
		this.lvl = lvl;

		//initialize exp
		this.exp = new AttributeInt (0);
		for (int i = 1; i <= lvl; i++) {
			this.exp._current = this.exp._base;
			this.exp._base += Constants.EXPNEEDED.ValueAtLvl (lvl);
		}

		//initialize allocated skills
		allocatedSkills = new int[39];
		for (int i = 0; i < 39; i++)
			this.allocatedSkills[i] = 0;

		//initialize attributes
		UnitDBItem unitDBItem = Persistence.GetUnitDB().items[id];
		this.health = new AttributeInt (unitDBItem.health.ValueAtLvl(lvl));
		this.mp = new AttributeInt (unitDBItem.mp.ValueAtLvl(lvl));
		this.shield = new AttributeInt (unitDBItem.shield.ValueAtLvl(lvl));
		this.barrier = new AttributeInt (unitDBItem.barrier.ValueAtLvl(lvl));
		this.speed = new AttributeFloat (unitDBItem.speed.ValueAtLvl(lvl));
		this.cooldown = new AttributeFloat (unitDBItem.cooldown.ValueAtLvl(lvl));
		this.cooldownSpecial = new AttributeFloat (unitDBItem.cooldownSpecial.ValueAtLvl(lvl));
		this.rangeAtk = new AttributeFloat (unitDBItem.rangeAtk.ValueAtLvl(lvl));
		this.rangeHeal = new AttributeFloat (unitDBItem.rangeHeal.ValueAtLvl(lvl));
		this.attack = new AttributeInt (unitDBItem.attack.ValueAtLvl(lvl));
		this.magic = new AttributeInt (unitDBItem.magic.ValueAtLvl(lvl));
		this.heal = new AttributeInt (unitDBItem.heal.ValueAtLvl(lvl));
		this.armor = new AttributeInt (unitDBItem.armor.ValueAtLvl(lvl));
		this.mr = new AttributeInt (unitDBItem.mr.ValueAtLvl(lvl));
		this.resistSol = new AttributeFloat (unitDBItem.resistSol.ValueAtLvl(lvl));
		this.resistLuna = new AttributeFloat (unitDBItem.resistLuna.ValueAtLvl(lvl));
		this.resistStella = new AttributeFloat (unitDBItem.resistStella.ValueAtLvl(lvl));
		this.resistVoid = new AttributeFloat (unitDBItem.resistVoid.ValueAtLvl(lvl));
		this.resistWind = new AttributeFloat (unitDBItem.resistWind.ValueAtLvl(lvl));
		this.resistEarth = new AttributeFloat (unitDBItem.resistEarth.ValueAtLvl(lvl));
		this.resistWater = new AttributeFloat (unitDBItem.resistWater.ValueAtLvl(lvl));
		this.resistFire = new AttributeFloat (unitDBItem.resistFire.ValueAtLvl(lvl));
		this.dodge = new AttributeInt (unitDBItem.dodge.ValueAtLvl(lvl));
		this.accuracy = new AttributeInt (unitDBItem.accuracy.ValueAtLvl(lvl));
		this.regenHealth = new AttributeFloat (unitDBItem.regenHealth.ValueAtLvl(lvl));
	}
}

[System.Serializable]
public class SverdData:UnitData{
	public int pcListId = -1;
	public AttributeFloat fuel;

	public SverdData(int id, int listId, int lvl = 1):base(id, listId){
		//initialize attributes
		UnitDBItem unitDBItem = Persistence.GetUnitDB().items[id];
		this.health = new AttributeInt (unitDBItem.health.ValueAtLvl(lvl));
		this.mp = new AttributeInt (unitDBItem.mp.ValueAtLvl(lvl));
		this.shield = new AttributeInt (unitDBItem.shield.ValueAtLvl(lvl));
		this.barrier = new AttributeInt (unitDBItem.barrier.ValueAtLvl(lvl));
		this.speed = new AttributeFloat (unitDBItem.speed.ValueAtLvl(lvl));
		this.cooldown = new AttributeFloat (unitDBItem.cooldown.ValueAtLvl(lvl));
		this.cooldownSpecial = new AttributeFloat (unitDBItem.cooldownSpecial.ValueAtLvl(lvl));
		this.rangeAtk = new AttributeFloat (unitDBItem.rangeAtk.ValueAtLvl(lvl));
		this.rangeHeal = new AttributeFloat (unitDBItem.rangeHeal.ValueAtLvl(lvl));
		this.attack = new AttributeInt (unitDBItem.attack.ValueAtLvl(lvl));
		this.magic = new AttributeInt (unitDBItem.magic.ValueAtLvl(lvl));
		this.heal = new AttributeInt (unitDBItem.heal.ValueAtLvl(lvl));
		this.armor = new AttributeInt (unitDBItem.armor.ValueAtLvl(lvl));
		this.mr = new AttributeInt (unitDBItem.mr.ValueAtLvl(lvl));
		this.resistSol = new AttributeFloat (unitDBItem.resistSol.ValueAtLvl(lvl));
		this.resistLuna = new AttributeFloat (unitDBItem.resistLuna.ValueAtLvl(lvl));
		this.resistStella = new AttributeFloat (unitDBItem.resistStella.ValueAtLvl(lvl));
		this.resistVoid = new AttributeFloat (unitDBItem.resistVoid.ValueAtLvl(lvl));
		this.resistWind = new AttributeFloat (unitDBItem.resistWind.ValueAtLvl(lvl));
		this.resistEarth = new AttributeFloat (unitDBItem.resistEarth.ValueAtLvl(lvl));
		this.resistWater = new AttributeFloat (unitDBItem.resistWater.ValueAtLvl(lvl));
		this.resistFire = new AttributeFloat (unitDBItem.resistFire.ValueAtLvl(lvl));
		this.dodge = new AttributeInt (unitDBItem.dodge.ValueAtLvl(lvl));
		this.accuracy = new AttributeInt (unitDBItem.accuracy.ValueAtLvl(lvl));
		this.regenHealth = new AttributeFloat (unitDBItem.regenHealth.ValueAtLvl(lvl));
		this.fuel = new AttributeFloat (180.0f);
	}
}

[System.Serializable]
public class SkillDB{
	public SkillDBItem [] items;
}

[System.Serializable]
public class SkillDBItem{
	public int id;
	public string name;
	public string icon;
	public string description;
	public int maxlvl;
	public int category;
	public int prerequisite;
	public int skilltype;
	public string effect;
	public int target;
	public float range;
	public string animation;
	public int cost;
	public float casttime;
	public string hitFrames;
}

[System.Serializable]
public class ItemDB{
	public ItemDBItem [] items;
}

[System.Serializable]
public class ItemDBItem{
	public int id;
	public string name;
	public string icon;
	public int category;
	public string description;
	public int buyFor;
	public int sellFor;
	public string effect;
}

[System.Serializable]
public class UnitDB{
	public UnitDBItem [] items;
}

[System.Serializable]
public class UnitDBItem{
	public int id;
	public string name;
	public string prefab;
	public string facesetId;
	public float sight;
	public string skills;
	public string attackFrames;
	public string healFrames;
	public DatabaseAttributeInt health;
	public DatabaseAttributeInt mp;
	public DatabaseAttributeInt shield;
	public DatabaseAttributeInt barrier;
	public DatabaseAttributeFloat speed;
	public DatabaseAttributeFloat cooldown;
	public DatabaseAttributeFloat cooldownSpecial;
	public DatabaseAttributeFloat rangeAtk;
	public DatabaseAttributeFloat rangeHeal;
	public DatabaseAttributeInt attack;
	public DatabaseAttributeInt magic;
	public DatabaseAttributeInt heal;
	public DatabaseAttributeInt armor;
	public DatabaseAttributeInt mr;
	public DatabaseAttributeFloat resistSol;
	public DatabaseAttributeFloat resistLuna;
	public DatabaseAttributeFloat resistStella;
	public DatabaseAttributeFloat resistVoid;
	public DatabaseAttributeFloat resistWind;
	public DatabaseAttributeFloat resistEarth;
	public DatabaseAttributeFloat resistWater;
	public DatabaseAttributeFloat resistFire;
	public DatabaseAttributeInt dodge;
	public DatabaseAttributeInt accuracy;
	public DatabaseAttributeFloat regenHealth;
	public DatabaseAttributeInt exp;
}

[System.Serializable]
public class DatabaseAttribute<T>{
	public T min;
	public T max;
	public int modifier;
}

[System.Serializable]
public class DatabaseAttributeInt: DatabaseAttribute<int>{
	public DatabaseAttributeInt(){}

	//alt constructor
	public DatabaseAttributeInt(int min, int max, int mod){
		this.min = min;
		this.max = max;
		this.modifier = mod;
	}

	public int ValueAtLvl(int lvl){
		//find maximum acceleration if v0 = 0
		float maxA = 2.0f * (this.max - this.min) / ((Constants.MAXLVL - 1) * (Constants.MAXLVL - 1));
		//Debug.Log ("max a: " + maxA);
		//apply modifier
		float a = -1.0f * maxA + 2.0f * maxA * (this.modifier + 10.0f) / 20.0f;
		//Debug.Log ("modded a: " + a);
		//find v0
		float v0 = ((this.max - this.min) - (0.5f * a * (Constants.MAXLVL - 1) * (Constants.MAXLVL - 1))) / (Constants.MAXLVL - 1);
		//Debug.Log ("v0: " + v0);
		//return value at level
		return (int)Mathf.Floor(this.min + v0 * (lvl - 1) + 0.5f * a * (lvl - 1) * (lvl - 1));
	}
}

[System.Serializable]
public class DatabaseAttributeFloat: DatabaseAttribute<float>{
	public DatabaseAttributeFloat(){}

	public float ValueAtLvl(int lvl){
		//find maximum acceleration if v0 = 0
		float maxA = 2.0f * (this.max - this.min) / ((Constants.MAXLVL - 1) * (Constants.MAXLVL - 1));
		//Debug.Log ("max a: " + maxA);
		//apply modifier
		float a = -1.0f * maxA + 2.0f * maxA * (this.modifier + 10.0f) / 20.0f;
		//Debug.Log ("modded a: " + a);
		//find v0
		float v0 = ((this.max - this.min) - (0.5f * a * (Constants.MAXLVL - 1) * (Constants.MAXLVL - 1))) / (Constants.MAXLVL - 1);
		//Debug.Log ("v0: " + v0);
		//return value at level
		return this.min + v0 * (lvl - 1) + 0.5f * a * (lvl - 1) * (lvl - 1);
	}
}