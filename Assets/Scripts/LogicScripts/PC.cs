﻿//main controllable unit
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public enum STANCE {VANGUARD, SENTINEL, MEDIC};

public class PC : Unit {
	public STANCE stance = STANCE.VANGUARD; //stance 

	public override void DownEvent(){
		sprite.GetComponent<Animator> ().Play ("Down");

		//check if all pc down then go back to title
		Persistence.CheckAllPCDown();
	}

	// Update is called once per frame
	public override void Update () {
		//call base update
		base.Update ();

		if (isDown) //unit is down, return right away
			return;

		//update fuel
		if (pilotSverdId != -1) {
			fuel.Add (Time.deltaTime);
		}

		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["PCUpdate"], this,Time.deltaTime);
	}
}
