﻿//pops on hit messages on top of instantiated unit
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HitNumber : MonoBehaviour {
	public float duration  = 1f;
	private float lifetime = 0.0f;
	public Text text;
	private RectTransform rectTransform;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		rectTransform = GetComponent<RectTransform> ();
	}

	// Update is called once per frame
	void Update () {
		lifetime += Time.deltaTime;
		rectTransform.localPosition = new Vector3 (rectTransform.localPosition.x,rectTransform.localPosition.y + Time.deltaTime * 100,rectTransform.localPosition.z);
		rectTransform.localScale = new Vector3 (1 - (lifetime / duration), 1 - (lifetime / duration), 1 - (lifetime / duration));
		text.color = new Color (text.color.r,text.color.g,text.color.b,1 - (lifetime / duration) / 2);
		if (lifetime > duration)
			Destroy (this.gameObject);
	}
}