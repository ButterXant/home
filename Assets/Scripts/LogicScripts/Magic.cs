﻿//placeholder for projectile skill
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magic : MonoBehaviour {
	public Unit target;
	public Unit caster;
	public int denom;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			float step = 5.0f * Time.deltaTime;
			this.transform.position = Vector3.MoveTowards (this.transform.position, target.gameObject.transform.position, step);
			if (Vector3.Distance (this.transform.position, target.gameObject.transform.position) <= 0.1f) {
				if (caster != null) {
					UnitMgr.Engage (caster, target, denom, ENGAGEMENTTYPE.ATTACK);
				}
				Destroy (this.gameObject);
			}
		} else {
			Destroy (this.gameObject);
		}
	}
}
