﻿//class that controls the behavior when receiving user inputs

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//flag to specify the intent of a controlled unit
public enum INTENT {ATTACK, HEAL, INTERACT};

public class Control : MonoBehaviour {
	//raycast parameters
	RaycastHit hit;
	private float raycastLength = 500;
	public LayerMask raycastMask;
	public GameObject pointer; //prefab ton indicate where user clicks

	//clicking parameters
	private float clickDownTime; //when user clicks
	private float clickUpTime; //when user releases
	private float clickThreshold = 0.5f; //threshold to determine whether it's a long click or short click
	private Vector3 fingerStartPos; //the starting position when user clicks
	private Vector3 cameraStartPos; //camera pos when user clicks
	private bool moved = false; //flag set to true when user drags

	public static GameObject selectedUnit = null; //the unit that we're currently selecting
	public static bool selectingSkillTarget = false; //flag whether we're in the selecting skill target mode

	public static bool follow = true; //placeholder to indicate whether units need to follow selected unit if it's too far


	//select unit, switch camera mode to catchup, activates the "selected unit" indicator 
	public static void SelectUnit(GameObject selected){
		if ((selectedUnit != null) && (selectedUnit != selected)) {
			if(selectedUnit.GetComponent<Unit>().selectedBase != null)
				selectedUnit.GetComponent<Unit>().selectedBase.SetActive (false);
		}
		selectedUnit = selected;
		CameraMgr.cameraMode = CAMERAMODE.CATCHUP;

		if(selectedUnit.GetComponent<Unit>().selectedBase != null)
			selectedUnit.GetComponent<Unit>().selectedBase.SetActive (true);
	}

	//determines whether we're clicking on a UI element or not
	private bool IsPointerOverUIObject(){
		PointerEventData eventDataCurrentPosition = new PointerEventData (EventSystem.current);
		eventDataCurrentPosition.position = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult> ();
		EventSystem.current.RaycastAll (eventDataCurrentPosition, results);
		return results.Count > 0;
	}

	// Update is called once per frame
	void Update () {
		//beginning of touch or click
		#if UNITY_IOS
		if(IsPointerOverUIObject()) return;
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		#else
		if (EventSystem.current.IsPointerOverGameObject ()) return;
		if (Input.GetMouseButtonDown (0))
		#endif
		{
			moved = false; //start of click, moved is false

			//store camera and finger start position
			cameraStartPos = Camera.main.transform.position;
			fingerStartPos = Camera.main.ScreenToViewportPoint (Input.mousePosition);
				
			clickDownTime = Time.unscaledTime;
		}

		//check if we move
		#if UNITY_IOS
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		#else
		if (Input.GetMouseButton (0) /*&& ((Time.unscaledTime - clickDownTime) > clickThreshold)*/)
		#endif
		{
			//get finger current position
			Vector3 fingerCurrentPos = Camera.main.ScreenToViewportPoint (Input.mousePosition);

			//if we move, set a flag no need to check for long click or clicking
			if (Vector3.Distance (fingerCurrentPos, fingerStartPos) > 0.01) {
				CameraMgr.cameraMode = CAMERAMODE.FREE;
				Camera.main.transform.position = new Vector3 (cameraStartPos[0] - 2 * (fingerCurrentPos[0] - fingerStartPos[0]) * Camera.main.orthographicSize * Screen.width / Screen.height,cameraStartPos[1] - 2 * (fingerCurrentPos[1] - fingerStartPos[1]) * Camera.main.orthographicSize,-10);
				moved = true;
			} else 
				moved = false;
		}
		if (moved) return;

		//check if we long press
		#if UNITY_IOS
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary)
		#else
		if (Input.GetMouseButton (0))
		#endif
		{
			clickUpTime = Time.unscaledTime;
			if ((clickUpTime - clickDownTime > clickThreshold) && (!selectingSkillTarget)) {
				if (selectedUnit) {
					#if UNITY_IOS
					Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
					#else
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					#endif

					if (Physics.Raycast (ray, out hit, raycastLength,raycastMask)) {
						if (hit.collider.GetComponent<Unit> ()) { //long click on a unit

							if (hit.collider.GetComponent<PC> () || hit.collider.GetComponent<Sverd> ()) { //unit is a PC or a controllable sverd
								if (selectedUnit != hit.collider.gameObject) { //a different unit, switch
									SelectUnit (hit.collider.gameObject);
									//selectedUnit = hit.collider.gameObject;
									//CameraMgr.cameraMode = CAMERAMODE.CATCHUP;
								} else { //the same game object
									//placeholder for self targetting probably
								}
							}
						} else { //long click on a plane, deselect the selected unit
							//selectedUnit = null;
						}
					}
				}
				return;
			}
		}

		//check when we release the click
		#if UNITY_IOS
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
		#else
		if(Input.GetMouseButtonUp(0))
		#endif
		{
			clickUpTime = Time.unscaledTime;
			if(clickUpTime - clickDownTime <= clickThreshold){
				#if UNITY_IOS
				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
				#else
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				#endif

				if (Physics.Raycast (ray, out hit, raycastLength,raycastMask)) {
					if (!selectingSkillTarget) { //only go here if we're not in selecting skill target
						if (hit.collider.GetComponent<Unit> ()) { //hitting a unit

							if (hit.collider.GetComponent<PC> () || hit.collider.GetComponent<Sverd> ()) { //unit is a PC or controllable sverd
								if (!selectedUnit) { //no selected unit yet
									SelectUnit (hit.collider.gameObject);
								} else {
									if (selectedUnit == hit.collider.gameObject) { //selected unit is the same as hit unit
										selectedUnit = null;
									} else { //selected unit is other unit
										CameraMgr.cameraMode = CAMERAMODE.CATCHUP;
										MoveSelectedToTargetWithIntent (hit.collider.gameObject, DetermineIntent (selectedUnit, hit.collider.gameObject));
										//selectedUnit.GetComponent<Unit> ().GoTo (hit.collider.gameObject,DetermineIntent(selectedUnit,hit.collider.gameObject));
									}

									//for now if clicking other selected units, switch to that one
									//SelectUnit (hit.collider.gameObject);
								}
							} else { //other type of unit
								if (selectedUnit) { //there's a selected unit
									CameraMgr.cameraMode = CAMERAMODE.CATCHUP;
									MoveSelectedToTargetWithIntent (hit.collider.gameObject, DetermineIntent (selectedUnit, hit.collider.gameObject));
									//selectedUnit.GetComponent<Unit> ().GoTo (hit.collider.gameObject,DetermineIntent(selectedUnit,hit.collider.gameObject));
								}
							}

							//update ui to display hit unit

						} else if (hit.collider.name == "Plane") { //hitting a terrain
							//instantiate pointer
							Instantiate (pointer, hit.point, Quaternion.identity);
				
							if (selectedUnit) { //theres a selected unit, make it go there
								CameraMgr.cameraMode = CAMERAMODE.CATCHUP;
								MoveSelectedToPos (hit.point);
							}

							//update ui to empty displayed hit unit
						} 
					} else { //we're in the selecting skill target mode
						if (!selectedUnit) { //error, there should be a selected unit when selecting skill target
						} else {
							//get the skill first
							SkillDBItem skillDBItem = Persistence.GetSkillDB ().items [UIMain.selectedSkillId];

							//check if the skill target type matches the target
							if (hit.collider.GetComponent<Unit> ()) { //hitting a unit

								if ((hit.collider.GetComponent<PC> () && (hit.collider.gameObject == selectedUnit) && (skillDBItem.target == 1))
								    || (hit.collider.GetComponent<PC> () && (hit.collider.gameObject != selectedUnit) && (skillDBItem.target == 2))
								    || (hit.collider.GetComponent<Sverd> () && (hit.collider.gameObject == selectedUnit) && (skillDBItem.target == 3))
								    || (hit.collider.GetComponent<Sverd> () && (hit.collider.gameObject != selectedUnit) && (skillDBItem.target == 4))
								    || (hit.collider.GetComponent<Enemy> () && (skillDBItem.target == 5))
								    //|| (hit.collider.GetComponent<NPC> () && (skillDBItem.target == 6))
								    || (!hit.collider.GetComponent<Enemy> () && (skillDBItem.target == 7))) { //correct target
									//selectedUnit.GetComponent<Unit> ().UseSkill (UIMain.selectedSkillId, hit.collider.GetComponent<Unit> ());
									AssignSelectedSkillToUse (UIMain.selectedSkillId, hit.collider.GetComponent<Unit>());
									UIMain.instance.GetComponent<UISkillList> ().cancelBtn.SetActive (false);
									UIMain.selectedSkillId = -1;
									Time.timeScale = 1;
									selectingSkillTarget = false;
								}else if(skillDBItem.target == 0){ //skill target is 0 but hitting a unit
									//selectedUnit.GetComponent<Unit> ().UseSkill (UIMain.selectedSkillId, hit.collider.transform.position);
									AssignSelectedSkillToUse (UIMain.selectedSkillId, hit.collider.transform.position);
									UIMain.instance.GetComponent<UISkillList> ().cancelBtn.SetActive (false);
									UIMain.selectedSkillId = -1;
									Time.timeScale = 1;
									selectingSkillTarget = false;
								}
							} else if ((hit.collider.name == "Plane") && (skillDBItem.target == 0)) { //hitting a terrain and skill target is 0
								//instantiate pointer
								Instantiate (pointer, hit.point, Quaternion.identity);
								//selectedUnit.GetComponent<Unit> ().UseSkill (UIMain.selectedSkillId, hit.point);
								AssignSelectedSkillToUse (UIMain.selectedSkillId, hit.point);
								UIMain.instance.GetComponent<UISkillList> ().cancelBtn.SetActive (false);
								UIMain.selectedSkillId = -1;
								Time.timeScale = 1;
								selectingSkillTarget = false;
							} else {
								Time.timeScale = 1;
								selectingSkillTarget = false;
							}
						}
					}
				}
			}
		}
	}

	//tell selected unit to use skills
	public void AssignSelectedSkillToUse(int skillId, Vector3 skillTarget){
		if (selectedUnit == null)
			return;
		Unit selected = selectedUnit.GetComponent<Unit> ();
		if (selected.isDown)
			return;
		selected.ResetUnitAction ();
		selected.skillIdToUse = skillId;
		selected.target = null;
		selected.skillTarget = skillTarget;
		//go to Lua, determine what to do there 
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["UsePCSkill"], selected);
	}

	//tell selected unit to use skills
	public void AssignSelectedSkillToUse(int skillId, Unit skillTarget){
		if (selectedUnit == null)
			return;
		Unit selected = selectedUnit.GetComponent<Unit> ();
		if (selected.isDown)
			return;
		selected.ResetUnitAction ();
		selected.skillIdToUse = skillId;
		selected.target = skillTarget.gameObject;
		//go to Lua, determine what to do there
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["UsePCSkill"], selected);
	}

	//after selecting a unit, if clicking other unit, move the selected unit to that unit
	public void MoveSelectedToTargetWithIntent(GameObject targetUnit, INTENT intent){
		if (selectedUnit == null)
			return;
		Unit selected = selectedUnit.GetComponent<Unit> ();
		if (selected.isDown)
			return;
		selected.ResetUnitAction ();
		selected.target = targetUnit;
		//go to lua, determine what to do there
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["MovePCWithIntent"], selected,(int) intent);
	}

	//after selecting a unit, if clicking a position on a plane, move selected unit to that position
	public void MoveSelectedToPos(Vector3 destination){
		if (selectedUnit == null)
			return;
		Unit selected = selectedUnit.GetComponent<Unit> ();
		if (selected.isDown)
			return;
		selected.ResetUnitAction ();
		selectedUnit.GetComponent<Unit>().ResetUnitAction ();
		selected.target = null;
		//go to lua, determine what to do there
		Persistence.instance.behaviorLua.Call (Persistence.instance.behaviorLua.Globals ["MovePC"], selectedUnit.GetComponent<Unit>());
		selectedUnit.GetComponent<Unit>().SetPath (hit.point);
	}

	//determine the intent of moving a target to another target
	public INTENT DetermineIntent(GameObject selectedUnit, GameObject target){
		if (target.GetComponent<Enemy> ()) { //target is an enemy
			return INTENT.ATTACK;
		} else {
			if (selectedUnit.GetComponent<Sverd> ()) { //selected unit is sverd
				return INTENT.INTERACT;
			} else { //selected unit is pc
				if (target.GetComponent<Sverd> ()) { //target is an ally sverd, can't be healed
					return INTENT.INTERACT;
				} else { //target is either an ally pc, or npc (for future guest characters)
					if (selectedUnit.GetComponent<PC>().stance != STANCE.MEDIC) { //not in medic stance, can't heal
						return INTENT.INTERACT;
					} else { //in medic stance, check health percentage of target
						if (target.GetComponent<Unit> ().health.ToPercent () <= Constants.HEALTHRESHOLD) { //health below threshold, need to be healed
							return INTENT.HEAL;
						} else {
							return INTENT.INTERACT;
						}
					}	
				}
			}	
		}
	}
}
