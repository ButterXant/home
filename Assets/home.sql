-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: myfirstdb
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT 'Potion',
  `icon` varchar(45) NOT NULL DEFAULT 'defaultitem',
  `category` int(11) NOT NULL DEFAULT '0',
  `description` varchar(45) NOT NULL DEFAULT 'Heal 100 HP',
  `buyFor` int(11) NOT NULL DEFAULT '1000',
  `sellFor` int(11) NOT NULL DEFAULT '10',
  `effect` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'Potion','potion',0,'Heal 100 HP',1000,10,'health:100'),(2,'Comm Device','comm',1,'A basic communication device',0,0,'mr:10'),(3,'T-Shirt','tshirt',2,'A plain T-Shirt',0,0,'armor:10'),(4,'Dagger','dagger',3,'A simple dagger',0,0,'attack:10'),(5,'Shield','shield',4,'A round shield',0,0,'armor:10'),(6,'Iron Bangle','bangle',5,'A bangle made of iron',0,0,'magic:10'),(7,'Sapphire Pendant','pendant',5,'A beautiful sapphire pendant',0,0,NULL),(8,'GTX1080Ti','GTX1080Ti',6,'A good starter engine for sverds',0,0,'fuel:180');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL DEFAULT 'default',
  `icon` varchar(45) NOT NULL DEFAULT 'default',
  `description` varchar(100) NOT NULL DEFAULT 'default skill',
  `maxlvl` int(11) NOT NULL DEFAULT '5',
  `category` int(11) NOT NULL DEFAULT '0',
  `prerequisite` int(11) NOT NULL DEFAULT '0',
  `skilltype` int(11) NOT NULL DEFAULT '0',
  `effect` varchar(100) NOT NULL DEFAULT 'health%+:10',
  `target` int(11) NOT NULL DEFAULT '0',
  `range` float NOT NULL DEFAULT '1',
  `animation` varchar(45) NOT NULL DEFAULT 'defaultAnimation',
  `cost` int(11) NOT NULL DEFAULT '10',
  `casttime` float NOT NULL DEFAULT '2.5',
  `hitFrames` varchar(45) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,'testSkill1','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(2,'testSkill2','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(3,'testSkill3','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(4,'testSkill4','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(5,'testSkill5','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(6,'testSkill6','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(7,'testSkill7','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(8,'testSkill8','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(9,'testSkill9','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(10,'testSkill10','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(11,'testSkill11','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(12,'testSkill12','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(13,'testSkill13','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(14,'Ground Smash','groundsmash','Smash the ground and damage surrounding enemies.',5,0,0,1,'aoe:5$5$2',0,1,'Ability00',10,0,'0.5_1'),(15,'Fire Magic','fireball','Unlock fire magic.',5,0,0,0,'unlock:39',0,1,'defaultAnimation',10,2.5,'1'),(16,'testSkill16','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(17,'testSkill17','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(18,'testSkill18','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(19,'testSkill19','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(20,'testSkill20','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(21,'testSkill21','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(22,'testSkill22','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(23,'testSkill23','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(24,'testSkill24','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(25,'testSkill25','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(26,'testSkill26','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(27,'testSkill27','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(28,'testSkill28','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(29,'testSkill29','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(30,'testSkill30','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(31,'testSkill31','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(32,'testSkill32','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(33,'testSkill33','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(34,'testSkill34','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(35,'testSkill35','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(36,'testSkill36','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(37,'testSkill37','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(38,'testSkill38','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(39,'testSkill39','default','default skill',5,0,0,0,'health%+:10',0,1,'defaultAnimation',10,2.5,'1'),(40,'Fire','fireball','Summon fireball directed at the target.',5,0,0,1,'magic:fire$2',5,5,'Magic',10,2.5,'attack');
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT 'default',
  `prefab` varchar(45) NOT NULL DEFAULT 'default',
  `facesetId` varchar(45) NOT NULL DEFAULT '',
  `sight` float NOT NULL DEFAULT '1',
  `skills` varchar(120) NOT NULL DEFAULT '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38',
  `attackFrames` varchar(45) NOT NULL DEFAULT '1',
  `healFrames` varchar(45) NOT NULL DEFAULT '1',
  `health_min` int(11) NOT NULL DEFAULT '1000',
  `health_max` int(11) NOT NULL DEFAULT '9999',
  `health_mod` int(11) NOT NULL DEFAULT '0',
  `mp_min` int(11) NOT NULL DEFAULT '100',
  `mp_max` int(11) NOT NULL DEFAULT '999',
  `mp_mod` int(11) NOT NULL DEFAULT '0',
  `shield_min` int(11) NOT NULL DEFAULT '0',
  `shield_max` int(11) NOT NULL DEFAULT '0',
  `shield_mod` int(11) NOT NULL DEFAULT '0',
  `barrier_min` int(11) NOT NULL DEFAULT '0',
  `barrier_max` int(11) NOT NULL DEFAULT '0',
  `barrier_mod` int(11) NOT NULL DEFAULT '0',
  `speed_min` float NOT NULL DEFAULT '5',
  `speed_max` float NOT NULL DEFAULT '5',
  `speed_mod` int(11) NOT NULL DEFAULT '0',
  `cooldown_min` float NOT NULL DEFAULT '5',
  `cooldown_max` float NOT NULL DEFAULT '5',
  `cooldown_mod` int(11) NOT NULL DEFAULT '0',
  `cooldownSpecial_min` float NOT NULL DEFAULT '10',
  `cooldownSpecial_max` float NOT NULL DEFAULT '10',
  `cooldownSpecial_mod` int(11) NOT NULL DEFAULT '0',
  `rangeAtk_min` float NOT NULL DEFAULT '1',
  `rangeAtk_max` float NOT NULL DEFAULT '1',
  `rangeAtk_mod` int(11) NOT NULL DEFAULT '0',
  `rangeHeal_min` float NOT NULL DEFAULT '1',
  `rangeHeal_max` float NOT NULL DEFAULT '1',
  `rangeHeal_mod` int(11) NOT NULL DEFAULT '0',
  `attack_min` int(11) NOT NULL DEFAULT '100',
  `attack_max` int(11) NOT NULL DEFAULT '1000',
  `attack_mod` int(11) NOT NULL DEFAULT '0',
  `magic_min` int(11) NOT NULL DEFAULT '100',
  `magic_max` int(11) NOT NULL DEFAULT '1000',
  `magic_mod` int(11) NOT NULL DEFAULT '0',
  `heal_min` int(11) NOT NULL DEFAULT '100',
  `heal_max` int(11) NOT NULL DEFAULT '1000',
  `heal_mod` int(11) NOT NULL DEFAULT '0',
  `armor_min` int(11) NOT NULL DEFAULT '10',
  `armor_max` int(11) NOT NULL DEFAULT '1000',
  `armor_mod` int(11) NOT NULL DEFAULT '0',
  `mr_min` int(11) NOT NULL DEFAULT '10',
  `mr_max` int(11) NOT NULL DEFAULT '1000',
  `mr_mod` int(11) NOT NULL DEFAULT '0',
  `resistSol_min` float NOT NULL DEFAULT '0',
  `resistSol_max` float NOT NULL DEFAULT '0',
  `resistSol_mod` int(11) NOT NULL DEFAULT '0',
  `resistLuna_min` float NOT NULL DEFAULT '0',
  `resistLuna_max` float NOT NULL DEFAULT '0',
  `resistLuna_mod` int(11) NOT NULL DEFAULT '0',
  `resistStella_min` float NOT NULL DEFAULT '0',
  `resistStella_max` float NOT NULL DEFAULT '0',
  `resistStella_mod` int(11) NOT NULL DEFAULT '0',
  `resistVoid_min` float NOT NULL DEFAULT '0',
  `resistVoid_max` float NOT NULL DEFAULT '0',
  `resistVoid_mod` int(11) NOT NULL DEFAULT '0',
  `resistWind_min` float NOT NULL DEFAULT '0',
  `resistWind_max` float NOT NULL DEFAULT '0',
  `resistWind_mod` int(11) NOT NULL DEFAULT '0',
  `resistEarth_min` float NOT NULL DEFAULT '0',
  `resistEarth_max` float NOT NULL DEFAULT '0',
  `resistEarth_mod` int(11) NOT NULL DEFAULT '0',
  `resistWater_min` float NOT NULL DEFAULT '0',
  `resistWater_max` float NOT NULL DEFAULT '0',
  `resistWater_mod` int(11) NOT NULL DEFAULT '0',
  `resistFire_min` float NOT NULL DEFAULT '0',
  `resistFire_max` float NOT NULL DEFAULT '0',
  `resistFire_mod` int(11) NOT NULL DEFAULT '0',
  `dodge_min` int(11) NOT NULL DEFAULT '100',
  `dodge_max` int(11) NOT NULL DEFAULT '100',
  `dodge_mod` int(11) NOT NULL DEFAULT '0',
  `accuracy_min` int(11) NOT NULL DEFAULT '100',
  `accuracy_max` int(11) NOT NULL DEFAULT '100',
  `accuracy_mod` int(11) NOT NULL DEFAULT '0',
  `regenHealth_min` float NOT NULL DEFAULT '0',
  `regenHealth_max` float NOT NULL DEFAULT '0',
  `regenHealth_mod` int(11) NOT NULL DEFAULT '0',
  `exp_min` int(11) NOT NULL DEFAULT '20',
  `exp_max` int(11) NOT NULL DEFAULT '2000',
  `exp_mod` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (1,'Alex','Alex','Faceset_00_00',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','0.5_1.16','0.5_1.16',1000,9999,0,100,999,0,0,0,0,0,0,0,5,5,0,1.5,1.5,0,10,10,0,1,1,0,1,1,0,100,1000,0,100,1000,0,100,1000,0,10,1000,0,10,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,100,0,100,100,0,0,0,0,20,2000,0),(2,'Veronica','Veronica','Faceset_00_01',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','4.83_6','4.83_6',750,9999,0,100,999,0,0,0,0,0,0,0,5,5,0,1.5,1.5,0,10,10,0,3,3,0,3,3,0,100,1000,0,100,1000,0,300,1000,0,10,1000,0,10,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,100,0,100,100,0,0,0,0,20,2000,0),(3,'Alice','default','',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','1','1',1000,9999,0,100,999,0,0,0,0,0,0,0,5,5,0,1.5,1.5,0,10,10,0,1,1,0,3,3,0,100,1000,0,100,1000,0,100,1000,0,10,1000,0,10,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,100,0,100,100,0,0,0,0,20,2000,0),(4,'Ahriman','Ahriman','',100,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','0.33_1','1',200,5000,0,100,999,0,0,0,0,0,0,0,2,2,0,1.5,1.5,0,10,10,0,1,1,0,1,1,0,50,1000,0,100,1000,0,100,1000,0,10,1000,0,10,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,100,0,100,100,0,0,0,0,0,0,0),(5,'Sverd_00','Sverd_00','Faceset_00_02',1,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','1_1.5','1',9999,99999,0,100,999,0,0,0,0,0,0,0,5,5,0,1.5,1.5,0,10,10,0,1,1,0,1,1,0,1000,10000,0,100,1000,0,100,1000,0,100,1000,0,100,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,10,0,10,10,0,0,0,0,0,0,0),(6,'White Whale','WhiteWhale','',10,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','0.33_1','1',9999,99999,0,100,999,0,0,0,0,0,0,0,1,1,0,1.5,1.5,0,10,10,0,1,1,0,1,1,0,500,5000,0,100,1000,0,100,1000,0,10,1000,0,10,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,10,0,10,10,0,0,0,0,0,0,0),(7,'Ahriman','Ahriman','',10,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38','0.33_1','1',200,5000,0,100,999,0,0,0,0,0,0,0,2,2,0,1.5,1.5,0,10,10,0,1,1,0,1,1,0,50,1000,0,100,1000,0,100,1000,0,10,1000,0,10,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,100,0,100,100,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-16 21:44:42
