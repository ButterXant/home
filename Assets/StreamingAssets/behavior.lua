IDLE = 0
LOCKON = 1
CHASEORATTACK = 2 
CHASEORSPECIAL = 3 
CHASEORMAGIC = 4
CHASING = 5
ATTACKING = 6 
SPECIAL = 7 
MAGIC = 8 
CHASEORINTERACT = 9 
CHASEORHEAL = 10 
INTERACTING = 11 
HEALING = 12 
CHASEORACT = 13 
CASTING = 14
CHECKTOUSESKILL = 15
ANIMATING = 16
FOCUSATTACK = 17
FOCUSHEAL = 18

VANGUARD = 0
SENTINEL = 1
MEDIC = 2

ATTACK = 0
HEAL = 1
INTERACT = 2

function MovePCWithIntent(pc,intent)
	if(intent == ATTACK) then
		pc.PushState(CHASEORATTACK)
	elseif(intent == HEAL) then
		pc.PushState(CHASEORHEAL)
	else
		pc.PushState(CHASEORINTERACT)
	end
end

function MovePC(pc)
	pc.PushState(CHASING)
end

function UsePCSkill(pc)
	pc.PushState(CHASEORMAGIC)
end

function PCUpdate(pc, dt)
	-- update cooldown
	if((pc.GetUnitState() != ATTACKING) and (pc.GetUnitState() != HEALING)) then
		pc.GetCooldown().Add(dt)
	end
	
	-- make pc face the target if possible
	pc.FaceTarget()

	if(pc.GetUnitState() == IDLE) then
		-- we're on idle state
		-- follow leader if too far
		leader = pc.GetLeader()
		if(leader != nil) then
			pc.SetTarget(leader)
			pc.PushState(CHASEORINTERACT)
			return
		end
		
		if(pc.GetUnitStance() == MEDIC) then
			-- we're on medic stance, find a unit to heal
			targetUnitArgs = {"Type:PC","Comparison:LowerHealth","Range:"..pc.GetRangeHeal()._current}
			targetUnit = pc.FindTarget(targetUnitArgs)
			if(targetUnit != nil) then
				if(pc.GetCooldown()._current >= pc.GetCooldown().AdjustedValue()) then
					pc.SetTarget(targetUnit)
					pc.GetCooldown()._current = 0
					pc.ResetHitFrames("heal")
					pc.PushState(HEALING)
					pc.FaceTarget()
					pc.PlayAnimation("Heal")
				end
				return
			end 
		end
		
		-- we're not in medic stance or there's noone to heal, find closest enemy in range
		targetUnitArgs = {"Type:Enemy","Comparison:CloserDistance","Range:"..pc.GetRangeAtk()._current}
		targetUnit = pc.FindTarget(targetUnitArgs)
		if(targetUnit != nil) then
			pc.SetTarget(targetUnit)
			pc.PushState(FOCUSATTACK)
		end
	elseif (pc.GetUnitState() == FOCUSATTACK) then
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range
				pc.ClearTarget()
				pc.PopState()
			else
				if(pc.GetCooldown()._current >= pc.GetCooldown().AdjustedValue()) then
					-- cooldown is off, can attack
					pc.GetCooldown()._current = 0
					pc.ResetHitFrames("attack")
					pc.PushState(ATTACKING)
					pc.FaceTarget()
					pc.PlayAnimation("Action00")
				end
			end
		end
	elseif (pc.GetUnitState() == FOCUSHEAL) then
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeHeal()._current)) then
				-- target is out of range
				pc.ClearTarget()
				pc.PopState()
			else
				if(pc.GetCooldown()._current >= pc.GetCooldown().AdjustedValue()) then
					-- cooldown is off, can heal
					pc.GetCooldown()._current = 0
					pc.ResetHitFrames("heal")
					pc.PushState(HEALING)
					pc.FaceTarget()
					pc.PlayAnimation("Heal")
				end
			end
		end
	elseif (pc.GetUnitState() == CHASEORATTACK) then
		-- should we chase or can we attack		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(ATTACK)
			else
				pc.PushState(FOCUSATTACK)
			end
		end
	elseif (pc.GetUnitState() == CHASEORHEAL) then
		-- should we chase or can we heal		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeHeal()._current)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(HEAL)
			else
				pc.PushState(FOCUSHEAL)
			end
		end
	elseif (pc.GetUnitState() == CHASEORINTERACT) then
		-- should we chase or can we heal		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(1.0)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(INTERACT)
			else
				pc.PopState()
				currentTarget = pc.GetTarget()
				if(currentTarget != nil) then
					if(not currentTarget.IsLocked()) then
						pc.PushState(INTERACTING)
						pc.Interact()
					end
				end
			end
		end
	elseif (pc.GetUnitState() == CHASEORMAGIC) then
		if(not pc.IsSkillTargetOutOfRange()) then
			-- target ground position is in range
			pc.SetupCasting()
			pc.PopState()
			pc.PushState(CASTING)
			pc.PlayAnimation("Casting")
		else
			pc.PushState(CHASING)
			pc.SetSkillPath()
		end
	elseif (pc.GetUnitState() == CHASING) then
		-- if we're not moving anymore, means we've reached the target
		if(not pc.IsMoving()) then
			pc.PopState()
		else
			-- still chasing the same target, consider repath if possible
			repathRate = pc.GetRepathRate()
			if(repathRate._current >= repathRate._base) then
				repathRate._current = 0
				pc.Repath()
			end
		end
	elseif (pc.GetUnitState() == ATTACKING) then
		-- doing attack animation, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		else
			if(not pc.Attack()) then
				pc.PopState()
			end
		end
	elseif (pc.GetUnitState() == HEALING) then
		-- doing healing animation, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		else
			if(not pc.Heal()) then
				pc.PopState()
			end
		end
	elseif (pc.GetUnitState() == MAGIC) then
		-- doing magic animation
		if(not pc.Skill()) then
			pc.PopState()
		end
	elseif (pc.GetUnitState() == INTERACTING) then
		-- doing  interaction, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		end
	elseif (pc.GetUnitState() == CASTING) then
		-- casting magic
		if(pc.GetCastTime() != nil) then
			pc.GetCastTime().Add(dt)
			if(pc.GetCastTime()._current >= pc.GetCastTime()._base) then
				pc.PopState()
				pc.PushState(MAGIC)
				pc.UseSkill()
			end
		end
	end
end

function SverdUpdate(pc, dt)
	-- update cooldown
	if((pc.GetUnitState() != ATTACKING) and (pc.GetUnitState() != HEALING)) then
		pc.GetCooldown().Add(dt)
	end
	
	-- make pc face the target if possible
	pc.FaceTarget()

	if(pc.GetUnitState() == IDLE) then
		-- we're on idle state
		-- follow leader if too far
		leader = pc.GetLeader()
		if(leader != nil) then
			pc.SetTarget(leader)
			pc.PushState(CHASEORINTERACT)
			return
		end
		
		-- find closest enemy in range
		targetUnitArgs = {"Type:Enemy","Comparison:CloserDistance","Range:"..pc.GetRangeAtk()._current}
		targetUnit = pc.FindTarget(targetUnitArgs)
		if(targetUnit != nil) then
			pc.SetTarget(targetUnit)
			pc.PushState(FOCUSATTACK)
		end
	elseif (pc.GetUnitState() == FOCUSATTACK) then		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range
				pc.ClearTarget()
				pc.PopState()
			else
				if(pc.GetCooldown()._current >= pc.GetCooldown().AdjustedValue()) then
					-- cooldown is off, can attack
					pc.GetCooldown()._current = 0
					pc.ResetHitFrames("attack")
					pc.PushState(ATTACKING)
					pc.FaceTarget()
					pc.PlayAnimation("Action00")
				end
			end
		end
	elseif (pc.GetUnitState() == CHASEORATTACK) then
		-- should we chase or can we attack		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(ATTACK)
			else
				pc.PushState(FOCUSATTACK)
			end
		end
	elseif (pc.GetUnitState() == CHASEORINTERACT) then
		-- should we chase or can we heal		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(1.0)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(INTERACT)
			else
				pc.PushState(INTERACTING)
				pc.Interact()
			end
		end
	elseif (pc.GetUnitState() == CHASEORMAGIC) then
		if(not pc.IsSkillTargetOutOfRange()) then
			-- target ground position is in range
			pc.SetupCasting()
			pc.PopState()
			pc.PushState(CASTING)
			pc.PlayAnimation("Casting")
		else
			pc.PushState(CHASING)
			pc.SetSkillPath()
		end
	elseif (pc.GetUnitState() == CHASING) then
		-- chasing a target, if we're not moving anymore, means we've reached the target
		if(not pc.IsMoving()) then
			pc.PopState()
		else
			-- still chasing the same target, consider repath if possible
			repathRate = pc.GetRepathRate()
			if(repathRate._current >= repathRate._base) then
				repathRate._current = 0
				pc.Repath()
			end
		end
	elseif (pc.GetUnitState() == ATTACKING) then
		-- doing attack animation, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		else
			if(not pc.Attack()) then
				pc.PopState()
			end
		end
	elseif (pc.GetUnitState() == INTERACTING) then
		-- doing  interaction, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		end
	elseif (pc.GetUnitState() == CASTING) then
		-- casting magic
		if(pc.GetCastTime() != nil) then
			pc.GetCastTime().Add(dt)
			if(pc.GetCastTime()._current >= pc.GetCastTime()._base) then
				pc.PopState()
				pc.PushState(MAGIC)
				pc.UseSkill()
			end
		end
	end
end

function EnemyUpdate(pc, dt)
	-- update cooldown
	if((pc.GetUnitState() != ATTACKING) and (pc.GetUnitState() != HEALING)) then
		pc.GetCooldown().Add(dt)
	end
	
	-- make pc face the target if possible
	pc.FaceTarget()

	if(pc.GetUnitState() == IDLE) then		
		-- get highest enmity
		targetUnit = pc.GetHighestEnmity()
		
		if(targetUnit == nil) then
			-- there's no enmity yet, find closest enemy in range
			targetUnitArgs = {"Type:PC","Type:Sverd","Comparison:CloserDistance","Range:"..pc.GetSight()}
			targetUnit = pc.FindTarget(targetUnitArgs)
		end
		
		if(targetUnit != nil) then
			pc.SetTarget(targetUnit)
			pc.PushState(CHASEORATTACK)
		end
	elseif (pc.GetUnitState() == FOCUSATTACK) then		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range
				pc.ClearTarget()
				pc.PopState()
			else
				if(pc.GetCooldown()._current >= pc.GetCooldown().AdjustedValue()) then
					-- cooldown is off, can attack
					pc.GetCooldown()._current = 0
					pc.ResetHitFrames("attack")
					pc.PushState(ATTACKING)
					pc.FaceTarget()
					pc.PlayAnimation("Action00")
				else
					-- although we're focusing on a unit to attack, check for new highest enmity
					targetUnit = pc.GetHighestEnmity()			
					if((targetUnit != nil) and (targetUnit != pc.GetTarget())) then
						-- there's a higher enmity target
						pc.SetTarget(targetUnit)
						pc.PopState()
					end
				end
			end
		end
	elseif (pc.GetUnitState() == CHASEORATTACK) then
		-- should we chase or can we attack		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(ATTACK)
			else
				pc.PushState(FOCUSATTACK)
			end
		end
	elseif (pc.GetUnitState() == CHASING) then
		-- chasing a target, if we're not moving anymore, means we've reached the target
		if(not pc.IsMoving()) then
			pc.PopState()
		else
			-- while moving, it's possible to change target if there's a higher enmity
			targetUnit = pc.GetHighestEnmity()			
			if((targetUnit != nil) and (targetUnit != pc.GetTarget())) then
				-- there's a higher enmity target
				pc.SetTarget(targetUnit)
				pc.PopState()
			else
				-- still chasing the same target, consider repath if possible
				repathRate = pc.GetRepathRate()
				if(repathRate._current >= repathRate._base) then
					repathRate._current = 0
					pc.Repath()
				end
			end
		end
	elseif (pc.GetUnitState() == ATTACKING) then
		-- doing attack animation, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		else
			if(not pc.Attack()) then
				pc.PopState()
			end
		end
	end
end

function EnemyUpdate2(pc, dt)
	-- update cooldown
	if((pc.GetUnitState() != ATTACKING) and (pc.GetUnitState() != HEALING)) then
		pc.GetCooldown().Add(dt)
	end
	
	-- make pc face the target if possible
	pc.FaceTarget()

	if(pc.GetUnitState() == IDLE) then		
		-- get highest enmity
		targetUnit = pc.GetHighestEnmity()
		
		if(targetUnit == nil) then
			-- there's no enmity yet, find closest enemy in range
			targetUnitArgs = {"Type:PC","Type:Sverd","Comparison:CloserDistance","Range:"..pc.GetSight()}
			targetUnit = pc.FindTarget(targetUnitArgs)
		end
		
		if(targetUnit != nil) then
			if(not pc.IsOutOfSight(targetUnit)) then
				-- there's a unit in sight range
				pc.SetTarget(targetUnit)
				pc.PushState(CHASEORATTACK)
				return
			end			
		end
		
		-- there's no target unit, scout around
		pc.Scout()
		pc.PushState(CHASING)
	elseif (pc.GetUnitState() == FOCUSATTACK) then		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range
				pc.ClearTarget()
				pc.PopState()
			else
				if(pc.GetCooldown()._current >= pc.GetCooldown().AdjustedValue()) then
					-- cooldown is off, can attack
					pc.GetCooldown()._current = 0
					pc.ResetHitFrames("attack")
					pc.PushState(ATTACKING)
					pc.FaceTarget()
					pc.PlayAnimation("Action00")
				else
					-- although we're focusing on a unit to attack, check for new highest enmity
					targetUnit = pc.GetHighestEnmity()			
					if((targetUnit != nil) and (targetUnit != pc.GetTarget())) then
						-- there's a higher enmity target
						if(not pc.IsOutOfSight(targetUnit)) then
							-- only switch if it's not out of sight
							pc.SetTarget(targetUnit)
							pc.PopState()
						end
					end
				end
			end
		end
	elseif (pc.GetUnitState() == CHASEORATTACK) then
		-- should we chase or can we attack		
		if(pc.IsTargetLost()) then
			-- check whether target is lost
			pc.PopState()
		elseif(pc.IsTargetDown()) then
			-- check whether target is down
			pc.ClearTarget()
			pc.PopState()
		else
			if(pc.IsTargetOutOfRange(pc.GetRangeAtk()._current)) then
				-- target is out of range, chase it
				pc.PushState(CHASING)
				pc.SetPathToTarget(ATTACK)
			else
				pc.PushState(FOCUSATTACK)
			end
		end
	elseif (pc.GetUnitState() == CHASING) then
		-- chasing a target, if we're not moving anymore, means we've reached the target
		if(not pc.IsMoving()) then
			pc.PopState()
		else
			currentTarget = pc.GetTarget()
			if(currentTarget != nil) then
				if(pc.IsOutOfSight(currentTarget)) then
				-- current target is going out of sight reset and go back to scouting
					pc.ClearTarget()
					pc.ResetState()
					pc.Scout()
					pc.PushState(CHASING)
				end
			end
		
			-- while moving, it's possible to change target if there's a higher enmity
			targetUnit = pc.GetHighestEnmity()			
			if((targetUnit != nil) and (targetUnit != pc.GetTarget())) then
				-- there's a higher enmity target
				if(not pc.IsOutOfSight(targetUnit)) then 
					-- only switch if it's not out of sight
					pc.SetTarget(targetUnit)
					pc.PopState()
				end
			else
				-- still chasing the same target, consider repath if possible
				repathRate = pc.GetRepathRate()
				if(repathRate._current >= repathRate._base) then
					repathRate._current = 0
					pc.Repath()
				end
			end
		end
	elseif (pc.GetUnitState() == ATTACKING) then
		-- doing attack animation, check for target lost or target down
		if(pc.IsTargetLost() or pc.IsTargetDown()) then
			pc.PopState()
		else
			if(not pc.Attack()) then
				pc.PopState()
			end
		end
	end
end

function NPCUpdate(pc,dt)
	if(pc.GetUnitState() == IDLE) then
	elseif(pc.GetUnitState() == INTERACTING) then
		if(not pc.IsLocked()) then
			pc.GetTarget().Unlock()
		end
	end
end

function DefaultInteracted(pc,interactingUnit)
	interactingUnit.ResetState()
	interactingUnit.ClearTarget()
end

function DefaultNPCInteracted(pc,interactingUnit)
	pc.Lock()
	interactingUnit.Lock()
	pc.ResetState()
	pc.SetTarget(interactingUnit)
	pc.PushState(INTERACTING)
	pc.QueryGameEvent()
end

return "Behavior Lua loaded"